﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('UsuariosServices', UsuariosServices);

    UsuariosServices.$inject = ['$ApiClient'];

    function UsuariosServices($ApiClient) {

        this.buscar = buscar;
        this.obtenerPorId = obtenerPorId;
        this.guardar = guardar;

        //============================================================================

        function buscar(email, idRol) {

            return $ApiClient.get("user/buscarUsuarios",
                {
                    email: email,
                    idRol: idRol
                });
        };

        function obtenerPorId(idUsuario) {

            return $ApiClient.get("user/obtenerPorId",
                {
                    idUsuario: idUsuario
                });
        };

        function guardar(usuario) {

            return $ApiClient.post("user/guardarUsuario",
                {
                    usuario: usuario
                });
        }; 
    }

})();