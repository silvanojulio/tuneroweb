﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('TurnosServices', TurnosServices);

    TurnosServices.$inject = ['$ApiClient'];

    function TurnosServices($ApiClient) {

        this.getDisponibilidades = getDisponibilidades;
        this.crearTurno = crearTurno;
        this.buscarTurnos = buscarTurnos;
        this.cambiarDeEstado = cambiarDeEstado;
        this.reasignarTurno = reasignarTurno;
        this.asignarProfesionalSecundario = asignarProfesionalSecundario;
        this.quitarProfesionalSecundario = quitarProfesionalSecundario;
        this.replicarEnGoogleCalendar = replicarEnGoogleCalendar;
        this.agregarObservacionesATurno = agregarObservacionesATurno;
        this.obtenerProximosTurnosDeUsuario = obtenerProximosTurnosDeUsuario;

        //============================================================================

        function getDisponibilidades(idsConsultorios, idsProfesionales, fechaDesde, fechaHasta) {

            return $ApiClient.get("Turnos/getDisponibilidades", {
                idsConsultorios: idsConsultorios,
                idsProfesionales: idsProfesionales,
                desde: fechaDesde,
                hasta: fechaHasta
            });
        };
        
        function crearTurno(idPaciente, idConsultorio, idProfesional, fechaHoraInicio, minutos, celular, email, comentarios) {

            return $ApiClient.post("Turnos/crearTurno", {
                idPaciente: idPaciente,
                idConsultorio: idConsultorio,
                idProfesional: idProfesional,
                fechaHoraInicio: fechaHoraInicio,
                minutos: minutos,
                celular: celular,
                email: email,
                comentarios: comentarios
            });
        };
        
        function buscarTurnos(idsConsultorios, idsProfesionales, idsEstados, fechaDesde, fechaHasta, idDisponibilidad) {

            return $ApiClient.get("Turnos/buscarTurnos", {
                idsConsultorios: idsConsultorios,
                idsProfesionales: idsProfesionales,
                idsEstados: idsEstados,
                desde: fechaDesde,
                hasta: fechaHasta,
                idDisponibilidad: idDisponibilidad
            });
        };

        function cambiarDeEstado(idTurno, idEstado, comentarios) {

            return $ApiClient.post("Turnos/cambiarDeEstado", {
                idTurno: idTurno,
                idEstado: idEstado,
                comentarios: comentarios
            });
        };

        function reasignarTurno(idTurno, fechaHoraInicio, idProfesional, idConsultorio) {

            return $ApiClient.post("Turnos/reasignarTurno", {
                idTurno: idTurno,
                fechaHoraInicio: fechaHoraInicio,
                idProfesional: idProfesional,
                idConsultorio: idConsultorio
            });
        };

        function asignarProfesionalSecundario(idTurno, idProfesional) {

            return $ApiClient.post("Turnos/asignarProfesionalSecundario", {
                idTurno: idTurno,
                idProfesional: idProfesional
            });
        };

        function quitarProfesionalSecundario(idTurno) {

            return $ApiClient.post("Turnos/asignarProfesionalSecundario", {
                idTurno: idTurno,
                idProfesional: null
            });
        };

        function replicarEnGoogleCalendar(idTurno) {

            return $ApiClient.post("Turnos/replicarTurnoEnGoogleCalendar", {
                idTurno: idTurno
            });
        };

        function agregarObservacionesATurno(idTurno, comentarios) {

            return $ApiClient.post("Turnos/agregarObservacionesATurno", {
                idTurno: idTurno,
                comentarios: comentarios
            });
        };

        function obtenerProximosTurnosDeUsuario() {

            return $ApiClient.get("Turnos/obtenerProximosTurnosDeUsuario", {
            });
        };
    }

})();