﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('ObrasSocialesServices', ObrasSocialesServices);

    ObrasSocialesServices.$inject = ['$ApiClient'];

    function ObrasSocialesServices($ApiClient) {

        this.buscar = buscar;
        this.obtenerPorId = obtenerPorId;
        this.obtenerPlanesDeObraSocial = obtenerPlanesDeObraSocial;
        this.guardarObraSocial = guardarObraSocial;
        this.guardarPlanObraSocial = guardarPlanObraSocial;

        //============================================================================

        function buscar(soloActivos) {

            return $ApiClient.get("obrasSociales/buscar",
                {
                    soloActivos: soloActivos
                });
        };

        function obtenerPorId(idObraSocial) {

            return $ApiClient.get("obrasSociales/obtenerPorId",
                {
                    idObraSocial: idObraSocial
                });
        };

        function obtenerPlanesDeObraSocial(idObraSocial) {

            return $ApiClient.get("obrasSociales/obtenerPlanesDeObraSocial",
                {
                    idObraSocial: idObraSocial
                });
        };
        
        function guardarObraSocial(obraSocial) {

            return $ApiClient.post("obrasSociales/guardarObraSocial", obraSocial);
        };

        function guardarPlanObraSocial(plan) {

            return $ApiClient.post("obrasSociales/guardarPlanObraSocial", plan);
        };
        
    }

})();