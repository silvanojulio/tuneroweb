﻿(function () {
    'use strict';

    angular
        .module('app.user')
        .service('LoginServices', LoginServices);

    LoginServices.$inject = ['$http', '$ApiClient'];

    function LoginServices($http, $ApiClient) {

        this.login = login;
        this.getCurrentUser = getCurrentUser;
        this.loginWithGoogle = loginWithGoogle;
        this.createUserApp = createUserApp;
        this.recoverPassword = recoverPassword;
        this.confirmarCuenta = confirmarCuenta;

        //============================================================================

        function login(email, password) {
            return $ApiClient.post("User/Login", {
                email: email,
                password:password
            });
        };

        function loginWithGoogle(request) {

            return $ApiClient.post("User/loginWithGoogle", request);
        };

        function getCurrentUser() {
            return $ApiClient.get("User/GetCurrentUser");
        };

        function createUserApp(email, password, phone) {
            return $ApiClient.post("User/crearUsuarioApp",
                {
                    email: email,
                    password: password,
                    nroCelular: phone
                } 
            );
        };

        function recoverPassword(email) {

            return $ApiClient.post("User/resetPassword",
                {
                    email: email
                }
            );
        };

        function confirmarCuenta(email, codigo) {
            return $ApiClient.post("User/confirmarCuentaPorEmail",
                {
                    email: email,
                    codigo: codigo
                }
            );
        }
    }

})();