﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('DisponibilidadServices', DisponibilidadServices);

    DisponibilidadServices.$inject = ['$ApiClient'];

    function DisponibilidadServices($ApiClient) {

        this.buscar = buscar;
        this.getDataForBuscar = getDataForBuscar;
        this.crearDisponibilidades = crearDisponibilidades;
        this.cancelarDisponibilidad = cancelarDisponibilidad;
        this.obtenerDisponibilidadPorId = obtenerDisponibilidadPorId;
        this.editarDisponibilidad = editarDisponibilidad;

        //============================================================================

        function buscar(idsProfesionales, desde, hasta, idEstado, idConsultorio) {

            return $ApiClient.get("disponibilidad/Buscar",
                {
                    fechaDesde: desde,
                    fechaHasta: hasta,
                    idConsultorio: idConsultorio,
                    idsProfesionales: idsProfesionales,
                    idEstado: idEstado
                });
        };

        function getDataForBuscar() {

            return $ApiClient.get("disponibilidad/getDataForBuscar",null);
        };
        
        function obtenerDisponibilidadPorId(id) {

            return $ApiClient.get("disponibilidad/obtenerPorId",
                {
                    idDisponibilidad: id
                });
        };

        function crearDisponibilidades(disponibilidad, idsProfesionales, esDisponibilidadRepetitiva, lunes, martes, miercoles, jueves, viernes, sabado, domingo) {

            return $ApiClient.post("disponibilidad/crearDisponibilidades",
                {
                    disponibilidad : disponibilidad,
                    idsProfesionales: idsProfesionales,
                    esDisponibilidadRepetitiva: esDisponibilidadRepetitiva,
                    lunes: lunes,
                    martes: martes,
                    miercoles: miercoles,
                    jueves: jueves,
                    viernes: viernes,
                    sabado: sabado,
                    domingo: domingo
                });
        };

        function cancelarDisponibilidad(idDisponibilidad) {
            return $ApiClient.post("disponibilidad/CancelarDisponibilidad",
                {
                    idDisponibilidad: idDisponibilidad
                });
        };

        function editarDisponibilidad(_id, fechaHoraInicio, fechaHoraFin, idConsultorio, idProfesional, comentarios) {

            return $ApiClient.post("disponibilidad/editarDisponibilidad",
                {
                    idDisponibilidad: _id,
                    fechaHoraInicio: fechaHoraInicio,
                    fechaHoraFin: fechaHoraFin,
                    idConsultorio: idConsultorio,
                    idProfesional: idProfesional,
                    comentarios: comentarios
                });
        };

    }

})();