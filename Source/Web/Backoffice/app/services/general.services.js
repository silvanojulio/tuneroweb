﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('GeneralServices', GeneralServices);

    GeneralServices.$inject = ['$ApiClient'];

    function GeneralServices($ApiClient) {

        this.getEstadosDeTurnos = getEstadosDeTurnos;
        this.getConsultorios = getConsultorios;
        this.getProfesionales = getProfesionales;
        this.getPacientes = getPacientes; 
        this.getValoresSatelites = getValoresSatelites; 

        //============================================================================
        
        function getEstadosDeTurnos() {

            return $ApiClient.get("General/getEstadosDeTurnos");
        };

        function getConsultorios() {

            return $ApiClient.get("General/getConsultorios");
        };

        function getProfesionales() {

            return $ApiClient.get("General/getProfesionales");
        };

        function getPacientes(texto) {

            return $ApiClient.get("General/getPacientes", {
                textoBuscado: texto
            });
        };

        function getValoresSatelites(idsTablas) {

            return $ApiClient.get("General/getValoresDeTablaSatelite", {
                idsTablasSatelites: idsTablas
            });
        };
    }

})();