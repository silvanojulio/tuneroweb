﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('EventosServices', EventosServices);

    EventosServices.$inject = ['$ApiClient'];

    function EventosServices($ApiClient) {

        this.guardarEvento = guardarEvento;
        this.obtenerEventoPorId = obtenerEventoPorId;
        this.obtenerEventosPorFechas = obtenerEventosPorFechas;
        this.eliminarEvento = eliminarEvento;

        //============================================================================

        function obtenerEventosPorFechas(desde, hasta) {

            return $ApiClient.get("eventos/Buscar",
                {
                    fechaDesde: desde,
                    fechaHasta: hasta
                });
        };
        
        function obtenerEventoPorId(id) {

            return $ApiClient.get("eventos/obtenerPorId",
                {
                    idEvento: id
                });
        };

        function guardarEvento(evento) {

            return $ApiClient.post("eventos/guardar", evento);
        };        

        function eliminarEvento(idEvento) {

            return $ApiClient.post("eventos/eliminarEvento", {
                idEvento : idEvento
            });
        };     
    }

})();