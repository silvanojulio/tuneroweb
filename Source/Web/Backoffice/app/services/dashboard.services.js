﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('DashboardServices', DashboardServices);

    DashboardServices.$inject = ['$ApiClient'];

    function DashboardServices($ApiClient) {

    	this.obtenerDashboard = obtenerDashboard;
        //============================================================================
        
        function obtenerDashboard() {

        	return $ApiClient.get("Dashboard/obtenerDashboard");
        };

    }

})();