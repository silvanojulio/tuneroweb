﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('PacientesServices', PacientesServices);

    PacientesServices.$inject = ['$ApiClient'];

    function PacientesServices($ApiClient) {

        this.buscarPacientes = buscarPacientes;
        this.obtenerPacientePorId = obtenerPacientePorId;
        this.getDataForCreate = getDataForCreate;
        this.guardar = guardar; 
        this.obtenerPacientesDeUsuarioActual = obtenerPacientesDeUsuarioActual;

        //============================================================================
        
        function buscarPacientes(nombre, dni, nroFicha) {

            return $ApiClient.get("Paciente/Buscar",
                {
                    nombreCompleto: nombre,
                    dni: dni,
                    nroFicha: nroFicha
                });
        };

        function obtenerPacientePorId(idPaciente) {

            return $ApiClient.get("Paciente/ObtenerPorId",
                {
                    idPaciente: idPaciente
                });
        };

        function getDataForCreate() {

            return $ApiClient.get("Paciente/getDataForCreate");
        };

        function guardar(paciente, email) {

            return $ApiClient.post("Paciente/Guardar",
                {
                    paciente: paciente,
                    email: email
                });
        };

        function obtenerPacientesDeUsuarioActual() {

            return $ApiClient.get("Paciente/obtenerPacientesDeUsuarioActual",
                {
                });
        };
    }

})();