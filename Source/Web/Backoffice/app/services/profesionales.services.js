﻿(function () {
    'use strict';

    angular
        .module('app.turnero')
        .service('ProfesionalesServices', ProfesionalesServices);

    ProfesionalesServices.$inject = ['$ApiClient'];

    function ProfesionalesServices($ApiClient) {

        this.buscar = buscar;
        this.obtenerPorId = obtenerPorId;
        this.getDataForCreate = getDataForCreate;
        this.guardar = guardar;

        //============================================================================

        function buscar(nombre) {

            return $ApiClient.get("Profesionales/buscar",
                {
                    nombre: nombre
                });
        };

        function obtenerPorId(idProfesional) {

            return $ApiClient.get("Profesionales/obtenerPorId",
                {
                    idProfesional: idProfesional
                });
        };

        function getDataForCreate() {

            return $ApiClient.get("Profesionales/getDataForCreate");
        };

        function guardar(profesional) {

            return $ApiClient.post("Profesionales/Guardar", profesional);
        };
    }

})();