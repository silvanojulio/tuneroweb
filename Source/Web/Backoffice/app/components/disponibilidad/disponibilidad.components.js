﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('buscarDisponibilidadesController', buscarDisponibilidadesController);
    buscarDisponibilidadesController.$inject = ['$scope', 'SweetAlert', 'DisponibilidadServices', 'GestionesDeDisponibilidades'];
    function buscarDisponibilidadesController($scope, SweetAlert, DisponibilidadServices, GestionesDeDisponibilidades) {

        var init = function () {
            $scope.disponibilidades = null;
            $scope.busqueda =
                {
                    fechaDesde: null,
                    fechaHasta: null,
                    idConsultorio: null,
                    idEstado: null,
                    idsProfesionales: null,
                    periodo:{}
                };

            DisponibilidadServices.getDataForBuscar()
                .then(function (resp) {

                    $scope.profesionales = resp.data.profesionales;
                    $scope.estados = resp.data.estados;
                    $scope.consultorios = resp.data.consultorios;

                });
        };
        
        $scope.onBuscar = function (callback) {

            $scope.busqueda.fechaDesde = $scope.busqueda.periodo.desde;
            $scope.busqueda.fechaHasta = $scope.busqueda.periodo.hasta;
            $scope.busqueda.idConsultorio = $scope.busqueda.consultorioSelected != null ? $scope.busqueda.consultorioSelected._id : null;
            $scope.busqueda.idEstado = $scope.busqueda.estadoSelected != null ? $scope.busqueda.estadoSelected.id : 1;
            $scope.busqueda.idsProfesionales = $scope.busqueda.profesionalesSelected != null ? $scope.busqueda.profesionalesSelected.map(function (x) { return x._id; }) : null;

            actualizar(callback);
        };

        var actualizar = function (callback) {

            SweetAlert.showSpinner();

            DisponibilidadServices.buscar($scope.busqueda.idsProfesionales, $scope.busqueda.fechaDesde, $scope.busqueda.fechaHasta, $scope.busqueda.idEstado, $scope.busqueda.idConsultorio)
                .then(function (resp) {

                    $scope.disponibilidades = resp.data;

                    SweetAlert.closeSpinner();

                    if (callback != null) { //Callback que proviene del plugin de FullCalendar

                        var events = [];

                        angular.forEach($scope.disponibilidades, function (d) {

                            events.push(
                                {
                                    title: (d.esReceso ? '(R) ' : '')+ d.profesional.nombreCompleto + ' ('+ d.consultorio.nombre+')',
                                    start: new Date(d.fechaHoraInicio),
                                    end: new Date(d.fechaHoraFin),
                                    id: d._id,
                                    allDay: d.esTodoElDia,
                                    color: d.esReceso ? 'orange' : (d.esTodoElDia ? null : (d.profesional.color == null ? 'green' : d.profesional.color)) 
                                }
                            );
                        });

                        callback(events);
                    } else {
                        $scope.refreshCalendar();
                    }
                    
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onRefreshCalendarClick = function () {
            $scope.actions.refreshCalendar();
        }

        $scope.actions = { refreshCalendar: null };

        $scope.actions.onEventClick = function (calEvent, jsEvent, view) {

            GestionesDeDisponibilidades.editarDisponiblidad(calEvent._id, false, $scope.onRefreshCalendarClick, null);
        };

        $scope.onNuevaDisponibilidad = function () {
            GestionesDeDisponibilidades.editarDisponiblidad(null, false, null, null);
        };

        $scope.onCrearReceso = function () {
            GestionesDeDisponibilidades.editarDisponiblidad(null, true, null, null);
        };

        $scope.onEditar = function (disponiblidad) {
            GestionesDeDisponibilidades.editarDisponiblidad(disponiblidad._id, false, $scope.onRefreshCalendarClick, null);
        };

        $scope.cancelar = function (disponiblidad) {

            var mensaje = "Está seguro que quiere cancelar la disponibilidad seleccionada?";

            SweetAlert.confirm(mensaje, "Cancelar disponiblidad", function () {

                DisponibilidadServices.cancelarDisponibilidad(disponiblidad._id)
                    .then(
                    function (resp) {
                        $scope.onRefreshCalendarClick();
                    },
                    function (resp) {
                        SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                    });

            });
        };

        //Function que recibe la directiva del calendario cada vez que necesita obtener los eventos para mostrar en un determinado periodo
        $scope.getEvents = function (start, end, timezone, callback) {

            $scope.busqueda.periodo.desde = start._d;
            $scope.busqueda.periodo.hasta = end._d;

            $scope.onBuscar(callback);
        };
        
        $scope.obtenerColorDeFila = function (disponiblidad) {

            if (disponiblidad.estado.id == 2 || disponiblidad.estado.id == 3) return "bg-red-300"; //Cancelado

            return "";
        }

        init();
    }

    angular.module('app.turnero').controller('editarDisponibilidadController', editarDisponibilidadController);
    editarDisponibilidadController.$inject = ['$scope', 'SweetAlert', 'DisponibilidadServices', 'GestionesDeDisponibilidades', '$uibModalInstance', 'idDisponibilidad', 'esReceso'];
    function editarDisponibilidadController($scope, SweetAlert, DisponibilidadServices, GestionesDeDisponibilidades, $uibModalInstance, idDisponibilidad, esReceso) {

        $scope.idDisponibilidad = idDisponibilidad;
        $scope.esEdicion = $scope.idDisponibilidad != null;

        $scope.esReceso = esReceso;

        var init = function () {

            $scope.modelo = {};
            
            DisponibilidadServices.getDataForBuscar()
                .then(function (resp) {

                    $scope.profesionales = resp.data.profesionales;
                    $scope.consultorios = resp.data.consultorios;
                    $scope.motivosDeReceso = resp.data.motivosDeReceso;

                    if ($scope.motivosDeReceso.length>0)
                        $scope.modelo.motivoRecesoSelected = $scope.motivosDeReceso[0];

                    //Si es edición de disponibilidad
                    if ($scope.idDisponibilidad != null) {
                        obtenerDisponibilidad();
                    } else {
                        $scope.titulo = $scope.esReceso ? "Crear recesos" : "Crear disponiblidad de profesionales";
                    }
                });       
        };

        var obtenerDisponibilidad = function () {

            DisponibilidadServices.obtenerDisponibilidadPorId($scope.idDisponibilidad)
                .then(function (resp) {

                    $scope.disponibilidad = resp.data;

                    var consultorio = $scope.consultorios.filter(function (x) { return x._id == $scope.disponibilidad.idConsultorio; });
                    if (consultorio.length > 0) $scope.modelo.consultorioSelected = consultorio[0];

                    var profesional = $scope.profesionales.filter(function (x) { return x._id == $scope.disponibilidad.idProfesional; });
                    if (profesional.length > 0) $scope.modelo.profesionalSelected = profesional[0];
                    
                    $scope.modelo.fechaDesde = new Date($scope.disponibilidad.fechaHoraInicio);
                    $scope.modelo.fechaHasta = new Date($scope.disponibilidad.fechaHoraFin);

                    $scope.titulo = "Editar disponibilidad";
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        var validar = function () {
            var message = "";

            if ($scope.esReceso && $scope.modelo.motivoRecesoSelected == null) message += "Debe seleccionar un motivo. ";
                        
            return { hasError: message != "", message: message };
        };

        $scope.onGuardar = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            if ($scope.idDisponibilidad == null) crearDisponibilidades();
            else guardarDisponibilidad();
                       
        };

        $scope.onCancelarDisponibilidad = function () {

            var mensaje = "Está seguro que quiere cancelar la disponibilidad seleccionada?";

            SweetAlert.confirm(mensaje, "Cancelar disponiblidad", function () {

                DisponibilidadServices.cancelarDisponibilidad($scope.idDisponibilidad)
                    .then(
                    function (resp) {
                        $uibModalInstance.close();
                    },
                    function (resp) {
                        SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                    });

            });
        };

        var crearDisponibilidades = function () {

            SweetAlert.showSpinner();

            $scope.modelo.fechaDesde.setSeconds(0);
            $scope.modelo.fechaDesde.setMilliseconds(0);

            $scope.modelo.fechaHasta.setSeconds(0);
            $scope.modelo.fechaHasta.setMilliseconds(0);

            var disponibilidad = {                                
                fechaHoraInicio: $scope.modelo.fechaDesde,
                fechaHoraFin: $scope.modelo.fechaHasta,
                esTodoElDia: $scope.modelo.esTodoElDia,
                idConsultorio: $scope.modelo.consultorioSelected._id,

                esReceso: $scope.esReceso,
                comentarios: $scope.esReceso? $scope.modelo.comentarios : null,
                motivoReceso: $scope.esReceso ? $scope.modelo.motivoRecesoSelected : null        
            };

            var idsProfesionales = $scope.modelo.profesionalesSelected.map(function (x) { return x._id; });
            
            DisponibilidadServices.crearDisponibilidades(disponibilidad, idsProfesionales,
                $scope.modelo.esSerie,
                $scope.modelo.diaLunes.obtenerHorario(), $scope.modelo.diaMartes.obtenerHorario(),
                $scope.modelo.diaMiercoles.obtenerHorario(), $scope.modelo.diaJueves.obtenerHorario(),
                $scope.modelo.diaViernes.obtenerHorario(), $scope.modelo.diaSabado.obtenerHorario(),
                $scope.modelo.diaDomingo.obtenerHorario())
                .then(function (resp) {

                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        var guardarDisponibilidad = function () {

            SweetAlert.showSpinner();

            $scope.modelo.fechaDesde.setSeconds(0);
            $scope.modelo.fechaDesde.setMilliseconds(0);

            $scope.modelo.fechaHasta.setSeconds(0);
            $scope.modelo.fechaHasta.setMilliseconds(0);

            var d = {
                _id: $scope.idDisponibilidad,
                fechaHoraInicio: $scope.modelo.fechaDesde,
                fechaHoraFin: $scope.modelo.fechaHasta,
                idConsultorio: $scope.modelo.consultorioSelected._id,
                idProfesional: $scope.modelo.profesionalSelected._id,
                comentarios: $scope.esReceso ? $scope.modelo.comentarios : null
            };
            
            DisponibilidadServices.editarDisponibilidad(d._id, d.fechaHoraInicio, d.fechaHoraFin, d.idConsultorio, d.idProfesional, d.comentarios)
                .then(function (resp) {

                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };

        $scope.onDesdeChange = function (nuevaFecha) {

            if ($scope.modelo.fechaHasta == null || nuevaFecha > $scope.modelo.fechaHasta) {
                $scope.modelo.fechaHasta = nuevaFecha;
            }
        };

        $scope.onHastaChange = function (nuevaFecha) {

            if ($scope.modelo.fechaDesde == null || $scope.modelo.fechaDesde > nuevaFecha) {
                $scope.modelo.fechaDesde = nuevaFecha;
            }
        };
        
        init();
    }
    
    angular.module('app.turnero').service('GestionesDeDisponibilidades', GestionesDeDisponibilidades);
    GestionesDeDisponibilidades.$inject = ['DisponibilidadServices', '$uibModal'];
    function GestionesDeDisponibilidades(DisponibilidadServices, $uibModal) {

        this.editarDisponiblidad = editarDisponiblidad;

        //============================================================================

        function editarDisponiblidad(idDisponibilidad, esReceso, onSuccess, onCancel) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/disponibilidad/editarDisponiblidad.html',
                controller: 'editarDisponibilidadController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    idDisponibilidad: function () {
                        return idDisponibilidad;
                    },
                    esReceso: function() {
                        return esReceso;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };
    }

})();

