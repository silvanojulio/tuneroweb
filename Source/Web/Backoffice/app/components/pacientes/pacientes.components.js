﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('buscarPacientesController', buscarPacientesController);
    buscarPacientesController.$inject = ['$scope', 'SweetAlert', '$cookies', 'PacientesServices', 'GestionesDePacientes', 'LoginServices'];

    function buscarPacientesController($scope, SweetAlert, $cookies, PacientesServices, GestionesDePacientes, LoginServices) {
        var obraSocial = null;

        var init = function () {
            $scope.pacientes = null;
            $scope.busqueda =
                {
                    nombrePaciente: null,
                    dni: null,
                    nroFicha: null
                };

            LoginServices.getCurrentUser()
                .then(function (resp) {
                    if (resp.status = 200 && resp.data != null) {

                        PacientesServices.getDataForCreate().then(function (resp) {
                            $scope.obrasSociales = resp.data.obrasSociales;

                        }, function (resp) {
                            SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                        });

                        PacientesServices.obtenerPacientesDeUsuarioActual()
                            .then(function (resp) {

                                $scope.pacientes = resp.data;

                                if ($scope.obrasSociales != null) {
                                    angular.forEach($scope.pacientes, function (paciente) {
                                        if (paciente.idObraSocial != null) {

                                            obraSocial = $scope.obrasSociales.filter(function (o) { return o._id == paciente.idObraSocial; })[0];
                                            paciente.obraSocial = obraSocial.nombre;

                                            paciente.plan = obraSocial.planes.filter(function (p) { return p._id == paciente.idPlanObraSocial; })[0].nombre;
                                            obraSocial = null;
                                        }
                                    });
                                }

                                SweetAlert.closeSpinner();
                            }, function (resp) {
                                SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                            });
                    }
                }, function (resp) { });
        };

        $scope.onBuscar = function () {

            SweetAlert.showSpinner();

            PacientesServices.buscarPacientes($scope.busqueda.nombrePaciente, $scope.busqueda.dni, $scope.busqueda.nroFicha)
                .then(function (resp) {

                    $scope.pacientes = resp.data;
                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onNuevoPaciente = function () {
            GestionesDePacientes.editarPaciente(null, init, null);
        };

        $scope.onEditar = function (paciente) {
            GestionesDePacientes.editarPaciente(paciente._id, $scope.onBuscar, null);
        };

        init();
    }

    angular.module('app.turnero').controller('editarPacienteController', editarPacienteController);
    editarPacienteController.$inject = ['$scope', 'SweetAlert', 'PacientesServices', 'GestionesDePacientes', '$uibModalInstance', 'idPaciente'];
    function editarPacienteController($scope, SweetAlert, PacientesServices, GestionesDePacientes, $uibModalInstance, idPaciente) {

        $scope.idPaciente = idPaciente;
        var init = function () {

            $scope.paciente = {};

            PacientesServices.getDataForCreate().then(function (resp) {

                $scope.provincias = resp.data.provincias;
                $scope.estados = resp.data.estados;
                $scope.obrasSociales = resp.data.obrasSociales;

                if ($scope.idPaciente != null) {
                    obtenerPaciente();
                } else {
                    $scope.titulo = "Nuevo paciente";
                }

            }, function (resp) {
                SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
            });

        };

        var obtenerPaciente = function () {
            PacientesServices.obtenerPacientePorId($scope.idPaciente)
                .then(function (resp) {

                    $scope.paciente = resp.data.paciente;
                    $scope.paciente.fechaDeNacimiento = new Date($scope.paciente.fechaDeNacimiento);

                    $scope.titulo = "Paciente: " + $scope.paciente.nombreCompleto;

                    var idsObrasSociales = $scope.obrasSociales == null ?
                        [] :
                        $scope.obrasSociales.map(function (x) { return x._id; });

                    if ($scope.paciente.idObraSocial != null && idsObrasSociales.indexOf($scope.paciente.idObraSocial) > -1) {
                        $scope.onObraSocialChange();
                    }

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        var validar = function () {
            var message = "";

            if ($scope.paciente.nombre == null || $scope.paciente.nombre == "") message += "Debe ingresar un nombre para el paciente";
            if ($scope.paciente.apellido == null || $scope.paciente.apellido == "") message += "Debe ingresar un apellido para el paciente";

            return { hasError: message != "", message: message };
        };

        $scope.onGuardar = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            SweetAlert.showSpinner();

            PacientesServices.guardar($scope.paciente, $scope.email)
                .then(function (resp) {

                    SweetAlert.closeSpinner();

                    $uibModalInstance.close();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };

        $scope.onObraSocialChange = function () {

            if ($scope.paciente.idObraSocial == null) return;

            var obraSocialSeleccionada = $scope.obrasSociales.filter(function (x) { return x._id == $scope.paciente.idObraSocial; });

            $scope.planesDisponibles = obraSocialSeleccionada[0].planes;
        };

        init();
    }

    angular.module('app.turnero').service('GestionesDePacientes', GestionesDePacientes);
    GestionesDePacientes.$inject = ['PacientesServices', '$uibModal'];
    function GestionesDePacientes(PacientesServices, $uibModal) {

        this.editarPaciente = editarPaciente;

        //============================================================================

        function editarPaciente(idPaciente, onSuccess, onCancel) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/pacientes/editarPaciente.html',
                controller: 'editarPacienteController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    idPaciente: function () {
                        return idPaciente;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };
    }

})();
