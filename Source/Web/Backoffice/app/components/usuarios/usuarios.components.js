﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('buscarUsuariosController', buscarUsuariosController);
    buscarUsuariosController.$inject = ['$scope', 'SweetAlert', 'UsuariosServices', 'GestionesDeUsuarios'];
    function buscarUsuariosController($scope, SweetAlert, UsuariosServices, GestionesDeUsuarios) {

        var init = function () {

            $scope.usuarios = null;

            $scope.busqueda =
                {
                    email: null,
                    idRol: 3,
                    activo: true
                };

            $scope.roles = ROLES_DE_USUARIOS;
        };

        $scope.onBuscar = function () {

            SweetAlert.showSpinner();

            UsuariosServices.buscar($scope.busqueda.email, $scope.busqueda.idRol)
                .then(function (resp) {

                    $scope.usuarios = resp.data;
                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onNuevo = function () {
            GestionesDeUsuarios.editar(null, null, null);
        };

        $scope.onEditar = function (item) {
            GestionesDeUsuarios.editar(item._id, $scope.onBuscar, null);
        };

        init();
    }

    angular.module('app.turnero').controller('editarUsuarioController', editarUsuarioController);
    editarUsuarioController.$inject = ['$scope', 'SweetAlert', 'UsuariosServices', 'GestionesDeUsuarios', '$uibModalInstance', 'idUsuario'];
    function editarUsuarioController($scope, SweetAlert, UsuariosServices, GestionesDeUsuarios, $uibModalInstance, idUsuario) {

        $scope.idUsuario = idUsuario;
        $scope.roles = ROLES_DE_USUARIOS;

        var init = function () {
            if ($scope.idUsuario == null) {
                $scope.titulo = "Nuevo usuario";
                $scope.usuario = { idRol:3 };
            } else {
                obtenerUsuario();
            } 
        };

        var obtenerUsuario = function () {

            UsuariosServices.obtenerPorId($scope.idUsuario)
                .then(function (resp) {

                    $scope.usuario = resp.data;
                    $scope.usuario.idRol = $scope.usuario.rol.id;
                    $scope.titulo = "Usuario: " + $scope.usuario.email;

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        var validar = function () {
            var message = "";

            if ($scope.usuario.email == null || $scope.usuario.email == "") message += "Error en el campo email";
            
            return { hasError: message != "", message: message };
        };
        
        $scope.onGuardar = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            SweetAlert.showSpinner();

            UsuariosServices.guardar($scope.usuario)
                .then(function (resp) {

                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };
        
        init();
    }

    angular.module('app.turnero').service('GestionesDeUsuarios', GestionesDeUsuarios);
    GestionesDeUsuarios.$inject = ['UsuariosServices', '$uibModal'];
    function GestionesDeUsuarios(UsuariosServices, $uibModal) {

        this.editar = editar;

        //============================================================================

        function editar(idUsuario, onSuccess, onCancel) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/usuarios/editarUsuario.html',
                controller: 'editarUsuarioController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    idUsuario: function () {
                        return idUsuario;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };
    }

    var ROLES_DE_USUARIOS = [
        //{ id: 1, nombre: 'Paciente' },
        //{ id: 2, nombre: 'Profesional' },
        { id: 3, nombre: 'Secretaria' }
    ];

})();
