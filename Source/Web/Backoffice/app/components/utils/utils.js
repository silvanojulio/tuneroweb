function gup(name) {    
    var url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}


(function () {
    'use strict';

    angular
        .module('app.core')
        .service('$ApiClient', $ApiClient);

    $ApiClient.$inject = ['$http', '$cookies'];

    function $ApiClient($http, $cookies) {

        this.post = post;
        this.get = get;

        if (CURRENT_TOKEN == undefined || CURRENT_TOKEN == null) {
            CURRENT_TOKEN = $cookies.get("CURRENT_TOKEN");
        }

        function post(action, body) {

            return $http(
                {
                    method: 'POST',
                    url: _webConfig.apiUrl + action,
                    data: body,
                    headers: {
                        "SECURITYTOKEN": CURRENT_TOKEN
                    }
                }
            );
        }

        function get(action, params) {

            if (params == null) params = {};

            params.noCache = { noCache: new Date().getTime() };

            return $http(
                {
                    method: 'GET',
                    url: _webConfig.apiUrl + action,
                    params: params,
                    headers: {
                        "SECURITYTOKEN": CURRENT_TOKEN,
                        "If-Modified-Since": 'Mon, 26 Jul 1997 05:00:00 GMT',
                        'Cache-Control': 'no-cache',
                        'Pragma': 'no-cache'

                    }
                }
            );
        }
    }
})();


(function () {
    'use strict';

    angular
        .module('app.core')
        .service('SweetAlert', SweetAlert);
    
    function SweetAlert() {

        this.showError = showError;
        this.showSuccess = showSuccess;
        this.confirm = confirm;
        this.promtSingleText = promtSingleText;
        this.showSpinner = showSpinner;
        this.closeSpinner = closeSpinner;

        function showError(message, title) {

            swal(title, message, 'error');
        };

        function showSuccess(message, title) {

            swal(title, message, 'success');
        };
        
        function confirm(message, title, onConfirmed, onCanceled) {

                swal({
                    title: title,
                    text: message,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm) {
                    if (isConfirm) {
                        onConfirmed();
                    } else {
                        if (onCanceled != null)
                            onCanceled();
                    }
                });

        };

        function promtSingleText(message, title, inputPlaceholder, onConfirmed, onCanceled) {

            swal({
                title: title,
                text: message,
                type: 'input',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                closeOnConfirm: true,
                closeOnCancel: true,
                inputPlaceholder: inputPlaceholder
            }, function (inputValue) {

                if (inputValue !== false)
                    onConfirmed(inputValue);

            });

            }    
                
        function showSpinner(){

            swal({
                title: "Espere por favor...",
                text: '<div class="loader-primary"> <div class="loader-demo"> <div class="loader-inner square-spin"><div></div></div></div></div> ',
                html: true,
                showCloseButton: false,
                showCancelButton: false,
                showConfirmButton: false
            });

        }

        function closeSpinner(){
            swal.close();
        }
    }
})();


(function () {
    'use strict';

    angular.module('app.core')
        .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter, { 'event': event });
                    });

                    event.preventDefault();
                }
            });
        };
    });

})();


(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('fullCalendar', fullCalendar)
        .directive('dateRange', dateRange)
        .directive('dateTimePicker', dateTimePicker)
        .directive('seleccionarDiaHorario', seleccionarDiaHorario)
        .filter('propsFilter', propsFilter);

    fullCalendar.$inject = ['$timeout'];
    function fullCalendar($timeout) {
        var directive = {
            restrict: 'E',
            templateUrl:'app/components/utils/directive.fullcalendar.view.html',
            link: link,
            scope: {
                getEvents: "=",
                actions: "="
            }
        };
        return directive;

        function link(scope, element) {
            
            var myCalendar = $('#myCalendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: function (start, end, timezone, callback) {

                    scope.getEvents(start, end, timezone, callback);
                },
                eventClick: function (calEvent, jsEvent, view) {

                    scope.actions.onEventClick(calEvent, jsEvent, view)

                }
            });

            scope.actions.refreshCalendar = function () {
                $('#myCalendar').fullCalendar('refetchEvents');
            };
        }
    }

    /**
     * AngularJS default filter with the following expression:
     * "person in people | filter: {name: $select.search, age: $select.search}"
     * performs a AND between 'name: $select.search' and 'age: $select.search'.
     * We want to perform a OR.
     */
    function propsFilter() {

        return filterFilter;

        ////////////////
        function filterFilter(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function (item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];

                        if (props[prop] == null || props[prop] == undefined) return out;

                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        }
    }

    function dateRange() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/utils/directive.daterange.view.html',
            link: link,
            scope: {
                ngModel: "=",
                hoy:"@"
            }
        };
        return directive;

        function link(scope, element) {

            var init = function () {

                if (scope.hoy == "true") {

                    if (scope.ngModel == null) {
                        scope.ngModel = {};
                    }

                    scope.ngModel.hasta = new Date();
                    scope.ngModel.desde = new Date();
                }
            };

            scope.onDesdeChange = function () {

                if (scope.ngModel.hasta == null || scope.ngModel.desde > scope.ngModel.hasta) {
                    scope.ngModel.hasta = scope.ngModel.desde;
                }
            };

            scope.onHastaChange = function () {
                if (scope.ngModel.desde == null || scope.ngModel.desde > scope.ngModel.hasta) {
                    scope.ngModel.desde = scope.ngModel.hasta;
                }
            };

            init();
        }
    }
    
    function dateTimePicker() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/utils/directive.datetimepicker.view.html',
            link: link,
            scope: {
                ngModel: "=",
                hideTime: "=?",
                ngChange: "=?"
            }
        };
        return directive;

        function link(scope, element) {

            if (scope.ngChange == undefined || scope.ngChange == null) {
                scope.ngChange = function () {};
            }

            if (scope.hideTime == null)
                scope.hideTime = false;

            if (scope.ngModel == null) {
                var d = new Date();
                d.setMinutes(0);
                scope.ngModel = d;
            }

        }
    }

    function seleccionarDiaHorario() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/utils/directive.seleccionardiahorario.html',
            link: link,
            scope: {
                ngModel: "=",
                ocultarHorario: "=?",
                dia: "@",
            }
        };
        return directive;

        function link(scope, element) {

            if (scope.ngModel == null) {
                scope.ngModel = {};
            }

            scope.ngModel.horarioDesde = new Date();
            scope.ngModel.horarioDesde.setHours(8);
            scope.ngModel.horarioDesde.setMinutes(0);

            scope.ngModel.horarioHasta = new Date();
            scope.ngModel.horarioHasta.setHours(17);
            scope.ngModel.horarioHasta.setMinutes(0);

            scope.ngModel.selected = false;

            scope.ngModel.obtenerHorario = function () {

                if (!scope.ngModel.selected) return null;

                if (scope.ocultarHorario)
                    return {
                        horaDesde: 0,
                        minutosDesde: 0,

                        horaHasta: 23,
                        minutosHasta: 59,
                    };
                else
                    return {
                        horaDesde: scope.ngModel.horarioDesde.getHours(),
                        minutosDesde: scope.ngModel.horarioDesde.getMinutes(),

                        horaHasta: scope.ngModel.horarioHasta.getHours(),
                        minutosHasta: scope.ngModel.horarioHasta.getMinutes(),
                    };

            };
        }
    }
})();


function getUrlPar(name) {
    var url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}