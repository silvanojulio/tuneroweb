﻿(function () {
	'use strict';

	angular.module('app.turnero').service('GestionesDeEventos', GestionesDeEventos);
	GestionesDeEventos.$inject = ['$uibModal'];
	function GestionesDeEventos($uibModal) {

		this.editEvento = editEvento;

		//============================================================================

		function editEvento(onSuccess, onCancel, idEvento) {

			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'app/components/eventos/edit-evento.html',
				controller: 'editEventoController',
				size: 'lg',
				backdrop: false,
				resolve: {
					idEvento: function () { return idEvento; }
				}
			});

			modalInstance.result.then(function () {
				if (onSuccess != null) onSuccess();
			}, function () {
				if (onCancel != null) onCancel();
			});
		};

	}

	angular.module('app.turnero').controller('editEventoController', editEventoController);
	editEventoController.$inject = ['$scope', 'SweetAlert', 'GestionesDeEventos', 'idEvento', '$uibModalInstance', 'EventosServices'];
	function editEventoController($scope, SweetAlert, GestionesDeEventos, idEvento, $uibModalInstance, EventosServices) {

		$scope.idEvento = idEvento;
		$scope.modelo = {};

		var init = function () {
			
			$scope.titulo = "Nuevo evento";

			if ($scope.idEvento != null)
			{
				obtenerEvento();
				$scope.titulo = "Editar evento";
			}
		};

		var obtenerEvento = function () {

			EventosServices.obtenerEventoPorId($scope.idEvento)
                .then(function (resp) {

                	var evento = resp.data;

                	$scope.modelo.titulo = evento.titulo;
                	$scope.modelo.descripcion = evento.descripcion;
                	$scope.modelo.color = evento.realizado === true? '#00682d' : evento.color;
                	$scope.modelo.realizado = evento.realizado ;

                	$scope.modelo.fechaDesde = new Date(evento.fechaHoraInicio);
                	$scope.modelo.fechaHasta = new Date(evento.fechaHoraFin);

                	$scope.titulo = "Editar evento";

                }, function (resp) {
                	SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
		};

		var validar = function () {
			var message = "";

			if ($scope.esReceso && $scope.modelo.motivoRecesoSelected == null) message += "Debe seleccionar un motivo. ";

			return { hasError: message != "", message: message };
		};
		
		$scope.onGuardar = function () {
						
			$scope.modelo.fechaDesde.setSeconds(0);
			$scope.modelo.fechaDesde.setMilliseconds(0);

			$scope.modelo.fechaHasta.setSeconds(0);
			$scope.modelo.fechaHasta.setMilliseconds(0);

			var evento = {
				id: $scope.idEvento,
				fechaHoraInicio: $scope.modelo.fechaDesde,
				fechaHoraFin: $scope.modelo.fechaHasta,
				titulo: $scope.modelo.titulo,
				descripcion: $scope.modelo.descripcion,
				realizado: $scope.modelo.realizado,
				color: $scope.modelo.color
			};

			SweetAlert.showSpinner();

			EventosServices.guardarEvento(evento)
                .then(function (resp) {

                	SweetAlert.closeSpinner();
                	$uibModalInstance.close();
                }, function (resp) {
                	SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
		};

		$scope.onEliminar = function () {

			SweetAlert.showSpinner();

			EventosServices.eliminarEvento($scope.idEvento)
                .then(function (resp) {
                	SweetAlert.closeSpinner();
                	$uibModalInstance.close();
                }, function (resp) {
                	SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
		};

		$scope.onCancelar = function () {
			$uibModalInstance.dismiss();
		};

		$scope.onDesdeChange = function (nuevaFecha) {

			if ($scope.modelo.fechaHasta == null || nuevaFecha > $scope.modelo.fechaHasta) {
				$scope.modelo.fechaHasta = nuevaFecha;
			}
		};

		$scope.onHastaChange = function (nuevaFecha) {

			if ($scope.modelo.fechaDesde == null || $scope.modelo.fechaDesde > nuevaFecha) {
				$scope.modelo.fechaDesde = nuevaFecha;
			}
		};

		init();
	}

})();
