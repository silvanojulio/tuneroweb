﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('buscarObrasSocialesController', buscarObrasSocialesController);
    buscarObrasSocialesController.$inject = ['$scope', 'SweetAlert', 'ObrasSocialesServices', 'GestionesDeObrasSociales'];
    function buscarObrasSocialesController($scope, SweetAlert, ObrasSocialesServices, GestionesDeObrasSociales) {

        var init = function () {
            $scope.obrasSociales = null;
            $scope.busqueda =
                {
                    soloActivos: true
                };
        };

        $scope.onBuscar = function () {

            SweetAlert.showSpinner();

            ObrasSocialesServices.buscar($scope.busqueda.soloActivos)
                .then(function (resp) {

                    $scope.obrasSociales = resp.data;
                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onNuevo = function () {
            GestionesDeObrasSociales.editar(null, null, null);
        };

        $scope.onEditar = function (item) {
            GestionesDeObrasSociales.editar(item._id, $scope.onBuscar, null);
        };

        init();
    }

    angular.module('app.turnero').controller('editarObraSocialController', editarObraSocialController);
    editarObraSocialController.$inject = ['$scope', 'SweetAlert', 'ObrasSocialesServices', 'GestionesDeObrasSociales', '$uibModalInstance', 'idObraSocial'];
    function editarObraSocialController($scope, SweetAlert, ObrasSocialesServices, GestionesDeObrasSociales, $uibModalInstance, idObraSocial) {

        $scope.idObraSocial = idObraSocial;

        $scope.planEdicion = null;

        var init = function () {
            if ($scope.idObraSocial == null) {
                $scope.titulo = "Nueva obra social";
            } else {
                obtenerObraSocial();
            } 
        };

        var obtenerObraSocial = function () {

            ObrasSocialesServices.obtenerPorId($scope.idObraSocial)
                .then(function (resp) {

                    $scope.obraSocial = resp.data;
                    
                    $scope.titulo = "Obra social: " + $scope.obraSocial.nombre;

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        var validar = function () {
            var message = "";

            //if ($scope.profesional.nombre == null || $scope.profesional.nombre == "") message += "Debe ingresar un nombre para el profesional";
            //if ($scope.profesional.apellido == null || $scope.profesional.apellido == "") message += "Debe ingresar un apellido para el profesional";
            //if ($scope.profesional.prefijoProfesional == null || $scope.profesional.prefijoProfesional == "") message += "Debe ingresar el prefijo para el profesional";

            return { hasError: message != "", message: message };
        };

        $scope.editarPlan = function (plan) {
            $scope.planEdicion = angular.copy(plan);
        };

        $scope.nuevoPlan = function () {
            $scope.planEdicion = {};
        };

        $scope.cancelarEdicionPlan = function (plan) {
            $scope.planEdicion = null;
        };
        
        $scope.guardarPlan = function () {

            if ($scope.planEdicion == null) return;

            SweetAlert.showSpinner();

            $scope.planEdicion.idObraSocial = $scope.idObraSocial;

            ObrasSocialesServices.guardarPlanObraSocial($scope.planEdicion)
                .then(function (resp) {
                    $scope.planEdicion = null;
                    SweetAlert.closeSpinner();
                    obtenerPlanesDeObraSocial();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onGuardar = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            SweetAlert.showSpinner();

            ObrasSocialesServices.guardarObraSocial($scope.obraSocial)
                .then(function (resp) {

                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };

        var obtenerPlanesDeObraSocial = function () {

            ObrasSocialesServices.obtenerPlanesDeObraSocial($scope.idObraSocial)
                .then(function (resp) {
                    $scope.obraSocial.planes = resp.data;                    
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        init();
    }

    angular.module('app.turnero').service('GestionesDeObrasSociales', GestionesDeObrasSociales);
    GestionesDeObrasSociales.$inject = ['ObrasSocialesServices', '$uibModal'];
    function GestionesDeObrasSociales(ObrasSocialesServices, $uibModal) {

        this.editar = editar;

        //============================================================================

        function editar(idObraSocial, onSuccess, onCancel) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/obrasSociales/editarObraSocial.html',
                controller: 'editarObraSocialController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    idObraSocial: function () {
                        return idObraSocial;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };
    }

})();
