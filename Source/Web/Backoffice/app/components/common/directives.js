(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('selectConsultorios', selectConsultorios)
        .directive('selectProfesionales', selectProfesionales)
        .directive('selectEstadosDeTurnos', selectEstadosDeTurnos)
        .directive('selectPaciente', selectPaciente)
        .directive('selectEspecialidad', selectEspecialidad)
        .directive('selectTipoDeTurno', selectTipoDeTurno)
        .directive('seleccionPasoUnoNuevoTurno', seleccionPasoUnoNuevoTurno)
        .directive('seleccionPasoDosNuevoTurno', seleccionPasoDosNuevoTurno)
        .directive('seleccionPasoTresNuevoTurno', seleccionPasoTresNuevoTurno)
        .directive('confirmacionPasoCuatroNuevoTurno', confirmacionPasoCuatroNuevoTurno);


    selectConsultorios.$inject = ['GeneralServices'];
    function selectConsultorios(GeneralServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.selectConsultorios.view.html',
            link: link,
            scope: {
                dirModel: "=",
                optionItems: "=?",
                multiple: "@",
                allowClear: "@"
            }
        };

        function link(scope, element) {

            var init = function () {
                if (scope.optionItems == undefined) {
                    GeneralServices.getConsultorios()
                        .then(function (resp) {
                            scope.optionItems = resp.data
                        });
                }
            }

            init();
        }
    }

    selectProfesionales.$inject = ['GeneralServices'];
    function selectProfesionales(GeneralServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.selectProfesionales.view.html',
            link: link,
            scope: {
                dirModel: "=",
                optionItems: "=?",
                multiple: "@",
                allowClear: "@"
            }
        };

        function link(scope, element) {

            var init = function () {

                if (scope.optionItems == undefined) {
                    GeneralServices.getProfesionales()
                        .then(function (resp) {
                            scope.optionItems = resp.data
                        });
                }

            }

            init();
        }
    }

    selectEstadosDeTurnos.$inject = ['GeneralServices'];
    function selectEstadosDeTurnos(GeneralServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.selectEstadosDeTurnos.view.html',
            link: link,
            scope: {
                dirModel: "=",
                optionItems: "=?",
                multiple: "@",
                allowClear: "@"
            }
        };

        function link(scope, element) {

            var init = function () {

                if (scope.optionItems == undefined) {
                    GeneralServices.getEstadosDeTurnos()
                        .then(function (resp) {
                            scope.optionItems = resp.data

                            if (scope.multiple == "true") {
                                scope.dirModel.estadosTurnosSelected = scope.optionItems.filter(
                                    function (x) {
                                        return x.id == 1 || x.id == 2 || x.id == 3;
                                    }
                                );
                            }
                        });
                }
            }

            init();
        }
    }

    selectPaciente.$inject = ['GeneralServices'];
    function selectPaciente(GeneralServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.selectPaciente.view.html',
            link: link,
            scope: {
                dirModel: "=",
                optionItems: "=?"
            }
        };

        function link(scope, element) {

            scope.pacientes = [];
            scope.selected = { paciente: null };

            var init = function () {

            }

            scope.onPacienteSelected = function (paciente) {

                scope.dirModel = paciente;
            }

            scope.refreshPacientes = function (textoBuscado) {

                if (!textoBuscado) return;

                GeneralServices
                    .getPacientes(textoBuscado)
                    .then(function (resp) {

                        scope.pacientes = resp.data;
                    });
            };

            init();
        }
    }

    selectEspecialidad.$inject = ['GeneralServices'];
    function selectEspecialidad(GeneralServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.selectEspecialidad.view.html',
            link: link,
            scope: {
                dirModel: "=",
                optionItems: "=?",
                multiple: "@",
                allowClear: "@"
            }
        };

        function link(scope, element) {

            var init = function () {

                if (scope.optionItems == undefined) {

                    GeneralServices.getValoresSatelites([3]).then(function (resp) {
                        scope.optionItems = resp.data;
                    });
                }
            }

            init();
        }
    }

    selectTipoDeTurno.$inject = ['GeneralServices'];
    function selectTipoDeTurno(GeneralServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.selectTipoDeTurno.view.html',
            link: link,
            scope: {
                dirModel: "=",
                optionItems: "=?",
                multiple: "@",
                allowClear: "@"
            }
        };

        function link(scope, element) {

            var init = function () {

                if (scope.optionItems == undefined) {

                    var tipos = [];

                    tipos.push(
                        {
                            id: 1,
                            descripcion: 'Primera consulta'
                        });

                    tipos.push(
                        {
                            id: 2,
                            descripcion: '30 minutos'
                        });

                    tipos.push(
                        {
                            id: 3,
                            descripcion: 'M�s de 30 minutos'
                        });

                    tipos.push(
                        {
                            id: 4,
                            descripcion: 'Error llame al consultorio'
                        });

                    scope.optionItems = tipos;
                }
            }

            init();
        }
    }

    seleccionPasoUnoNuevoTurno.$inject = ['SweetAlert'];
    function seleccionPasoUnoNuevoTurno(SweetAlert) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.seleccionPasoUnoNuevoTurno.view.html',
            link: link,
            scope: {
                contexto: "="
            }
        };

        function link(scope, element) {

            scope.especialidad = {}; scope.consultorio = {}; scope.paciente = {}; scope.tipo = {};

            var init = function () {

                if (scope.contexto.paso1.datos.hasDatos) {
                    scope.especialidad = scope.contexto.paso1.especialidad;
                    scope.consultorio = scope.contexto.paso1.consultorio;

                    scope.paciente = scope.contexto.paso1.paciente;
                    scope.tipo = scope.contexto.paso1.tipo;
                }

            };

            scope.contexto.paso1.onSiguientePaso = function () {

                var validacion = validar();
                if (validacion.hasError) {
                    SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                    return;
                }
                else {
                    debugger;
                    scope.contexto.paso1.datos = {
                        idEspecialidad: scope.especialidad.especialidadSelected._id,
                        idConsultorio: scope.consultorio.consultoriosSelected._id,

                        idPaciente: scope.paciente._id,
                        idTipo: scope.tipo.tipoDeTurnoSelected.id,
                        hasDatos: true
                    };

                    scope.contexto.paso1.especialidad = scope.especialidad;
                    scope.contexto.paso1.consultorio = scope.consultorio;

                    scope.contexto.paso1.paciente = scope.paciente;
                    scope.contexto.paso1.tipo = scope.tipo;

                    scope.contexto.pasoActual++;
                }
            };

            var validar = function () {
                var message = "";

                if (scope.especialidad.especialidadSelected._id == null || scope.especialidad.especialidadSelected._id == "")
                    message += "Debe ingresar una especialidad para el paciente";

                if (scope.consultorio.consultoriosSelected._id == null || scope.consultorio.consultoriosSelected._id == "")
                    message += "Debe ingresar una especialidad para el paciente";

                if (scope.paciente._id == null || scope.paciente._id == "")
                    message += "Debe ingresar una especialidad para el paciente";

                if (scope.tipo.tipoDeTurnoSelected.id == null || scope.tipo.tipoDeTurnoSelected.id == "")
                    message += "Debe ingresar una especialidad para el paciente";

                return { hasError: message != "", message: message };
            };

            init();
        }

    }

    seleccionPasoDosNuevoTurno.$inject = ['SweetAlert'];
    function seleccionPasoDosNuevoTurno(SweetAlert) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.seleccionPasoDosNuevoTurno.view.html',
            link: link,
            scope: {
                contexto: "="
            }
        };

        function link(scope, element) {

            scope.modelo = {};
            scope.profesional = {};

            var init = function () {
                if (scope.contexto.paso2.datos.hasDatos) {
                    scope.modelo = scope.contexto.paso2.modelo;
                    scope.profesional = scope.contexto.paso2.profesional;
                }
            };

            scope.contexto.paso2.onSiguientePaso = function () {

                var validacion = validar();
                if (validacion.hasError) {
                    SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                    return;
                }
                else {
                    debugger;
                    scope.contexto.paso2.datos = {
                        idsProfesionales: scope.profesional.profesionalesSelected != null ? scope.profesional.profesionalesSelected.map(function (x) { return x._id }) : null,
                        desde: scope.modelo.periodo.desde,
                        hasta: scope.modelo.periodo.hasta,
                        hasDatos: true
                    };

                    scope.contexto.paso2.modelo = scope.modelo;
                    scope.contexto.paso2.profesional = scope.profesional;

                    scope.contexto.pasoActual++;
                }
            };

            var validar = function () {
                var message = "";
                var idsProfesionales = scope.profesional.profesionalesSelected != null ? scope.profesional.profesionalesSelected.map(function (x) { return x._id }) : null;

                if (idsProfesionales == null)
                    message += "Debe ingresar un profesional para el turno";


                return { hasError: message != "", message: message };
            };

            scope.filtrarDisponibilidadPorDia = function () {

                var idsDias = [];

                if (scope.modelo.diaLunes) idsDias.push(1);
                if (scope.modelo.diaMartes) idsDias.push(2);

                if (scope.modelo.diaMiercoles) idsDias.push(3);
                if (scope.modelo.diaJueves) idsDias.push(4);

                if (scope.modelo.diaViernes) idsDias.push(5);
                if (scope.modelo.diaSabado) idsDias.push(6);
                if (scope.modelo.diaDomingo) idsDias.push(7);

                scope.contexto.paso2.idsDias = idsDias;
            };


            init();
        }

    }

    seleccionPasoTresNuevoTurno.$inject = ['SweetAlert', 'TurnosServices'];
    function seleccionPasoTresNuevoTurno(SweetAlert, TurnosServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.seleccionPasoTresNuevoTurno.view.html',
            link: link,
            scope: {
                contexto: "="
            }
        };

        function link(scope, element) {

            scope.disponibilidades = {};

            var init = function () {
                if (scope.contexto.paso3.datos.hasDatos) {
                    scope.disponibilidadesOriginal = scope.contexto.paso3.disponibilidadesOriginal;
                    scope.disponibilidades = scope.contexto.paso3.disponibilidades;
                }

                scope.onObtenerDisponibilidades();

            };

            scope.contexto.paso3.onSiguientePaso = function () {

                var validacion = validar();
                if (validacion.hasError) {
                    SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                    return;
                }
                else {
                    debugger;
                    scope.contexto.paso3.datos = {
                        disponibilidadSelected: scope.disponibilidadSelected,
                        horarioSelected: scope.horarioSelected,
                        hasDatos: true
                    };

                    scope.contexto.paso3.disponibilidadesOriginal = scope.disponibilidadesOriginal;
                    scope.contexto.paso3.disponibilidades = scope.disponibilidades;

                    scope.contexto.pasoActual++;
                }
            };

            var validar = function () {
                var message = "";

                if (scope.disponibilidadSelected == null || scope.horarioSelected == null)
                    message += "Debe seleecionar fecha y hora para el turno";

                return { hasError: message != "", message: message };
            };

            scope.onObtenerDisponibilidades = function () {

                SweetAlert.showSpinner();

                var idsConsultorios = scope.contexto.paso1.datos.idConsultorio;
                var idsProfesionales = scope.contexto.paso2.datos.idsProfesionales;

                var desde = scope.contexto.paso2.datos.desde;
                var hasta = scope.contexto.paso2.datos.hasta;

                desde.setMonth(desde.getMonth() - 6);
                var idsDias = scope.contexto.paso2.idsDias;

                TurnosServices.getDisponibilidades(idsConsultorios, idsProfesionales, desde, hasta)
                    .then(function (resp) {

                        scope.disponibilidadesOriginal = resp.data.disponibilidades;
                        SweetAlert.closeSpinner();

                        scope.disponibilidades = scope.disponibilidadesOriginal.filter(function (x) {

                            var fecha = new Date(x.fechaDesde);
                            return idsDias.length == 0 || idsDias.indexOf(fecha.getDay()) >= 0;
                        });

                    }, function (resp) {
                        SweetAlert.closeSpinner();
                    });
            };

            scope.obtenerClaseSegunHorario = function (horario) {
                if (horario.incialParteDelDia == 'Ma') return 'bg-info';
                if (horario.incialParteDelDia == 'Me') return 'btn-warning';

                if (horario.incialParteDelDia == 'Si') return 'bg-deep-purple-500';
                if (horario.incialParteDelDia == 'Ta') return 'bg-pink-500';

                if (horario.incialParteDelDia == 'Nc') return 'btn-inverse';
                return 'btn-inverse';
            };

            scope.seleccionarDisponibilidad = function (horario, disponibilidad) {

                if (horario.selected) {
                    horario.selected = false;
                    horario.selectedClass = '';

                    scope.disponibilidadSelected = null;
                    scope.horarioSelected = null;
                }
                else {
                    horario.selected = true;
                    horario.selectedClass = 'bg-info';

                    scope.disponibilidadSelected = disponibilidad;
                    scope.horarioSelected = horario;
                }
            };


            init();
        }

    }

    confirmacionPasoCuatroNuevoTurno.$inject = ['SweetAlert', 'TurnosServices'];
    function confirmacionPasoCuatroNuevoTurno(SweetAlert, TurnosServices) {

        return {
            restrict: 'E',
            templateUrl: 'app/components/common/directive.confirmacionPasoCuatroNuevoTurno.view.html',
            link: link,
            scope: {
                contexto: "="
            }
        };

        function link(scope, element) {

            var init = function () {
                debugger;

                scope.paciente = scope.contexto.paso1.paciente;
                scope.especialidad = scope.contexto.paso1.especialidad.especialidadSelected;

                scope.consultorio = scope.contexto.paso1.consultorio.consultoriosSelected
                scope.tipo = scope.contexto.paso1.tipo.tipoDeTurnoSelected;

                scope.profesional = scope.contexto.paso2.profesional.profesionalesSelected[0];
                scope.horario = scope.contexto.paso3.datos.horarioSelected;

            };

            scope.contexto.paso4.onConfirmarPaso = function () {
                debugger;
                var idPaciente = scope.contexto.paso1.datos.idPaciente;
                var idEspecialidad = scope.contexto.paso1.datos.idEspecialidad;
                var idConsultorio = scope.contexto.paso1.datos.idConsultorio;

                var idTipo = scope.contexto.paso1.datos.idTipo;
                var idsProfesionales = scope.contexto.paso2.datos.idsProfesionales;
                var fechaHora = scope.contexto.paso3.datos.horarioSelected.fechaHoraDesde;

                var minutos = 1;                
                var celular = "";
                var email = "";
                var comentarios = "";

                SweetAlert.showSpinner();

                TurnosServices.crearTurno(idPaciente, idConsultorio, idProfesional, fechaHora, minutos, celular, email, comentarios)
                    .then(function (resp) {
                        debugger;
                        SweetAlert.closeSpinner();
                        if (resp.data != null) {

                        }

                    }, function (resp) {
                        SweetAlert.closeSpinner();
                        SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                    });

            };

            init();
        }

    }

})();

