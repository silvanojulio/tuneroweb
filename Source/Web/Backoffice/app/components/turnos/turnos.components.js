﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('buscarTurnosController', buscarTurnosController);
    buscarTurnosController.$inject = ['$scope', 'SweetAlert', 'GestionesDeTurnos', 'GeneralServices', 'TurnosServices', '$interval'];
    function buscarTurnosController($scope, SweetAlert, GestionesDeTurnos, GeneralServices, TurnosServices, $interval) {

        $scope.busqueda = {};
        $scope.actualizarAuto = true;

        var timer = null;

        var init = function () {
            $scope.idDisponibilidad = getUrlPar("idDisponibilidad");
            $scope.ocultarFiltros = $scope.idDisponibilidad != null;

            if ($scope.idDisponibilidad != null)
                $scope.onRefresh();

            $scope.onActualizarAuto();
                        
        };

        $scope.onNuevoTurno = function () {

            GestionesDeTurnos.crearTurno(null, null, null);
        };

        $scope.onRefresh = function (mostrarLoader) {

            var idsConsultorios = $scope.busqueda.consultoriosSelected ? $scope.busqueda.consultoriosSelected.map(function (x) { return x._id }) : null;
            var idsProfesionales = $scope.busqueda.profesionalesSelected ? $scope.busqueda.profesionalesSelected.map(function (x) { return x._id }) : null;
            var idsEstados = $scope.busqueda.estadosTurnosSelected ? $scope.busqueda.estadosTurnosSelected.map(function (x) { return x.id }) : null;
            var fechaDesde = $scope.busqueda.periodo ? $scope.busqueda.periodo.desde : null;
            var fechaHasta = $scope.busqueda.periodo ? $scope.busqueda.periodo.hasta : null;

            if ($scope.idDisponibilidad == null && (fechaDesde == null || fechaHasta == null)) {

                if (mostrarLoader == true)
                    SweetAlert.showError("Verifique las fechas del periodo de la búsqueda.", "Error");

                return;
            };

            if (mostrarLoader == true)
                SweetAlert.showSpinner();

            TurnosServices.buscarTurnos(idsConsultorios, idsProfesionales, idsEstados, fechaDesde, fechaHasta, $scope.idDisponibilidad)
                .then(function (resp) {
                    
                    $scope.turnos = resp.data.turnos;

                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.closeSpinner();
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };
        
        $scope.onActualizarAuto = function () {

            if ($scope.actualizarAuto === true) {
                timer = $interval($scope.onRefresh, 45000);
            } else {

                if (timer != null)
                    $interval.cancel(timer);

                timer = null;
            }
        };

        $scope.$on('$destroy', function () {
            if(timer != null)
                $interval.cancel(timer);
        });
        
        $scope.confirmarAsistencia = function (turno) {

            var mensaje = "Está seguro que desea confirmar la asistencia del paciente " + turno.paciente.nombreCompleto;

            cambiarDeEstado(mensaje, "Confirmar asistencia al turno", 5, turno);
        };

        $scope.cancelar = function (turno, porPaciente) {

            var mensaje = "Está seguro que desea cancelar " + (porPaciente ? "(por paciente)" : "(por consultorio)") + " el turno del paciente " + turno.paciente.nombreCompleto;

            cambiarDeEstado(mensaje, "Cancelar turno", porPaciente ? 7 : 4, turno);
        };

        $scope.noAsiste = function (turno) {

            var mensaje = "Está seguro que desea confirmar la inasistencia del paciente " + turno.paciente.nombreCompleto;

            cambiarDeEstado(mensaje, "Confirmar inasistencia al turno", 6, turno);
        };

        $scope.reasignar = function (turno) {
            GestionesDeTurnos.crearTurno(null, null, turno);
        };

        $scope.confirmarTurno = function (turno) {

            var mensaje = "Está seguro que desea confirmar el turno del paciente " + turno.paciente.nombreCompleto + "?";

            cambiarDeEstado(mensaje, "Confirmar turno", 2, turno);
        };

        var cambiarDeEstado = function (mensaje, titulo, estadoId, turno) {

            SweetAlert.promtSingleText(mensaje, titulo, 'Comentarios...', function (comentarios) {

                TurnosServices.cambiarDeEstado(turno._id, estadoId, comentarios)
                    .then(
                    function (resp) {
                        $scope.onRefresh();
                    },
                    function (resp) {
                        SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                    });

            });
        };

        $scope.eliminarProfesionalSecundario = function (turno) {

            SweetAlert.confirm("Está seguro que desea quitar el segundo profesional?", "Quitar segundo profesional", function () {

                TurnosServices.quitarProfesionalSecundario(turno._id)
                    .then(
                    function (resp) {
                        $scope.onRefresh();
                    },
                    function (resp) {
                        SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                    });

            });
        }

        $scope.obtenerColorDeFila = function (turno) {

            if (turno.estado == null) return "";
            if (turno.estado.id == 4 || turno.estado.id == 7) return "bg-amber-200"; //Cancelado
            if (turno.estado.id == 5) return "bg-green-200"; //Asistió
            if (turno.estado.id == 6) return "bg-red-300"; //Ausente

            return "";
        }

        $scope.verHistorial = function (turno) {
            GestionesDeTurnos.verHistorial(turno);
        };

        $scope.onAsignarProfesionalSecundario = function (turno) {

            GestionesDeTurnos.seleccionarProfesionalSecundario(turno,
                function () {
                    $scope.onRefresh();
                });
        };

        $scope.crearEnGoogleCalendar = function (turno) {

            SweetAlert.showSpinner();

            var t = turno;

            TurnosServices.replicarEnGoogleCalendar(turno._id)
                .then(
                function (resp) {
                    t.idGoogleCalendarEvent = " ";
                    SweetAlert.closeSpinner();
                },
                function (resp) {
                    SweetAlert.closeSpinner();
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onBuscarTurnoPaciente = function () {

            SweetAlert.showSpinner();
            
            var fechaDesde = new Date();
            fechaDesde.setDate(fechaDesde.getDate() - 1);

            var fechaHasta = new Date();
            fechaHasta.setMonth(fechaHasta.getMonth() - 6);

            TurnosServices.buscarTurnos(null, null, null, fechaDesde, fechaHasta, null)
                .then(function (resp) {

                    if (resp.data != null)
                        $scope.turnosAnteriores = resp.data.turnos;

                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.closeSpinner();
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });

            TurnosServices.obtenerProximosTurnosDeUsuario()
                .then(function (resp) {

                    if (resp.data != null)
                        $scope.turnosProximos = resp.data.turnos;

                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.closeSpinner();
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onNuevoTurnoPaciente = function () {
            window.location.href = "#/app/userNuevoTurno";
        }



        init();
    }

    angular.module('app.turnero').controller('calendarioTurnosController', calendarioTurnosController);
    calendarioTurnosController.$inject = ['$scope', 'SweetAlert', 'GestionesDeTurnos', 'GeneralServices', 'TurnosServices', '$interval', 'GestionesDeEventos', 'EventosServices'];
    function calendarioTurnosController($scope, SweetAlert, GestionesDeTurnos, GeneralServices, TurnosServices, $interval, GestionesDeEventos, EventosServices) {

        var timer = null;

        var init = function () {

            timer = $interval($scope.onRefresh, 45000);
        };

        $scope.onRefresh = function () {

            $scope.actions.refreshCalendar();
        };

        $scope.onAgregarEvento = function () {

            GestionesDeEventos.editEvento(function () {
                $scope.onRefresh();
            }, null, null);

        };

        var onBuscar = function (desde, hasta, callback) {

            var idsConsultorios = null;
            var idsProfesionales = null;
            var idsEstados = [1, 2, 3];
            var fechaDesde = desde;
            var fechaHasta = hasta;

            SweetAlert.showSpinner();

            var events = [];
            var turnosOk = false;
            var eventosOk = false;

            TurnosServices.buscarTurnos(idsConsultorios, idsProfesionales, idsEstados, fechaDesde, fechaHasta, $scope.idDisponibilidad)
                .then(function (resp) {

                    $scope.turnos = resp.data.turnos;
                    
                    angular.forEach($scope.turnos, function (t) {

                        events.push(
                            {
                                title: "Turno: " + t.paciente.nombreCompleto + ' / ' + t.profesional.nombreCompleto +
                                        (t.profesionalSecundario != null ? ' (' + t.profesionalSecundario.nombreCompleto + ')' : ''),
                                turno : t,
                                start: new Date(t.fechaHoraInicio),
                                end: new Date(t.fechaHoraFin),
                                id: t._id,
                                color: t.profesional.color == null ? 'green' : t.profesional.color
                            }
                        );
                    });

                    turnosOk = true;

                    if (eventosOk)
                        callback(events);

                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.closeSpinner();
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });

            EventosServices.obtenerEventosPorFechas(fechaDesde, fechaHasta)
                .then(function (resp) {

                    $scope.eventos = resp.data;

                    angular.forEach($scope.eventos, function (e) {

                        events.push(
                            {
                                title: 'Evento: ' +e.titulo,
                                evento: e,
                                start: new Date(e.fechaHoraInicio),
                                end: new Date(e.fechaHoraFin),
                                id: e._id,
                                color: e.color == null ? 'orange' : e.color
                            }
                        );
                    });

                    eventosOk = true;

                    if (turnosOk)
                        callback(events);
                    
                }, function (resp) {
                    SweetAlert.closeSpinner();
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        //Function que recibe la directiva del calendario cada vez que necesita obtener los eventos para mostrar en un determinado periodo
        $scope.getEvents = function (start, end, timezone, callback) {
            
            onBuscar(start._d, end._d, callback);
        };

        $scope.actions = { refreshCalendar: null };

        $scope.actions.onEventClick = function (calEvent, jsEvent, view) {

            if (calEvent.turno!=null)
                GestionesDeTurnos.verHistorial(calEvent.turno);
            else if (calEvent.evento != null)
                GestionesDeEventos.editEvento(function () {
                    $scope.onRefresh();
                }, null, calEvent.evento._id);

        };

        $scope.$on('$destroy', function () {
            if (timer != null)
                $interval.cancel(timer);
        });

        init();
    }
    
    angular.module('app.turnero').controller('crearTurnoController', crearTurnoController);
    crearTurnoController.$inject = ['$scope', 'SweetAlert', 'GestionesDeTurnos', '$uibModalInstance', 'TurnosServices', 'turnoReasignado'];
    function crearTurnoController($scope, SweetAlert, GestionesDeTurnos, $uibModalInstance, TurnosServices, turnoReasignado) {

        $scope.turnoReasignado = turnoReasignado;
        $scope.esReasignacion = $scope.turnoReasignado != null;

        $scope.modelo = {};

        var init = function () {

            if ($scope.esReasignacion) {
                $scope.detallesTurnoReasignado = "Reasignación de turno: " + $scope.turnoReasignado.paciente.nombreCompleto +
                    ". " + new Date($scope.turnoReasignado.fechaHoraInicio).toLocaleString() +
                    ". " + $scope.turnoReasignado.profesional.nombreCompleto +
                    ". " + $scope.turnoReasignado.consultorio.nombre;

            }

        };

        $scope.onObtenerDisponibilidades = function () {

            SweetAlert.showSpinner();

            var idsConsultorios = $scope.modelo.consultoriosSelected != null ? [$scope.modelo.consultoriosSelected._id] : null;
            var idsProfesionales = $scope.modelo.profesionalesSelected != null ? $scope.modelo.profesionalesSelected.map(function (x) { return x._id }) : null;
            var desde = $scope.modelo.periodo.desde;
            var hasta = $scope.modelo.periodo.hasta;

            TurnosServices.getDisponibilidades(idsConsultorios, idsProfesionales, desde, hasta)
                .then(function (resp) {

                    $scope.disponibilidadesOriginal = resp.data.disponibilidades;
                    $scope.filtrarDisponibilidadPorDia();
                    SweetAlert.closeSpinner();

                }, function (resp) {
                    SweetAlert.closeSpinner();
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };

        $scope.obtenerClaseSegunHorario = function (horario) {

            if (horario.incialParteDelDia == 'Ma') return 'bg-info';
            if (horario.incialParteDelDia == 'Me') return 'btn-warning';
            if (horario.incialParteDelDia == 'Si') return 'bg-deep-purple-500';
            if (horario.incialParteDelDia == 'Ta') return 'bg-pink-500';
            if (horario.incialParteDelDia == 'Nc') return 'btn-inverse';
            return 'btn-inverse';

        };

        $scope.seleccionarDisponibilidad = function (horario, disponibilidad) {

            $scope.disponibilidadSelected = disponibilidad;
            $scope.horarioSelected = horario;

            GestionesDeTurnos.confirmarTurno(
                {
                    idProfesional: disponibilidad.idProfesional,
                    nombreProfesional: disponibilidad.nombreProfesional
                }
                , {
                    idConsultorio: disponibilidad.idConsultorio,
                    nombreConsultorio: disponibilidad.nombreConsultorio
                }, new Date(horario.fechaHoraDesde), disponibilidad, $scope.onConfirmarTurno, null, $scope.turnoReasignado);
        };

        $scope.onConfirmarTurno = function () {
            $uibModalInstance.close();
        };

        $scope.filtrarDisponibilidadPorDia = function () {

            var idsDias = [];

            if ($scope.modelo.diaLunes) idsDias.push(1);
            if ($scope.modelo.diaMartes) idsDias.push(2);
            if ($scope.modelo.diaMiercoles) idsDias.push(3);
            if ($scope.modelo.diaJueves) idsDias.push(4);
            if ($scope.modelo.diaViernes) idsDias.push(5);
            if ($scope.modelo.diaSabado) idsDias.push(6);
            if ($scope.modelo.diaDomingo) idsDias.push(7);

            $scope.modelo.disponibilidades = $scope.disponibilidadesOriginal.filter(function (x) {

                var fecha = new Date(x.fechaDesde);
                return idsDias.length == 0 || idsDias.indexOf(fecha.getDay()) >= 0;
            });
        };

        init();
    }

    angular.module('app.turnero').controller('confirmarTurnoController', confirmarTurnoController);
    confirmarTurnoController.$inject = ['$scope', 'SweetAlert', 'GestionesDeTurnos', '$uibModalInstance', 'TurnosServices',
        'profesional', 'consultorio', 'fechaHoraDesde', 'disponibilidadSeleccionada', 'turnoReasignado'];
    function confirmarTurnoController($scope, SweetAlert, GestionesDeTurnos, $uibModalInstance, TurnosServices,
        profesional, consultorio, fechaHoraDesde, disponibilidadSeleccionada, turnoReasignado) {

        $scope.turnoReasignado = turnoReasignado;
        $scope.esReasignacion = $scope.turnoReasignado != null;

        $scope.disponibilidadSeleccionada = disponibilidadSeleccionada;

        $scope.modeloConfirmacion = {
            seleccionandoPaciente: true,
            pacienteSeleccionado: null,
            cantidadesDisponibles: [15, 30, 45, 60, 75, 90, 120],
            cantidadTurnos: 1,
            consultorio: consultorio,
            profesional: profesional,
            fechaHoraDesde: fechaHoraDesde,
            diaSeleccionado: fechaHoraDesde.toLocaleString('es-us', { weekday: 'long' }),
            cantidadMinutos: 1

        };

        var init = function () {

            $scope.modeloConfirmacion.cantidadMinutos = $scope.modeloConfirmacion.cantidadTurnos;

            if ($scope.esReasignacion == true) {
                $scope.modeloConfirmacion.seleccionandoPaciente = false;
                $scope.modeloConfirmacion.pacienteSeleccionado = $scope.turnoReasignado.paciente;
                $scope.modeloConfirmacion.celular = $scope.turnoReasignado.nroCelular;
                $scope.modeloConfirmacion.email = $scope.turnoReasignado.email;

                $scope.detallesTurnoReasignado = "Reasignación de turno: " + $scope.turnoReasignado.paciente.nombreCompleto +
                    ". " + new Date($scope.turnoReasignado.fechaHoraInicio).toLocaleString() +
                    ". " + $scope.turnoReasignado.profesional.nombreCompleto +
                    ". " + $scope.turnoReasignado.consultorio.nombre;
            }

        };

        $scope.onConfirmarPaciente = function () {
            $scope.modeloConfirmacion.seleccionandoPaciente = false;
        };

        $scope.onEditarPaciente = function () {
            $scope.modeloConfirmacion.seleccionandoPaciente = true;
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };

        var validar = function () {
            var message = "";

            //if ($scope.paciente.nombre == null || $scope.paciente.nombre == "") message += "Debe ingresar un nombre para el paciente";
            //if ($scope.paciente.apellido == null || $scope.paciente.apellido == "") message += "Debe ingresar un apellido para el paciente";

            return { hasError: message != "", message: message };
        };

        $scope.onConfirmarTurno = function () {

            if ($scope.esReasignacion) reasignarTurno();
            else crearTurno();

        };

        var crearTurno = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            var idPaciente = $scope.modeloConfirmacion.pacienteSeleccionado._id;
            var idProfesional = $scope.modeloConfirmacion.profesional.idProfesional;
            var idConsultorio = $scope.modeloConfirmacion.consultorio.idConsultorio;
            var minutos = $scope.modeloConfirmacion.cantidadTurnos * 1;
            var fechaHora = $scope.modeloConfirmacion.fechaHoraDesde;
            var celular = $scope.modeloConfirmacion.celular;
            var email = $scope.modeloConfirmacion.email;
            var comentarios = $scope.modeloConfirmacion.comentarios;

            SweetAlert.showSpinner();

            TurnosServices.crearTurno(idPaciente, idConsultorio, idProfesional, fechaHora, minutos, celular, email, comentarios)
                .then(function (resp) {

                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        }

        var reasignarTurno = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            var idTurno = $scope.turnoReasignado._id;
            var idProfesional = $scope.modeloConfirmacion.profesional.idProfesional;
            var idConsultorio = $scope.modeloConfirmacion.consultorio.idConsultorio;
            var fechaHora = $scope.modeloConfirmacion.fechaHoraDesde;

            SweetAlert.showSpinner();

            TurnosServices.reasignarTurno(idTurno, fechaHora, idProfesional, idConsultorio)
                .then(function (resp) {

                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        }

        init();
    }

    angular.module('app.turnero').controller('verHistorialTurnoController', verHistorialTurnoController);
    verHistorialTurnoController.$inject = ['$scope', '$uibModalInstance', 'turno', 'SweetAlert', 'TurnosServices', 'GestionesDeTurnos' ];
    function verHistorialTurnoController($scope, $uibModalInstance, turno, SweetAlert, TurnosServices, GestionesDeTurnos) {

        $scope.turno = turno;

        $scope.onAsignarProfesionalSecundario = function (turno) {

            GestionesDeTurnos.seleccionarProfesionalSecundario(turno,
                function () {
                    $scope.close();
                });
        };

        $scope.agregarObservacion = function () {

            SweetAlert.promtSingleText("Escriba las observaciones:", "Agregar observaciones al turno", 'Observaciones...',
                function (comentarios) {

                    if (comentarios != null && comentarios!= false){

                        SweetAlert.showSpinner();

                        TurnosServices.agregarObservacionesATurno(turno._id, comentarios)
                            .then(
                            function (resp) {
                                SweetAlert.closeSpinner();
                            },
                            function (resp) {
                                SweetAlert.closeSpinner();
                                SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                            });
                     }   

            });
        };
        
        $scope.confirmarAsistencia = function (turno) {

            var mensaje = "Está seguro que desea confirmar la asistencia del paciente " + turno.paciente.nombreCompleto;

            cambiarDeEstado(mensaje, "Confirmar asistencia al turno", 5, turno);
        };

        $scope.cancelar = function (turno, porPaciente) {

            var mensaje = "Está seguro que desea cancelar " + (porPaciente ? "(por paciente)" : "(por consultorio)") + " el turno del paciente " + turno.paciente.nombreCompleto;

            cambiarDeEstado(mensaje, "Cancelar turno", porPaciente ? 7 : 4, turno);
        };

        $scope.noAsiste = function (turno) {

            var mensaje = "Está seguro que desea confirmar la inasistencia del paciente " + turno.paciente.nombreCompleto;

            cambiarDeEstado(mensaje, "Confirmar inasistencia al turno", 6, turno);
        };

        $scope.reasignar = function (turno) {
            GestionesDeTurnos.crearTurno(null, null, turno);
        };

        $scope.confirmarTurno = function (turno) {

            var mensaje = "Está seguro que desea confirmar el turno del paciente " + turno.paciente.nombreCompleto + "?";

            cambiarDeEstado(mensaje, "Confirmar turno", 2, turno);
        };

        var cambiarDeEstado = function (mensaje, titulo, estadoId, turno) {

            SweetAlert.promtSingleText(mensaje, titulo, 'Comentarios...', function (comentarios) {

                TurnosServices.cambiarDeEstado(turno._id, estadoId, comentarios)
                    .then(
                    function (resp) {
                        $uibModalInstance.close();
                    },
                    function (resp) {
                        SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                    });

                });
        };
    }

    angular.module('app.turnero').controller('seleccionarProfesionalSecundario', seleccionarProfesionalSecundario);
    seleccionarProfesionalSecundario.$inject = ['$scope', '$uibModalInstance', 'turno', 'TurnosServices', 'SweetAlert'];
    function seleccionarProfesionalSecundario($scope, $uibModalInstance, turno, TurnosServices, SweetAlert) {

        $scope.turno = turno;
        $scope.profesional = {};

        $scope.onConfirmar = function () {

            if ($scope.profesional.profesionalesSelected == null) return;

            SweetAlert.showSpinner();

            TurnosServices.asignarProfesionalSecundario($scope.turno._id, $scope.profesional.profesionalesSelected._id).
                then(function (resp) {
                    SweetAlert.closeSpinner();
                    $uibModalInstance.close();
                }, function (resp) {

                    SweetAlert.showError(resp.data.error, "Error");
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };
    }

    angular.module('app.turnero').controller('solicitarTurnoPacienteController', solicitarTurnoPacienteController);
    solicitarTurnoPacienteController.$inject = ['$scope', 'SweetAlert'];
    function solicitarTurnoPacienteController($scope, SweetAlert) {

        $scope.contexto = {
            pasoActual: 0,
            paso1: {
                //onMostrarPaso = null,
                onSiguientePaso: null,
                datos: {}
            },
            paso2: {
                //onMostrarPaso = null,
                onSiguientePaso: null,
                datos: {}
            },
            paso3: {
                //onMostrarPaso = null,
                onSiguientePaso: null,
                datos: {}
            },
            paso4: {
                //onMostrarPaso = null,
                onConfirmarPaso: null,
                datos: {}
            },
        };        


        var init = function () {
            
            $scope.contexto.pasoActual = 1;

        };

        $scope.onSiguiente = function () {
            
            switch ($scope.contexto.pasoActual) {
                case 1:                    
                    if ($scope.contexto.paso1.onSiguientePaso != null) {
                        $scope.contexto.paso1.onSiguientePaso();
                    }
                    break;

                case 2:                    
                    if ($scope.contexto.paso2.onSiguientePaso != null) {
                        $scope.contexto.paso2.onSiguientePaso();
                    }
                    break;

                case 3:
                    if ($scope.contexto.paso3.onSiguientePaso != null) {
                        $scope.contexto.paso3.onSiguientePaso();
                    }
                    break;

                default: 
                    break;
            }
        };

        $scope.onAnterior = function () {
            $scope.contexto.pasoActual--;

        };

        $scope.onConfirmar = function () {

            if ($scope.contexto.pasoActual == 4 && $scope.contexto.paso4.onConfirmarPaso != null) {
                $scope.contexto.paso4.onConfirmarPaso();
            }
        };

        init();
    }

    angular.module('app.turnero').service('GestionesDeTurnos', GestionesDeTurnos);
    GestionesDeTurnos.$inject = ['$uibModal'];
    function GestionesDeTurnos($uibModal) {

        this.crearTurno = crearTurno;
        this.confirmarTurno = confirmarTurno;
        this.verHistorial = verHistorial;
        this.seleccionarProfesionalSecundario = seleccionarProfesionalSecundario;

        //============================================================================

        function crearTurno(onSuccess, onCancel, turnoReasignado) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/turnos/crearTurno.html',
                controller: 'crearTurnoController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    turnoReasignado: function () { return turnoReasignado; }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };

        function confirmarTurno(profesional, consultorio, fechaHoraDesde, disponibilidadSeleccionada, onSuccess, onCancel, turnoReasignado) {

            var modalInstance = $uibModal.open({

                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/turnos/crearTurno.confirmacion.html',
                controller: 'confirmarTurnoController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    profesional: function () {
                        return profesional;
                    },
                    consultorio: function () {
                        return consultorio;
                    },
                    fechaHoraDesde: function () {
                        return fechaHoraDesde;
                    },
                    disponibilidadSeleccionada: function () {
                        return disponibilidadSeleccionada;
                    },
                    turnoReasignado: function () { return turnoReasignado; }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };

        function verHistorial(turno) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/turnos/verHistorial.html',
                controller: 'verHistorialTurnoController',
                size: 'lg',
                resolve: {
                    turno: function () { return turno; }
                }
            });

        };

        function seleccionarProfesionalSecundario(turno, onSuccess) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/turnos/seleccionarProfesionalSecundario.html',
                controller: 'seleccionarProfesionalSecundario',
                size: 'md',
                resolve: {
                    turno: function () { return turno; }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            });
        };
    }
    
})();
