﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('buscarProfesionalesController', buscarProfesionalesController);
    buscarProfesionalesController.$inject = ['$scope', 'SweetAlert', 'ProfesionalesServices', 'GestionesDeProfesionales'];
    function buscarProfesionalesController($scope, SweetAlert, ProfesionalesServices, GestionesDeProfesionales) {

        var init = function () {
            $scope.profesionales = null;
            $scope.busqueda =
                {
                    nombre: null,
                    activo: true
                };
        };

        $scope.onBuscar = function () {

            SweetAlert.showSpinner();

            ProfesionalesServices.buscar($scope.busqueda.nombre)
                .then(function (resp) {

                    $scope.profesionales = resp.data;
                    SweetAlert.closeSpinner();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onNuevo = function () {
            GestionesDeProfesionales.editar(null, null, null);
        };

        $scope.onEditar = function (item) {
            GestionesDeProfesionales.editar(item._id, $scope.onBuscar, null);
        };

        init();
    }

    angular.module('app.turnero').controller('editarProfesionalController', editarProfesionalController);
    editarProfesionalController.$inject = ['$scope', 'SweetAlert', 'ProfesionalesServices', 'GestionesDeProfesionales',
        '$uibModalInstance', 'idProfesional', 'GeneralServices'];
    function editarProfesionalController($scope, SweetAlert, ProfesionalesServices, GestionesDeProfesionales,
        $uibModalInstance, idProfesional, GeneralServices) {

        $scope.idProfesional = idProfesional;
        $scope.profesional = {};

        var init = function () {

            GeneralServices.getValoresSatelites([3]).then(function (resp) {
                $scope.especialidades = resp.data;
                setEspecialidades();
            });

            if ($scope.idProfesional == null) {
                $scope.titulo = "Nuevo profesional";
            } else {
                obtenerProfesional();
            } 
            
        };

        var setEspecialidades = function () {

            if ($scope.profesional == null || $scope.profesional.idsEspecialidades == null || $scope.especialidades == null) return;

            angular.forEach($scope.especialidades, function (x) {
                x.selected = $scope.profesional.idsEspecialidades.indexOf(x.idValor) >= 0;
            });
        };

        var obtenerProfesional = function () {
            ProfesionalesServices.obtenerPorId($scope.idProfesional)
                .then(function (resp) {

                    $scope.profesional = resp.data.profesional;

                    if ($scope.profesional.usuario != null) {
                        $scope.profesional.email = $scope.profesional.usuario.email;
                        $scope.profesional.nroCelular = $scope.profesional.usuario.nroCelular;
                    }
                    
                    $scope.titulo = "Profesional: " + $scope.profesional.nombreCompleto;
                    setEspecialidades();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        var validar = function () {
            var message = "";

            if ($scope.profesional.nombre == null || $scope.profesional.nombre == "") message += "Debe ingresar un nombre para el profesional";
            if ($scope.profesional.apellido == null || $scope.profesional.apellido == "") message += "Debe ingresar un apellido para el profesional";
            if ($scope.profesional.prefijoProfesional == null || $scope.profesional.prefijoProfesional == "") message += "Debe ingresar el prefijo para el profesional";

            return { hasError: message != "", message: message };
        };

        $scope.onGuardar = function () {

            var validacion = validar();
            if (validacion.hasError) {
                SweetAlert.showError(validacion.message, "Verifique los datos ingresados");
                return;
            }

            SweetAlert.showSpinner();

            $scope.profesional.idsEspecialidades = $scope.especialidades.filter(x => x.selected == true).map(x => x.idValor);

            ProfesionalesServices.guardar($scope.profesional)
                .then(function (resp) {

                    SweetAlert.closeSpinner();

                    $uibModalInstance.close();
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };
        
        init();
    }

    angular.module('app.turnero').service('GestionesDeProfesionales', GestionesDeProfesionales);
    GestionesDeProfesionales.$inject = ['ProfesionalesServices', '$uibModal'];
    function GestionesDeProfesionales(ProfesionalesServices, $uibModal) {

        this.editar = editar;

        //============================================================================

        function editar(idProfesional, onSuccess, onCancel) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/profesionales/editarProfesional.html',
                controller: 'editarProfesionalController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    idProfesional: function () {
                        return idProfesional;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (onSuccess != null) onSuccess();
            }, function () {
                if (onCancel != null) onCancel();
            });
        };
    }

})();
