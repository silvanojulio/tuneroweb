﻿(function () {
    'use strict';

    angular
        .module('app.user')
        .controller('loginController', loginController);

    loginController.$inject = ['$scope', 'LoginServices', 'SweetAlert', '$cookies', '$uibModal'];

    function loginController($scope, LoginServices, SweetAlert, $cookies, $uibModal) {
        var vm = this;
        var atInitTime = new Date();

        var init = function () {

            procesarConfirmacionDeCuenta();
        };

        $scope.onValidar = function () {
            SweetAlert.showSpinner();
            LoginServices.login($scope.account.name, $scope.account.password)
                .then(function (resp) {
                    processResponse(resp);
                }, function () {
                    SweetAlert.showError("Se ha producido un error", "Error");
                });
        };

        $scope.loginWithGoogle = function (googleResponse) {

            var now = new Date();

            if ((now - atInitTime) < 2000 ) {
                var auth2 = gapi.auth2.getAuthInstance();
                try {
                    auth2.signOut();
                } catch (e) { }
                return;
            }

            var profile = googleResponse.getBasicProfile();

            var request = {
                email: profile.getEmail(),
                googleToken: googleResponse.getAuthResponse().id_token,
                apellido: profile.getFamilyName(),
                nombre: profile.getName(),
                googleId: profile.getId(),
                imgUrl: profile.getImageUrl()
            };

            SweetAlert.showSpinner();

            LoginServices.loginWithGoogle(request)
                .then(function (resp) {
                    processResponse(resp);
                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error", "Error");
                });

        };

        var processResponse= function (resp) {
            if (resp.status == 200) {

                var usuario = resp.data.currentUser;
                var token = resp.data._id;

                $cookies.putObject("CURRENT_USER", usuario);
                $cookies.put("CURRENT_TOKEN", token);

                SweetAlert.showSuccess("Bienvenido " + (usuario.googleProfile != null ? (usuario.googleProfile.nombre + " " + usuario.googleProfile.apellido) : usuario.email));
                window.location.href = "/";

            } else
                SweetAlert.showError(resp.data.message, "Error al ingresar al sistema");
        };

        $scope.onCreateNewUser = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/components/login/crearUsuarioApp.html',
                controller: 'createUsuerController',
                size: 'md',
                backdrop: false
            });

            modalInstance.result.then(function (nuevoUsuario) {
                $scope.account.name = nuevoUsuario.email;
                $scope.account.password = nuevoUsuario.password;
            });
        };

        var procesarConfirmacionDeCuenta = function () {

            if (getUrlParam("activarcuenta") !== "true") return;

            var email = getUrlParam("email");
            var codigo = getUrlParam("codigo");

            SweetAlert.showSpinner();
            LoginServices.confirmarCuenta(email, codigo)
                .then(function (resp) {

                    SweetAlert.showSuccess("Su cuenta ha sido confirmada. Por favor ingrese al sistema con sus credenciales", "Confirmación");

                }, function (resp) {
                    if (resp.data.error != null)
                        SweetAlert.showError(resp.data.error, "Error al confimar la cuenta");
                    else
                        SweetAlert.showError("Se ha producido un error", "Error");
                });
        };

        init();
    }

})();

(function () {
    'use strict';

    angular
        .module('app.user')
        .controller('createUsuerController', createUsuerController);

    createUsuerController.$inject = ['$scope', 'LoginServices', 'SweetAlert', '$cookies', '$uibModalInstance'];

    function createUsuerController($scope, LoginServices, SweetAlert, $cookies, $uibModalInstance) {
        var vm = this;
        
        var init = function () {
        };

        var validate = function () {
            var validation = { isError: false, message: "" };

            if ($scope.usuario.email == null || $scope.usuario.email.indexOf('@') < 0)
            {
                validation.isError = true;
                validation.message = "Ingrese un email válido. "
            }

            if ($scope.usuario.password == null || $scope.usuario.password.length < 4) {
                validation.isError = true;
                validation.message += "Ingrese una contraseña con más de 4 caracteres. "
            }

            if ($scope.usuario.password != $scope.usuario.passwordRepeticion ) {
                validation.isError = true;
                validation.message += "Las contraseñas no son iguales. Repita la contraseña. "
            }

            if ($scope.usuario.phone == null || $scope.usuario.phone.length < 8){
                validation.isError = true;
                validation.message += "Ingrese un número de teléfono válido. "
            }

            return validation;
        };

        $scope.onConfirmar = function () {
            var v = validate();

            if (v.isError) {
                SweetAlert.showError("Error: " + v.message, "Error en el ingreso de datos");
                return;
            }

            SweetAlert.showSpinner();

            LoginServices.createUserApp($scope.usuario.email, $scope.usuario.password, $scope.usuario.phone)
                .then(function (resp) {
                    $uibModalInstance.close(
                        {
                            email: $scope.usuario.email,
                            password: $scope.usuario.password,
                            phone: $scope.usuario.phone
                        }
                    );

                    SweetAlert.showSuccess("El usuario se ha creado exitosamente", "Confirmación");
                    
                }, function (resp) {

                    if (resp.data.error != null) 
                        SweetAlert.showError(resp.data.error, "Error al crear el usuario");
                    else
                        SweetAlert.showError("Se ha producido un error", "Error");
                });
        };
        
        $scope.onCancelar = function () {
            $uibModalInstance.dismiss();
        };
        
        init();
    }

})();
