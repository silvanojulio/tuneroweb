﻿(function () {
    'use strict';

    angular.module('app.turnero').controller('homeController', homeController);
    homeController.$inject = ['$scope', 'SweetAlert', '$cookies', 'DashboardServices'];
    function homeController($scope, SweetAlert, $cookies, DashboardServices) {

        var init = function () {
            SweetAlert.showSpinner();
            DashboardServices.obtenerDashboard()
                .then(function (resp) {
                    $scope.dashboard = resp.data.dashboard;

                    setGraficos();

                    SweetAlert.closeSpinner();

                }, function (resp) {
                    SweetAlert.showError("Se ha producido un error. " + resp.data != null ? resp.data.error : "", "Error");
                });
        };

      

        var setGraficos = function () {
            setGraficoProfesionales();
            setGraficoEstados();
        };

        var _charEstados = null;
        var setGraficoEstados = function () {

            var items = $scope.dashboard.cantidadPorEstado;

            var descripciones = items.map(function (x) { return x.descripcion; });
            var valores = items.map(function (x) { return x.valor; });
            var colores = [materialColors.green.a100, materialColors.orange.a100, materialColors.red.a100,
                materialColors.lightBlue.a100, materialColors.yellow.a100, materialColors.purple.a100];

            var coloresBackground = [materialColors.green.a200, materialColors.orange.a200, materialColors.red.a200,
                materialColors.lightBlue.a200, materialColors.yellow.a200, materialColors.purple.a200];

            var myData =
                {
                    labels: descripciones,
                    datasets: [
                        {
                            data: valores,
                            backgroundColor: colores,
                            borderColor: coloresBackground,
                            borderWidth: 1
                        }],

                };

            if (_charEstados != null) {
                _charEstados.destroy();
            }

            var config = {
                type: 'pie',
                data: myData
            };

            _charEstados = _createBarChart($("#chart-estados"), config);

        };

        var _chartProfesionalesEstados = null;
        var setGraficoProfesionales = function () {

            var items = $scope.dashboard.cantidadPorProfesionalYEstado;

            var descripciones = items.map(function (item, index) { return item.descripcion; });

            var cantidadAsistio = items.map(function (item, index) { return item.valores[0]; });
            var cantidadCancelado = items.map(function (item, index) { return item.valores[1]; });
            var cantidadCanceladoPorPaciente = items.map(function (item, index) { return item.valores[2]; });
            var cantidadConfirmadoPorConsultorio = items.map(function (item, index) { return item.valores[3]; });
            var cantidadConfirmadoPorPaciente = items.map(function (item, index) { return item.valores[4]; });
            var cantidadNoAsistio = items.map(function (item, index) { return item.valores[5]; });

            var myData =
                {
                    labels: descripciones,
                    datasets: [
                        {
                            label: 'Asisitó',
                            data: cantidadAsistio,
                            backgroundColor: materialColors.green.a100,
                            borderColor: materialColors.green.a200,
                            borderWidth: 1
                        },
                        {
                            label: 'Cancelado',
                            data: cantidadCancelado,
                            backgroundColor: materialColors.orange.a100,
                            borderColor: materialColors.orange.a200,
                            borderWidth: 1
                        },
                        {
                            label: 'Cancelado por paciente',
                            data: cantidadCanceladoPorPaciente,
                            backgroundColor: materialColors.red.a100,
                            borderColor: materialColors.red.a200,
                            borderWidth: 1
                        },
                        {
                            label: 'Confirmado',
                            data: cantidadConfirmadoPorConsultorio,
                            backgroundColor: materialColors.lightBlue.a100,
                            borderColor: materialColors.lightBlue.a200,
                            borderWidth: 1
                        },
                        {
                            label: 'Pendiente confirmación',
                            data: cantidadConfirmadoPorPaciente,
                            backgroundColor: materialColors.yellow.a100,
                            borderColor: materialColors.yellow.a200,
                            borderWidth: 1
                        },
                        {
                            label: 'No asistió',
                            data: cantidadNoAsistio,
                            backgroundColor: materialColors.purple.a100,
                            borderColor: materialColors.purple.a200,
                            borderWidth: 1
                        }
                    ],
                };

            if (_chartProfesionalesEstados != null) {
                _chartProfesionalesEstados.destroy();
            }

            var config = {
                type: 'bar',
                data: myData,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            },
                            stacked: true
                        }],
                        xAxes: [{
                            stacked: true
                        }],
                    }
                }
            };

            _chartProfesionalesEstados = _createBarChart($("#chart-profesionales-estados"), config);

        };
        
        init();
    }
    

    function labelFormatter(label, series) {
        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
    }

    var _createBarChart = function (ctx, chartConfiguration) {

        var chartCreated = new Chart(ctx, chartConfiguration);

        return chartCreated;
    }
})();
