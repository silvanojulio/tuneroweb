﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SJSystems.Core.Utils
{
    public class SimpleEntity
    {
        //[MongoDB.Bson.Serialization.Attributes.BsonElement("Descripcion")]
        public string Descripcion { get; set; }

        //[MongoDB.Bson.Serialization.Attributes.BsonElement("Id")]
        public int Id { get; set; }
        
        public bool Igual(object obj)
        {
            return ((SimpleEntity)obj).Id == this.Id;
        }
        
    }
}
