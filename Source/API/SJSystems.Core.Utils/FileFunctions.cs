﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SJSystems.Core.Utils
{
    public class FileFunctions
    {
        public static string ReadStringLinesFromFile(string path)
        {
            var f = File.OpenRead(path);
            var st = new StringBuilder();
            String line;

            var file = new StreamReader(f);
            while ((line = file.ReadLine()) != null)
            {
                st.AppendLine(line);
            }
            f.Dispose();
            file.Dispose();

            return st.ToString();
        }
    }
}
