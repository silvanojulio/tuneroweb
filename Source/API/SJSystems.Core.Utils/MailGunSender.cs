﻿using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SJSystems.Core.Utils
{
    public class MailGunSender
    {        
        private string apiUrl = "https://api.mailgun.net/v3/";
        private string publicApiKey;
        private string secretApiKey;
        private string domain;

        private RestClient client;

        public MailGunSender(string _publicApiKey, string _secretApiKey, string _domain)
        {            
            publicApiKey = _publicApiKey;
            secretApiKey = _secretApiKey;
            domain = _domain;

            client = new RestClient();
            client.BaseUrl = new Uri(apiUrl);
            client.Authenticator = new HttpBasicAuthenticator("api", secretApiKey);
        }

        public async Task SendEmail(List<string> to, List<string> cc, string from, string subject, string htmlBodyContent,
            List<string> attachmentsPaths, List<Tuple<string, string>> inLineImagesPaths, List<string> tags, string emailId)
        {
            RestRequest request = new RestRequest();
            request.AddParameter("domain", domain);
            request.Resource = domain + "/messages";
            request.AddParameter("from", from);

            foreach (var item in to)
            {
                request.AddParameter("to", item);
            }

            if (cc != null)
                foreach (var item in cc)
                {
                    request.AddParameter("cc", item);
                }

            if (attachmentsPaths != null)
                foreach (var item in attachmentsPaths)
                {
                    request.AddFile("attachment", item);
                }

            if (inLineImagesPaths != null)
                foreach (var item in inLineImagesPaths)
                {
                    request.AddFile("inline", item.Item2); //inline=@files/cartman.jpg
                }

            request.AddParameter("subject", subject);
            request.AddParameter("html", htmlBodyContent);

            request.AddParameter("v:email_id", emailId);

            if (tags != null && tags.Any())
                foreach (var tag in tags)
                    request.AddParameter("o:tag", tag);

            request.Method = Method.POST;

            var result = client.ExecuteAsync(request, (r) =>
            {

                var deserializer = new JsonDeserializer();

                var response = deserializer.Deserialize<MailGunResponse>(r);

                if (r.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception("Se ha generado un error");
                }
            });
        }

        public async Task SendEmail(List<string> to, List<string> cc, string from, string subject, 
            string bodyFilePath, Dictionary<string, string> variables,
            List<string> attachmentsPaths, List<Tuple<string, string>> inLineImagesPaths, 
            List<string> tags, string emailId, string emailTemplateBasePath=null)
        {
            var messageBody = FileFunctions.ReadStringLinesFromFile(bodyFilePath);

            if (!string.IsNullOrEmpty(emailTemplateBasePath))
            {
                var templateBaseContent = FileFunctions.ReadStringLinesFromFile(emailTemplateBasePath);
                messageBody = templateBaseContent.Replace("{{contenidoEmail}}", messageBody);
            }

            messageBody = variables.Aggregate(messageBody, (current, value) => current.Replace("{{" + value.Key + "}}", value.Value));

            await SendEmail(to, cc, from, subject, messageBody, attachmentsPaths, inLineImagesPaths, tags, emailId);
        }
        
    }

    public class MailGunResponse
    {
        public int total_count { get; set; }
    }
}
