﻿using SJSystems.Core.MongoDb;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SJSystems.Core.Security
{
    public class User : MongoEntityBase
    {
        public string Email { get; set; }
        public string NroCelular { get; set; }
        public string Password { get; set; }
        public bool Activo { get; set; }
        public DateTime? FechaConfirmacionEmail { get; set; }
        public DateTime? FechaConfirmacionCelular { get; set; }
        public DateTime FechaAlta { get; set; }
        public string CodigoValidacionEmail { get; set; }
        public string CodigoValidacionCelular { get; set; }
        public RolDeUsuario Rol { get; set; }
        public GoogleProfile GoogleProfile { get; set; }     
        public List<string> IdsDevices { get; set; }
    }

    public class GoogleProfile
    {
        public string GoogleToken { get; set; }
        public string GoogleId { get; set; }
        public string ImgUrl { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
    }

    public class RolDeUsuario : SimpleEntity
    {
        public static RolDeUsuario Paciente { get { return new RolDeUsuario { Descripcion = "Paciente", Id = 1 }; } }
        public static RolDeUsuario Profesional { get { return new RolDeUsuario { Descripcion = "Profesional", Id = 2 }; } }
        public static RolDeUsuario Secretaria { get { return new RolDeUsuario { Descripcion = "Secretaria", Id = 3 }; } }
        public static RolDeUsuario Admin { get { return new RolDeUsuario { Descripcion = "Admin", Id = 4 }; } }

        public static List<RolDeUsuario> Todos => new List<RolDeUsuario>{ Paciente, Profesional, Secretaria, Admin };
        public static List<RolDeUsuario> SoloUsuariosPrivados => new List<RolDeUsuario> {Profesional, Secretaria, Admin };
        public static RolDeUsuario GetRol(RolesDeUsuario rol) { return Todos.FirstOrDefault(x => x.Id == (int)rol); }
        public static List<RolDeUsuario> GetRoles(List<RolesDeUsuario> roles)
        {
            var r = new List<RolDeUsuario>();

            foreach (var rol in roles)
                r.AddRange(GetRoles(rol));

            return r;
        }
        public static List<RolDeUsuario> GetRoles(RolesDeUsuario rol)
        {
            if (rol == RolesDeUsuario.Todos)
                return Todos;
            else if (rol == RolesDeUsuario.SoloUsuariosPrivados)
                return SoloUsuariosPrivados;
            else
                return Todos.Where(x => x.Id == (int) rol).ToList();
        }
    }

    public enum RolesDeUsuario
    {
        Paciente = 1,
        Profesional = 2,
        Secretaria = 3,
        Admin = 4,
        Todos = 100,
        SoloUsuariosPrivados = 101
    }
}
