﻿using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace SJSystems.Core.Security
{
    public class Session : MongoEntityBase
    {
        public User CurrentUser { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime UltimaActualizacion { get; set; }
        public bool Expirada { get; set; }
    }
}
