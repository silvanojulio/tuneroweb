﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SJSystems.Core.Security
{
    public class UserRepository : MongoRepositoryBase<User>
    {
        public UserRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "Users";
        }

        public User GetUserByEmail(string email, bool soloActivos = true)
        {
            var filter = Filter.Eq(x => x.Email, email);

            if (soloActivos)
            {
                filter = filter & Filter.Eq(x => x.Activo, true);
            }

            var r = Collection.Find(filter);

            return r.FirstOrDefault();
        }

        public async Task CrearUsuarioAdmin()
        {
            var currentAdmin = GetUserByEmail("silvanojulio@gmail.com");

            if (currentAdmin != null) throw new Exception("El usuario admin ya existe");

            var user = new User
            {
                Email = "silvanojulio@gmail.com",
                Activo = true,
                CodigoValidacionCelular = "0103",
                CodigoValidacionEmail = "0103",
                FechaConfirmacionCelular = DateTime.Now,
                FechaConfirmacionEmail = DateTime.Now,
                Password = "Lili0903",
                Rol = RolDeUsuario.Admin,
                FechaAlta = DateTime.Now
            };

            await Insert(user);
        }
        
        public async Task<List<User>> Buscar(string email, int idRol)
        {
            var filter = Filter.Eq(x => x.Rol.Id, idRol); ;

            if (!string.IsNullOrEmpty(email))
                filter = Filter.Regex(u => u.Email, new BsonRegularExpression("/" + email + "/i"));

            return await Collection.Find(filter).ToListAsync();
        }
    }
}
