﻿using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace SJSystems.Core.Security
{
    public class SessionRepository : MongoRepositoryBase<Session>
    {
        public SessionRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "Sessions";
        }        
    }
}
