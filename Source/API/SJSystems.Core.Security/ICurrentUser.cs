﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SJSystems.Core.Security
{
    public interface ICurrentUser
    {
        User GetCurrentUser();
    }
}
