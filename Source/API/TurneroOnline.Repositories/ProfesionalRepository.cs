﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class ProfesionalRepository : MongoRepositoryBase<Profesional>
    {        
        public ProfesionalRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "Profesionales";
        }

        public async Task<long> ObtenerCantidadDeActivos()
        {
            var filter = Filter.Eq(x => x.Activo, true);

            return await Collection.Find(filter).CountAsync();
        }

        public async Task<Profesional> ObtenerPorUserId(string id)
        {
            var filter = Filter.Eq(x => x.IdUsuario, id);
            return await GetFirst(filter);
        }

        public async Task<List<Profesional>> ObtenerProfesionalesActivos()
        {
            var filter = Filter.Eq(x => x.Activo, true);

            var profesionales = await GetList(filter);

            return profesionales.OrderBy(x => x.Apellido).ToList();
        }

        public async Task<List<Profesional>> Buscar(string nombre)
        {
            var filter = Filter.Regex(u => u.NombreCompleto, new BsonRegularExpression("/" + nombre + "/i"));

            return await GetList(filter);
        }
    }

    public class EventoCalendarioRepository : MongoRepositoryBase<EventoCalendario>
    {
        public EventoCalendarioRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "EventosCalendario";
        }

        public Task<List<EventoCalendario>> ObtenerPorFechas(DateTime desde, DateTime hasta)
        {
            var filter = Filter.Gte(x => x.fechaHoraInicio, desde) & Filter.Lte(x => x.fechaHoraFin, hasta);
            return  GetList(filter);
        }
        
    }
}
