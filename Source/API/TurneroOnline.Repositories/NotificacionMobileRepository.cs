﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;
using System.Linq;

namespace TurneroOnline.Repositories
{
    public class NotificacionMobileRepository : MongoRepositoryBase<NotificacionMobile>
    {        
        public NotificacionMobileRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "NotificacionesMobiles";
        }

        public async Task<List<NotificacionMobile>> ObtenerNotificaciones(string idUsuario, DateTime fechaUltimaNotificacion)
        {
            var filter = Filter.Eq(x => x.IdUsuario, idUsuario) & Filter.Gt(x => x.FechaCreada, fechaUltimaNotificacion);

            return (await GetList(filter)).OrderBy(x=>x.FechaCreada).ToList();
        }

        public async Task  CrearNotificacion(string titulo, string mensaje, string idUsuario)
        {
            var notificacion = new NotificacionMobile
            {
                IdUsuario = idUsuario,
                Titulo = titulo,
                Mensaje = mensaje,         
                FechaCreada = DateTime.Now
            };

            await Insert(notificacion);
        }
    }
}
