﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class ObraSocialRepository : MongoRepositoryBase<ObraSocial>
    {
        public ObraSocialRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "ObrasSociales";
            AuditChanges = true;
        }
        
    }
}
