﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{

    public class PacienteRepository : MongoRepositoryBase<Paciente>
    {
        public PacienteRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "Pacientes";
            AuditChanges = true;
        }

        public async Task<Paciente> ObtenerPacientePorDni(string dni)
        {
            var filter = Filter.Eq(x => x.Dni, dni) & Filter.Eq(x => x.Estado, EstadoPaciente.Activo);

            return await GetFirst(filter);
        }

        public async Task<long> ObtenerCantidadDeActivos()
        {
            var filter =  Filter.Eq(x => x.Estado, EstadoPaciente.Activo);

            return await Collection.Find(filter).CountAsync();
        }

        public async Task<List<Paciente>> BuscarPacientes(string nombreApellido, string dni, string nroFicha, string idUsuario)
        {
            FilterDefinition<Paciente> filter = Filter.Eq(x => x.Estado.Id, EstadoPaciente.Activo.Id);

            if (!string.IsNullOrEmpty(nombreApellido))
                filter = filter & Filter.Regex(u => u.NombreCompleto, new BsonRegularExpression("/" + nombreApellido + "/i"));

            if (!string.IsNullOrEmpty(dni))
            {
                filter = filter & Filter.Regex("Dni", dni);
            }

            if (!string.IsNullOrEmpty(nroFicha))
            {
                filter = filter & Filter.Eq(x => x.NroFicha, nroFicha);
            }

            if (!string.IsNullOrEmpty(idUsuario))
            {
                filter = filter & Filter.Eq(x => x.IdUsuario, idUsuario);
            }

            return await GetList(filter);
        }

        public async Task<Paciente> ObtenerPorUserId(string id)
        {
            var filter = Filter.Eq(x => x.IdUsuario, id) & Filter.Eq(x => x.EsPacienteUsuario, true);
            return await GetFirst(filter);
        }
        
        public async Task<List<Paciente>> BusquedaRapida(string textoBuscado, string idUsuario=null)
        {
            var texto = MongoEntityBase.EliminarCaracteresEspeciales(textoBuscado);
            var filter = Filter.Regex(u => u._TextoDeBusqueda, new BsonRegularExpression("/" + texto + "/i"));

            if (!string.IsNullOrEmpty(idUsuario))
            {
                filter = filter & Filter.Eq(u => u.IdUsuario, idUsuario);
            }

            return await GetList(filter);
        }
    }
}
