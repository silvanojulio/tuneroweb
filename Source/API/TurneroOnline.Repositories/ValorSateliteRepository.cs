﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class ValorSateliteRepository : MongoRepositoryBase<ValorSatelite>
    {        
        public ValorSateliteRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "ValoresSatelites";
        }

        public async Task<List<ValorSatelite>> ObtenerValoresDeTabla(TablasSatelites tablaSatelite)
        {
            var filter = Filter.Eq(x => x.IdTablaSatelite, (int)tablaSatelite);

            return await GetList(filter);
        }

        public async Task<List<ValorSatelite>> ObtenerValoresDeTabla(List<TablasSatelites> tablasSatelites)
        {
            var idsTablas = tablasSatelites.Select(x => (int)x).ToList();

            return await ObtenerValoresDeTabla(idsTablas);
        }

        public async Task<List<ValorSatelite>> ObtenerValoresDeTabla(List<int> idsTablas)
        {
            var filter = Filter.In(x => x.IdTablaSatelite, idsTablas);

            return await GetList(filter);
        }
    }
}
