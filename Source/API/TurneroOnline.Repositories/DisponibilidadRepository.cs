﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class DisponibilidadRepository : MongoRepositoryBase<Disponibilidad>
    {        
        public DisponibilidadRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "Disponibilidades";
            AuditChanges = true;
        }

        public async Task<List<Disponibilidad>> ObtenerDisponiblidadesDentroRango(DateTime fechaHoraInicio, DateTime fechaHoraFin, string idProfesional, string idConsultorio, bool esReceso)
        {
            var filter = (Filter.Lt(x => x.fechaHoraInicio, fechaHoraInicio) & Filter.Gt(x => x.fechaHoraFin, fechaHoraInicio)) |
                         (Filter.Lt(x => x.fechaHoraInicio, fechaHoraFin) & Filter.Gt(x => x.fechaHoraFin, fechaHoraFin));

            if (!string.IsNullOrEmpty(idProfesional))
                filter = filter & Filter.Eq(x => x.IdProfesional, idProfesional);

            if (!string.IsNullOrEmpty(idConsultorio))
                filter = filter & Filter.Eq(x => x.IdConsultorio, idConsultorio);

            filter = filter & Filter.Eq(x => x.EsReceso, esReceso) & filter & Filter.Eq(x => x.Estado.Id, EstadoDisponibilidad.Confirmado.Id);

            return await GetList(filter);
        }

        public async Task<List<Disponibilidad>> BuscarDisponibilidades(DateTime fechaHoraInicio, DateTime fechaHoraFin, string idConsultorio, List<string> idsProfesionales, int? idEstado)
        {
            var items = await BuscarDisponibilidades(fechaHoraInicio, fechaHoraFin, 
                        string.IsNullOrEmpty(idConsultorio) ? null: new List<string> {idConsultorio}, 
                        idsProfesionales, idEstado);

             return items;
        }

        public async Task<List<Disponibilidad>> BuscarDisponibilidades(DateTime fechaHoraInicio, DateTime fechaHoraFin, List<string> idsConsultorio, List<string> idsProfesionales, int? idEstado)
        {
            var filter = Filter.Gte(x => x.fechaHoraFin, fechaHoraInicio) & Filter.Lte(x => x.fechaHoraInicio, fechaHoraFin);

            if (idsProfesionales != null && idsProfesionales.Any())
                filter = filter & Filter.In(x => x.IdProfesional, idsProfesionales);

            if (idsConsultorio!=null && idsConsultorio.Any())
                filter = filter & Filter.In(x => x.IdConsultorio, idsConsultorio);

            if (idEstado.HasValue)
                filter = filter & Filter.Eq(x => x.Estado.Id, idEstado);

            var items = (await GetList(filter));

            return items;
        }
    }
}
