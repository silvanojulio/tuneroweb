﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Text;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class DataBaseContext
    {
        private IOptions<TurneroOnlineConfiguration> settings; 
        public DataBaseContext(IOptions<TurneroOnlineConfiguration> _settings)
        {
            settings = _settings;
            
        }

        private PacienteRepository _pacientes = null;
        public PacienteRepository Pacientes
        {
            get {
                if (_pacientes == null) _pacientes = new PacienteRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _pacientes;
            }
        }

        private UserRepository _usuarios = null;
        public UserRepository Usuarios
        {
            get
            {
                if (_usuarios == null) _usuarios = new UserRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _usuarios;
            }
        }

        private SessionRepository _sessiones = null;
        public SessionRepository Sessiones
        {
            get
            {
                if (_sessiones == null) _sessiones = new SessionRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _sessiones;
            }
        }

        private ValorSateliteRepository _valoresSatelites = null;
        public ValorSateliteRepository ValoresSatelites
        {
            get
            {
                if (_valoresSatelites == null) _valoresSatelites = new ValorSateliteRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _valoresSatelites;
            }
        }
        
        private ProfesionalRepository _profesionales = null;
        public ProfesionalRepository Profesionales
        {
            get
            {
                if (_profesionales == null) _profesionales = new ProfesionalRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _profesionales;
            }
        }

        private ConsultorioRepository _consultorios = null;
        public ConsultorioRepository Consultorios
        {
            get
            {
                if (_consultorios == null) _consultorios = new ConsultorioRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _consultorios;
            }
        }

        private DisponibilidadRepository _disponibilidades = null;
        public DisponibilidadRepository Disponibilidades
        {
            get
            {
                if (_disponibilidades == null) _disponibilidades = new DisponibilidadRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _disponibilidades;
            }
        }

        private TurnoRepository _turnos = null;
        public TurnoRepository Turnos
        {
            get
            {
                if (_turnos == null) _turnos = new TurnoRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _turnos;
            }
        }

        private ObraSocialRepository _obrasSociales = null;
        public ObraSocialRepository ObrasSociales
        {
            get
            {
                if (_obrasSociales == null) _obrasSociales = new ObraSocialRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _obrasSociales;
            }
        }

        private PlanDeObraSocialRepository _planesDeObraSocial = null;
        public PlanDeObraSocialRepository PlanesDeObraSocial
        {
            get
            {
                if (_planesDeObraSocial == null) _planesDeObraSocial = new PlanDeObraSocialRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _planesDeObraSocial;
            }
        }


        private NotificacionMobileRepository _notificacionesMobile = null;
        public NotificacionMobileRepository NotificacionesMobile
        {
            get
            {
                if (_notificacionesMobile == null) _notificacionesMobile = new NotificacionMobileRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _notificacionesMobile;
            }
        }
        
        private EventoCalendarioRepository _eventosCalendario = null;
        public EventoCalendarioRepository EventosCalendario
        {
            get
            {
                if (_eventosCalendario == null) _eventosCalendario = new EventoCalendarioRepository(settings.Value.ConnectionString, settings.Value.Database);
                return _eventosCalendario;
            }
        }

    }
}
