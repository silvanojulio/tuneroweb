﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class PlanDeObraSocialRepository : MongoRepositoryBase<PlanDeObraSocial>
    {        
        public PlanDeObraSocialRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "PlanesDeObraSocial";
            AuditChanges = true;
        }

        public async Task<List<PlanDeObraSocial>> ObtenerPlanesDeObraSocial(string idObraSocial)
        {
            var filter = Filter.Eq(x => x.IdObraSocial, idObraSocial);

            var planes = await GetList(filter);

            return planes.OrderBy(x => x.Nombre).ToList();
        }

        public async Task<List<PlanDeObraSocial>> ObtenerPlanesDeObraSocial(List<string> idsObrasSociales)
        {
            var filter = Filter.In(x => x.IdObraSocial, idsObrasSociales);

            var planes = await GetList(filter);

            return planes.OrderBy(x => x.Nombre).ToList();
        }
    }
}
