﻿using MongoDB.Bson;
using MongoDB.Driver;
using SJSystems.Core.MongoDb;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnline.Repositories
{
    public class TurnoRepository : MongoRepositoryBase<Turno>
    {        
        public TurnoRepository(string connectionString, string dataBase) : base(connectionString, dataBase)
        {
            CollectionName = "Turnos";
            AuditChanges = true;
        }

        public async Task<List<Turno>> BuscarTurnos(DateTime fechaHoraInicio, DateTime fechaHoraFin, 
            List<string> idsConsultorio, List<string> idsProfesionales, List<int> idsEstados,
            List<string> idsPacientes)
        {
            var filter = Filter.Gte(x => x.fechaHoraFin, fechaHoraInicio) & Filter.Lte(x => x.fechaHoraInicio, fechaHoraFin);

            if (idsProfesionales != null && idsProfesionales.Any())
                filter = filter & 
                    (Filter.In(x => x.IdProfesional, idsProfesionales) | 
                     Filter.In(x => x.IdProfesionalSecundario, idsProfesionales));

            if (idsConsultorio != null && idsConsultorio.Any())
                filter = filter & Filter.In(x => x.IdConsultorio, idsConsultorio);

            if (idsEstados != null && idsEstados.Any())
                filter = filter & Filter.In(x => x.Estado.Id, idsEstados);

            if (idsPacientes != null && idsPacientes.Any())
                filter = filter & Filter.In(x => x.IdPaciente, idsPacientes);

            var items = (await GetList(filter));

            return items;
        }

        public async Task<long> ObtenerCantidadDeTurnosPendientesDeConfirmacion()
        {
            DateTime fechaHoraInicio = DateTime.Now;
            DateTime fechaHoraFin = DateTime.Now.AddMonths(3);

            var filter = Filter.Gte(x => x.fechaHoraFin, fechaHoraInicio) & Filter.Lte(x => x.fechaHoraInicio, fechaHoraFin);
            filter = filter & Filter.Eq(x => x.Estado.Id, EstadoDeTurno.ConfirmadoPorPaciente.Id);

            return await Collection.Find(filter).CountAsync();
        }

        public async Task<long> ObtenerCantidadDeTurnosConfirmadosAFuturo()
        {
            DateTime fechaHoraInicio = DateTime.Now;
            DateTime fechaHoraFin = DateTime.Now.AddMonths(3);

            var filter = Filter.Gte(x => x.fechaHoraFin, fechaHoraInicio) & Filter.Lte(x => x.fechaHoraInicio, fechaHoraFin);
            filter = filter & Filter.Eq(x => x.Estado.Id, EstadoDeTurno.ConfirmadoPorConsultorio.Id);

            return await Collection.Find(filter).CountAsync();
        }

    }
}
