﻿using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TurneroOnline.Entities
{
    public class ValorSatelite : MongoEntityBase
    {
        public int IdValor { get; set; }
        public string TextoValor { get; set; }
        public string Descripcion { get; set; }
        public int IdTablaSatelite { get; set; }
    }

    public enum TablasSatelites
    {
        [Description("Provincias")]
        Provincias = 1,

        [Description("Motivos de receso")]
        MotivosDeReceso = 2,

        [Description("Especialidades")]
        Especialidades = 3,

        [Description("Dias de la semana")]
        DiasDeLaSemana = 4,

    }
}
