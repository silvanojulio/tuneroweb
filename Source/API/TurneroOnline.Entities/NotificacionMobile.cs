﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class NotificacionMobile : MongoEntityBase
    {
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Extra3 { get; set; }
        public string IdUsuario { get; set; }
        public DateTime FechaCreada { get; set; }
        public DateTime? FechaRecibida { get; set; }

        [BsonIgnore]
        public string Antiguedad{ get {

                var antiguedad = DateTime.UtcNow - FechaCreada.ToUniversalTime();

                if (antiguedad.Days >= 2 )
                    return string.Format("Hace {0} días", antiguedad.Days);

                if (antiguedad.Days >= 1)
                    return string.Format("Ayer");

                if (antiguedad.Minutes > 1)
                    return string.Format("Hace {0} minutos", antiguedad.Minutes);

                return string.Format("Recién");
            } }
    }
}
