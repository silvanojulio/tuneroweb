﻿using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Consultorio : MongoEntityBase
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public bool Activo { get; set; }
        public bool EsPrimeraCita { get; set; }
    }
}
