﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Periodo : MongoEntityBase
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime fechaHoraInicio { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime fechaHoraFin { get; set; }

        public string Horario
        {
            get
            {
                return fechaHoraInicio.ToString("HH:mm") + " - " + fechaHoraFin.ToString("HH:mm");
            }
            set { }
        }

        public int Minutos
        {
            get
            {
                var tiempo = (fechaHoraFin - fechaHoraInicio);
                return Convert.ToInt32(tiempo.TotalMinutes);
            }
            set { }
        }
    }
}
