﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class ObraSocial : MongoEntityBase
    {
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public bool Activo { get; set; }

        [BsonIgnore]
        public List<PlanDeObraSocial> Planes { get; set; }
    }

    public class PlanDeObraSocial : MongoEntityBase
    {
        public string IdObraSocial { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public bool Activo { get; set; }
    }

    public class EventoCalendario : Periodo
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Color { get; set; }
        public bool Realizado { get; set; }

    }
}
