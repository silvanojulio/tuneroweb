﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Disponibilidad : Periodo
    {
        public string IdProfesional { get; set; }
        public string IdConsultorio { get; set; }
        public EstadoDisponibilidad Estado { get; set; }
        public string Comentarios { get; set; }
        public ValorSatelite MotivoReceso { get; set; }
        public bool EsReceso { get; set; }
        public bool EsTodoElDia { get; set; }

        [BsonIgnore]
        public Profesional Profesional { get; set; }

        [BsonIgnore]
        public Consultorio Consultorio { get; set; }
    }

    public class EstadoDisponibilidad : SimpleEntity
    {
        [BsonIgnore]
        public static EstadoDisponibilidad Confirmado { get { return new EstadoDisponibilidad { Descripcion = "Confirmado", Id = 1 }; } }

        //[BsonIgnore]
        //public static EstadoDisponibilidad Eliminado { get { return new EstadoDisponibilidad { Descripcion = "Eliminado", Id = 2 }; } }

        [BsonIgnore]
        public static EstadoDisponibilidad Cancelado { get { return new EstadoDisponibilidad { Descripcion = "Cancelado", Id = 3 }; } }

        [BsonIgnore]
        public static List<EstadoDisponibilidad> Estados { get; set; } = new List<EstadoDisponibilidad> { Confirmado, Cancelado };

        public static EstadoDisponibilidad ObtenerEstadoPorId(int id)
        {
            return Estados.FirstOrDefault(x => x.Id == id);
        }
    }

    public class DiaHoraMinutoDisponibilidad
    {
        public DiaDeLaSemana dia { get; set; }
        public TimeSpan desde { get; set; }
        public TimeSpan hasta { get; set; }
    }
}
