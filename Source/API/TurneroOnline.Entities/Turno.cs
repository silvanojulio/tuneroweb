﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Turno : Periodo
    {
        public string IdPaciente { get; set; }
        public string IdConsultorio { get; set; }
        public string IdProfesional { get; set; }
        public string IdProfesionalSecundario { get; set; }
        public string IdDisponibilidad { get; set; }
        public EstadoDeTurno Estado { get; set; }
        public DateTime FechaRecordatorio { get; set; }
        public DateTime? FechaConfirmado { get; set; }
        public DateTime? FechaCancelado { get; set; }
        public string IdTurnoRelacionado { get; set; }
        public string Observaciones { get; set; }
        public bool CanceladoPorPaciente { get; set; }
        public List<string> Historial { get; set; } 

        public string IdGoogleCalendarEvent { get; set; }

        public string Email { get; set; }
        public string NroCelular { get; set; }

        [BsonIgnore]
        public Profesional Profesional { get; set; }

        [BsonIgnore]
        public Profesional ProfesionalSecundario { get; set; }

        [BsonIgnore]
        public Consultorio Consultorio { get; set; }

        [BsonIgnore]
        public Paciente Paciente { get; set; }

        [BsonIgnore]
        public Disponibilidad Disponiblidad{ get; set; }
        
        public void AgregarHistorial(string mensaje, string usuario)
        {
            if (this.Historial == null) this.Historial = new List<string>();

            this.Historial.Insert(0, string.Format("{0} hs - {1} - {2}", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), 
                                                usuario, mensaje));
        }
    }

    public class EstadoDeTurno : SimpleEntity
    {
        public static EstadoDeTurno Reservado { get { return new EstadoDeTurno { Descripcion = "Reservado", Id = 1 }; } }
        public static EstadoDeTurno ConfirmadoPorConsultorio { get { return new EstadoDeTurno { Descripcion = "Confirmado por consultorio", Id = 2 }; } }
        public static EstadoDeTurno ConfirmadoPorPaciente { get { return new EstadoDeTurno { Descripcion = "Solicitado por paciente", Id = 3 }; } }
        public static EstadoDeTurno Cancelado { get { return new EstadoDeTurno { Descripcion = "Cancelado", Id = 4 }; } }
        public static EstadoDeTurno CanceladoPorPaciente { get { return new EstadoDeTurno { Descripcion = "Cancelado por paciente", Id = 7 }; } }
        public static EstadoDeTurno Asistio { get { return new EstadoDeTurno { Descripcion = "Asistió", Id = 5 }; } }
        public static EstadoDeTurno NoAsistio { get { return new EstadoDeTurno { Descripcion = "No asistió", Id = 6 }; } }

        public static List<EstadoDeTurno> Estados = new List<EstadoDeTurno> { Reservado, ConfirmadoPorConsultorio, ConfirmadoPorPaciente, Cancelado, Asistio, NoAsistio, CanceladoPorPaciente };
        public static List<EstadoDeTurno> EstadosValidosParaDisponibilidad = new List<EstadoDeTurno> { Reservado, ConfirmadoPorConsultorio, ConfirmadoPorPaciente, Asistio};
        public static List<EstadoDeTurno> EstadosValidosParaPaciente = new List<EstadoDeTurno> { Reservado, ConfirmadoPorConsultorio, ConfirmadoPorPaciente};

        public static EstadoDeTurno ObtenerEstado(int idEstado)
        {
            return Estados.FirstOrDefault(x => x.Id == idEstado);
        }
    }
}
