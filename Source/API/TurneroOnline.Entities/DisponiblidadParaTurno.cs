﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class DisponiblidadParaTurno
    {
        private DateTime _fechaDesde;
        private DateTime _fechaHasta;
        private bool calcular = true;
        private List<HorarioDeDisponibilidadParaTurno> turnosDisponibles= null;

        public DateTime FechaDesde
        {
            get
            {
                return _fechaDesde;
            }
            set
            {
                calcular = true;
                _fechaDesde = value;
            }
        }
        public DateTime FechaHasta
        {
            get
            {
                return _fechaHasta;
            }
            set
            {
                calcular = true;
                _fechaHasta = value;
            }
        }

        public int MinutosDisponibles { get; set; }
        public string NombreProfesional { get; set; }
        public string IdProfesional { get; set; }
        public string NombreConsultorio { get; set; }
        public string IdConsultorio { get; set; }

        public string Horario { get {
                return FechaDesde.ToString("HH:mm") + " - " + FechaHasta.ToString("HH:mm");
            } set { } }

        public List<HorarioDeDisponibilidadParaTurno> TurnosDisponibles
        {
            get
            {
                if (!calcular) return this.turnosDisponibles;

                turnosDisponibles = new List<HorarioDeDisponibilidadParaTurno>();
               
                if(FechaDesde < FechaHasta)
                {
                    var horaActual = this.FechaDesde;

                    while (horaActual < FechaHasta)
                    {
                        turnosDisponibles.Add(new HorarioDeDisponibilidadParaTurno
                        {
                            FechaHoraDesde = horaActual,
                            FechaHoraHasta = horaActual.AddMinutes(15)
                        });

                        horaActual = horaActual.AddMinutes(15);
                    }
                }

                calcular = false;
                return turnosDisponibles;
            }
            set{}
        }
    }

    public class HorarioDeDisponibilidadParaTurno
    {
        public DateTime FechaHoraDesde { get; set; }
        public DateTime FechaHoraHasta { get; set; }

        public string Horario
        {
            get
            {
                return FechaHoraDesde.ToString("HH:mm") + " - " + FechaHoraHasta.ToString("HH:mm");
            }
            set { }
        }

        public int Minutos
        {
            get
            {
                return (FechaHoraHasta - FechaHoraDesde).Minutes;
            }
            set { }
        }

        public string IncialParteDelDia
        {
            get
            {
                if (FechaHoraDesde.Hour < 12) return "Ma";
                if (FechaHoraDesde.Hour < 14) return "Me";
                if (FechaHoraDesde.Hour < 16) return "Si";
                if (FechaHoraDesde.Hour < 20) return "Ta";
                return "Nc";
            }
            set { }
        }
    }
}
