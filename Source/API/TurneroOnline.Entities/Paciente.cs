﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using SJSystems.Core.Security;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Paciente : MongoEntityBase
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NombreCompleto { get { return string.Format("{0} {1}", Nombre, Apellido); } set { } }
        public string Dni { get; set; }
        public DateTime FechaDeNacimiento { get; set; }
        public string Domicilio { get; set; }
        public ValorSatelite Provincia { get; set; }
        public string Pais { get; set; }
        public string Ciudad { get; set; }
        public EstadoPaciente Estado { get; set; }
        public string IdUsuario { get; set; }
        public bool EsPacienteUsuario { get; set; }
        public string NroFicha { get; set; }
        public string IdProfesionalDefault { get; set; }
        public string IdObraSocial { get; set; }
        public string IdPlanObraSocial { get; set; }
        public string Celular { get; set; }

        public override void GenerarTextoDeBusqueda()
        {
            var texto = new StringBuilder();

            texto
                .Append(this.Nombre + " ")
                .Append(this.Apellido + " ")
                .Append(this.Nombre + " ")
                .Append(this.Dni + " ")
                .Append("f" + this.NroFicha + " ")
                .Append(this.FechaDeNacimiento.ToString("dd/MM/yyyy") + " ");

            this._TextoDeBusqueda = EliminarCaracteresEspeciales(texto.ToString());
        }

        [BsonIgnore]
        public User Usuario { get; set; }
        [BsonIgnore]
        public Profesional Profesional { get; set; }
        [BsonIgnore]
        public ObraSocial ObraSocial { get; set; }
        [BsonIgnore]
        public PlanDeObraSocial Plan { get; set; }
    }

    public class EstadoPaciente : SimpleEntity
    {
        public static EstadoPaciente Activo { get { return new EstadoPaciente { Descripcion = "Activo", Id = 1 }; } }
        public static EstadoPaciente Eliminado { get { return new EstadoPaciente { Descripcion = "Eliminado", Id = 2 }; } }
        public static EstadoPaciente Inactivo { get { return new EstadoPaciente { Descripcion = "Inactivo", Id = 3 }; } }
        
        public static List<EstadoPaciente> Estados = new List<EstadoPaciente>
            {
                Activo,
                Eliminado,
                Inactivo
            };

        public EstadoPaciente ObtenerPorId (int id)
        {
            return Estados.FirstOrDefault(x => x.Id == id);
        }

    }
}
