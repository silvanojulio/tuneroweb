﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurneroOnline.Entities
{
    public class DiaDeLaSemana: SimpleEntity
    {
        [BsonIgnore]
        public static DiaDeLaSemana Lunes { get { return new DiaDeLaSemana { Descripcion = "Lunes", Id = 1 }; } }
        [BsonIgnore]
        public static DiaDeLaSemana Martes { get { return new DiaDeLaSemana { Descripcion = "Martes", Id = 2 }; } }
        [BsonIgnore]
        public static DiaDeLaSemana Miercoles { get { return new DiaDeLaSemana { Descripcion = "Miércoles", Id = 3 }; } }
        [BsonIgnore]
        public static DiaDeLaSemana Juenes { get { return new DiaDeLaSemana { Descripcion = "Jueves", Id = 4 }; } }
        [BsonIgnore]
        public static DiaDeLaSemana Viernes { get { return new DiaDeLaSemana { Descripcion = "Viernes", Id = 5 }; } }
        [BsonIgnore]
        public static DiaDeLaSemana Sabado { get { return new DiaDeLaSemana { Descripcion = "Sábado", Id = 6 }; } }
        [BsonIgnore]
        public static DiaDeLaSemana Domingo { get { return new DiaDeLaSemana { Descripcion = "Domingo", Id = 0 }; } }

        public static List<DiaDeLaSemana> Dias { get { return new List<DiaDeLaSemana> {
            Lunes,Martes,Miercoles,Juenes,Viernes,Sabado,Domingo
        }; } }

        public static DiaDeLaSemana ObtenerDia(int dia)
        {
            return Dias.FirstOrDefault(x => x.Id == dia);
        }
    }
}
