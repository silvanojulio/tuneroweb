﻿using SJSystems.Core.MongoDb;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Log: MongoEntityBase
    {
        public TiposDeLog Tipo { get; set; }
        public string Funcionalidad { get; set; }
        public string Mensaje { get; set; }
        public DateTime FechaHora { get; set; }
        public User Usuario { get; set; }
    }

    public enum TiposDeLog
    {
        [Description("Error")]
        Error = 1,

        [Description("Información")]
        Info = 2,

        [Description("Advertencia")]
        Advertencia = 2,

    }
}
