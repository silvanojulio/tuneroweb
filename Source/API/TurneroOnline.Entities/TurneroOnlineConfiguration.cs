﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class TurneroOnlineConfiguration
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public string NombreEmpresa { get; set; }
        public string UrlSistema { get; set; }
        public string EmailFrom { get; set; }
        public string MailGunSecretKey { get; set; }
        public string MailGunPublicKey { get; set; }
        public string MailGunDomain { get; set; }
        public string PathTemplates { get; set; }
        public string GoogleClientId { get; set; }
        public string GoogleSecret { get; set; }
        public string AccountServiceEmail { get; set; }
        public string ApplicationName { get; set; }
        public string DiferenciaHorariaCalendar { get; set; }
    }
}
