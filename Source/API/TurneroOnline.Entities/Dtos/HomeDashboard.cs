﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities.Dtos
{
    public class HomeDashboard
    {
        public long CantidadDePacientes { get; set; }
        public long CantidadDeProfesionales { get; set; }
        public long CantidadDeTurnosPendientesDeConfirmacion { get; set; }
        public long CantidadDeTurnosPendientesDeAtencion { get; set; }

        public List<DatoParaGrafico<long>> CantidadPorProfesionalYEstado { get; set; }
        public List<DatoParaGrafico<long>> CantidadPorEstado { get; set; }
    }

    public class DatoParaGrafico<T>
    {
        public string Descripcion { get; set; }
        public T Valor { get; set; }
        public List<T> Valores { get; set; }
    }
}
