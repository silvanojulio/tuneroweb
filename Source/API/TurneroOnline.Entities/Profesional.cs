﻿using MongoDB.Bson.Serialization.Attributes;
using SJSystems.Core.MongoDb;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurneroOnline.Entities
{
    public class Profesional : MongoEntityBase
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string PrefijoProfesional { get; set; }
        public string DetalleDeProfesional { get; set; }
        public string NombreCompleto { get { return PrefijoProfesional + " " + Apellido + ", " + Nombre; } set { } }
        public string IdUsuario { get; set; }
        public bool Activo { get; set; }
        public bool EsPrimeraCita { get; set; }
        public string Color { get; set; }
        public List<int> IdsEspecialidades { get; set; }

        public override void GenerarTextoDeBusqueda()
        {
            var texto = new StringBuilder();

            texto
                .Append(this.Nombre + " ")
                .Append(this.Apellido + " ")
                .Append(this.Nombre + " ")
                .Append(this.PrefijoProfesional + " ");

            this._TextoDeBusqueda = EliminarCaracteresEspeciales(texto.ToString());
        }

        [BsonIgnore]
        public User Usuario { get; set; }

        [BsonIgnore]
        public List<ValorSatelite> Especialidades { get; set; }
    }
}
