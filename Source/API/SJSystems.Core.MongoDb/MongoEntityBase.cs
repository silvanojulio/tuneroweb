﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SJSystems.Core.MongoDb
{
    public class MongoEntityBase 
    {
        public string _id { set; get; }

        public bool IsNew { get { return string.IsNullOrEmpty(_id); } }

        public bool Igual(object obj)
        {
            return ((MongoEntityBase)obj)._id == this._id;
            
        }

        public string _TextoDeBusqueda
        {
            get; set;
        }

        public virtual void GenerarTextoDeBusqueda()
        {

        }
        
        public static string EliminarCaracteresEspeciales(string texto)
        {
            return string.IsNullOrEmpty(texto) ? texto :
                    texto
                    .Replace("Á", "A")
                    .Replace("á", "a")
                    .Replace("É", "E")
                    .Replace("é", "e")
                    .Replace("Í", "I")
                    .Replace("í", "i")
                    .Replace("Ó", "O")
                    .Replace("ó", "o")
                    .Replace("Ú", "U")
                    .Replace("ú", "U")
                    .Replace("  ", " ")
                    ;
        }
    }
}
