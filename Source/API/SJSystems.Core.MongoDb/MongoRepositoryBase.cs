﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SJSystems.Core.MongoDb
{
    public class MongoRepositoryBase<T> : IMongoRepositoryBase<T> where T : MongoEntityBase
    {
        #region Config

        protected bool AuditChanges { get; set; }

        private readonly IMongoDatabase _database = null;

        private string ConnectionString { get; set; }
        private string DataBaseName { get; set; }
        protected virtual string CollectionName { get; set; }

        protected IMongoCollection<T> Collection
        {
            get { return _database.GetCollection<T>(CollectionName); }
        }

        /// <summary>
        /// Gets the type of the element(s) that are returned when the expression tree associated with this instance of IQueryable is executed.
        /// </summary>
        public Type ElementType => this.Collection.AsQueryable<T>().ElementType;

        /// <summary>
        /// Gets the expression tree that is associated with the instance of IQueryable.
        /// </summary>
        public Expression Expression => this.Collection.AsQueryable<T>().Expression;

        /// <summary>
        /// Gets the query provider that is associated with this data source.
        /// </summary>
        public IQueryProvider Provider => this.Collection.AsQueryable<T>().Provider;

        public MongoRepositoryBase(string connectionString, string dataBase)
        {
            ConnectionString = connectionString;
            DataBaseName = dataBase;

            var client = new MongoClient(connectionString);
            if (client != null)
                _database = client.GetDatabase(dataBase);
        }

        /// <summary>
        /// Filter for queries
        /// </summary>
        public FilterDefinitionBuilder<T> Filter
        {
            get
            {
                return Builders<T>.Filter;
            }
        }
        #endregion

        #region General methods

        public async Task<DeleteResult> DeleteAsync(string id)
        {
            return await Collection.DeleteOneAsync(Builders<T>.Filter.Eq("_id", id));
        }

        public async Task<IAsyncCursor<T>> GetAllASync()
        {
            BsonDocument filter = new BsonDocument();

            var options = new FindOptions<T>
            {
                // Our cursor is a tailable cursor and informs the server to await
                //CursorType = CursorType.TailableAwait, 
                NoCursorTimeout = true,
                MaxAwaitTime = new TimeSpan(168, 0, 0)
            };

            var cursor = await Collection.FindAsync(filter, options);

            return cursor;
        }

        public async Task<T> GetByIdAsync(string id)
        {
            var filter = FilterDefinition(id);

            var obj = await Collection.Find(filter).FirstOrDefaultAsync();

            return obj;
        }

        public async Task<List<T>> GetByIdsAsync(List<string> ids)
        {
            var filter = Filter.In(x => x._id, ids);

            var items = await GetList(filter);

            return items;
        }

        public async Task<T> Insert(T newObject)
        {
            try
            {
                newObject.GenerarTextoDeBusqueda();
                newObject._id = ObjectId.GenerateNewId().ToString();

                if (this.AuditChanges) CreateAuditRecord(newObject);

                await Collection.InsertOneAsync(newObject);

                return newObject;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<ReplaceOneResult> Update(T entity)
        {
            try
            {
                entity.GenerarTextoDeBusqueda();

                if (this.AuditChanges) CreateAuditRecord(entity);

                return await Collection
                            .ReplaceOneAsync(n => n._id.Equals(entity._id)
                                            , entity
                                            , new UpdateOptions { IsUpsert = true });
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<List<T>> GetList(FilterDefinition<T> filter)
        {
            return await Collection.Find(filter).ToListAsync();
        }

        public async Task<T> GetFirst(FilterDefinition<T> filter)
        {
            return await Collection.Find(filter).FirstOrDefaultAsync();
        }

        public T GetFirstSync(FilterDefinition<T> filter)
        {
            return Collection.Find(filter).FirstOrDefault();
        }

        public async Task<List<T>> BusquedaRapida(string textoBuscado)
        {
            var texto = MongoEntityBase.EliminarCaracteresEspeciales(textoBuscado);
            var filter = Filter.Regex(u => u._TextoDeBusqueda, new BsonRegularExpression("/" + texto + "/i"));
            
            return await GetList(filter);
        }
        #endregion

        #region Audit

        public void CreateAuditRecord(T entity)
        {
            var auditRepo = new AuditEntityRepository<T>(this.ConnectionString, this.DataBaseName, "AuditEntities");

            var auditRecord = new AuditEntity<T>
            {
                Collection = CollectionName,
                IdEntity = entity._id,
                Record = entity,
                FechaHora = DateTime.UtcNow,
            };

            auditRepo.Insert(auditRecord);
        }

        public async Task<List<AuditEntity<T>>> GetAuditRecords(string _id)
        {
            var auditRepo = new AuditEntityRepository<T>(this.ConnectionString, this.DataBaseName, "AuditEntities");
            return await auditRepo.GetAuditRecordsByEntityId(_id, this.CollectionName);
        }

        #endregion

        #region Private

        private static FilterDefinition<T> FilterDefinition(string id)
        {
            var filter = Builders<T>.Filter.Eq("_id", id);

            return filter;
        }

        #endregion
    }
}
