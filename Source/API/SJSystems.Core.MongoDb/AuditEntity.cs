﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SJSystems.Core.MongoDb
{
    public class AuditEntity<T> : MongoEntityBase
    {
        public T Record { get; set; }
        public string IdEntity { get; set; }
        public DateTime FechaHora { get; set; }
        public string Collection { get; set; }
        public string Usuario{get;set;}
        public string IdUsuario { get; set; }
        public string Detalles { get; set; }
    }
}
