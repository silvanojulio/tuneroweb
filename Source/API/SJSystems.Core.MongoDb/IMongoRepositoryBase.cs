﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SJSystems.Core.MongoDb
{
    public interface IMongoRepositoryBase<T> where T : MongoEntityBase
    {
        Task<DeleteResult> DeleteAsync(string id);
        Task<IAsyncCursor<T>> GetAllASync();
        Task<T> GetByIdAsync(string id);
        Task<T> Insert(T newObject);
        Task<ReplaceOneResult> Update(T entity);
    }

    
}
