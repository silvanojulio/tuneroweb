﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SJSystems.Core.MongoDb
{
    public class AuditEntityRepository<T> : MongoRepositoryBase<AuditEntity<T>>
    {
        public AuditEntityRepository(string connectionString, string dataBase, string collectionNAme) : base(connectionString, dataBase)
        {
            CollectionName = collectionNAme;
            AuditChanges = false;
        }

        public async Task<List<AuditEntity<T>>> GetAuditRecordsByEntityId(string _id, string collection)
        {
            var filter = Filter.Eq(x => x.Collection, collection) & Filter.Eq(x => x.IdEntity, _id);

            return await GetList(filter);
        }
    }
}
