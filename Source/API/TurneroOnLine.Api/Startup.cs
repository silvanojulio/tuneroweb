﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TurneroOnline.Entities;
using TuneroOnline.Managers;
using TurneroOnLine.Api.Components;
using Microsoft.AspNetCore.Http;
using SJSystems.Core.Security;
using Newtonsoft.Json;

namespace TurneroOnLine.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            // Add framework services.
            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(GetRequestTokenFilter));
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            });

            services.Configure<TurneroOnlineConfiguration>(options =>
            {
                options.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database = Configuration.GetSection("MongoConnection:Database").Value;

                options.NombreEmpresa = Configuration.GetSection("Empresa:Nombre").Value;
                options.PathTemplates = Configuration.GetSection("Empresa:PathTemplates").Value;
                options.UrlSistema = Configuration.GetSection("Empresa:UrlSistema").Value;

                options.EmailFrom = Configuration.GetSection("MailGun:EmailFrom").Value;
                options.MailGunDomain = Configuration.GetSection("MailGun:MailGunDomain").Value;
                options.MailGunSecretKey = Configuration.GetSection("MailGun:MailGunSecretKey").Value;
                options.MailGunPublicKey = Configuration.GetSection("MailGun:MailGunPublicKey").Value;

                options.GoogleClientId = Configuration.GetSection("GoogleConfig:ClientId").Value;
                options.GoogleSecret = Configuration.GetSection("GoogleConfig:Secret").Value;
                options.ApplicationName = Configuration.GetSection("GoogleConfig:ApplicationName").Value;
                options.AccountServiceEmail = Configuration.GetSection("GoogleConfig:AccountServiceEmail").Value;
                options.DiferenciaHorariaCalendar = Configuration.GetSection("GoogleConfig:DiferenciaHorariaCalendar").Value;
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<ICurrentUser, CurrentUser>();
            services.AddTransient<ISessionManager, SessionManager>();
            services.AddTransient<IUserManager, UserManager>();
            services.AddTransient<IPacientesManager, PacientesManager>();
            services.AddTransient<IValorSateliteManager, ValorSateliteManager>();
            services.AddTransient<IDisponiblidadManager, DisponiblidadManager>();
            services.AddTransient<IConsultorioManager, ConsultorioManager>(); 
            services.AddTransient<IProfesionalManager, ProfesionalManager>();
            services.AddTransient<ITurnoManager, TurnoManager>();
            services.AddTransient<IObraSocialManager, ObraSocialManager>(); 
            services.AddTransient<INotificacionesMobileManager, NotificacionesMobileManager>();
            services.AddTransient<IDashboardManager, DashboardManager>();
            services.AddTransient<IEventoCalendarioManager, EventoCalendarioManager>();
            services.AddTransient<IDisponiblidadManager, DisponiblidadManager>();
            services.AddTransient<IGoogleFirebaseManager, GoogleFirebaseManager>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("CorsPolicy");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }
    }
}
