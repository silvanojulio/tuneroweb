﻿using Microsoft.AspNetCore.Http;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components
{
    public class CurrentUser : ICurrentUser
    {
        IHttpContextAccessor _httpAccessor;

        public const string CURRENT_SESSION = "CURRENT_SESSION";

        public CurrentUser(IHttpContextAccessor httpAccessor)
        {
            _httpAccessor = httpAccessor;
        }

        public User GetCurrentUser()
        {
            if (!_httpAccessor.HttpContext.Items.ContainsKey(CURRENT_SESSION)) return null;

            return ((Session)_httpAccessor.HttpContext.Items[CURRENT_SESSION]).CurrentUser;
        }
    }
}
