﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TuneroOnline.Managers;

namespace TurneroOnLine.Api.Components
{
    public class GetRequestTokenFilter : IActionFilter
    {
        public const string CURRENT_TOKEN = "CURRENT_TOKEN";
        const string HEADER_TOKEN = "SECURITYTOKEN";
        const string SESSION_MANAGER = "SESSION_MANAGER";

        private string _token;
        private string _currentSession;

        ISessionManager _sessionManager;

        public GetRequestTokenFilter(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
           
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Items.Add(SESSION_MANAGER, _sessionManager);

            //Si la request no tiene header 
            if (!context.HttpContext.Request.Headers.ContainsKey(HEADER_TOKEN))
                return;

            _token = context.HttpContext.Request.Headers[HEADER_TOKEN];

            //Si la request no tiene header 
            if (string.IsNullOrEmpty(_token))
                return;

            context.HttpContext.Items.Add(CURRENT_TOKEN, _token);
        }
    }
}
