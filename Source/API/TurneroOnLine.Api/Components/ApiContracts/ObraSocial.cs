﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class GuardarObraSocialRequest
    {
        public string _id { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public bool Activo { get; set; }
    }

    public class GuardarPlanObraSocialRequest
    {
        public string _id { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public bool Activo { get; set; }
        public string IdObraSocial { get; set; }
    }
}
