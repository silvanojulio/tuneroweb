﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class ContractBase
    {
    }

    public class RequestBase: ContractBase
    {
        public string source { get; set; }
    }

    public class ResponseBase: ContractBase
    {
        public int error { get; set; } = 0;
    }
}
