﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class GuardarEventoRequest : RequestBase
    {
        public string Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Color { get; set; }
        public bool Realizado { get; set; }

        public DateTime FechaHoraInicio { get; set; }
        public DateTime FechaHoraFin { get; set; }
    }

    public class EliminarEventoRequest : RequestBase
    {
        public string IdEvento { get; set; }
    }
}
