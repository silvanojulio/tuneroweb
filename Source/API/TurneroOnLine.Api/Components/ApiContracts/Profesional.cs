﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class GuardarProfesionalRequest
    {
        public string _id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string PrefijoProfesional { get; set; }
        public string DetalleDeProfesional { get; set; }
        public bool Activo { get; set; }
        public bool EsPrimeraCita { get; set; }
        public string Color { get; set; }

        public string email { get; set; }
        public string password { get; set; }
        public string nroCelular { get; set; }

        public List<int> idsEspecialidades { get; set; }

    }
}
