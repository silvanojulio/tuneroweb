﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class LoginRequest:RequestBase
    {
        public string email { get; set; }
        public string password { get; set; }        
    }

    public class LoginWithGoogleRequest : RequestBase
    {
        public string email { get; set; }
        public string googleToken { get; set; }
        public string apellido { get; set; }
        public string nombre { get; set; }
        public string googleId { get; set; }
        public string imgUrl { get; set; }
    }

    public class CambioDePasswordRequest : RequestBase
    {
        public string password { get; set; }
        public string idUsuario { get; set; }
    }

    public class AsignarNroDeCelularRequest : RequestBase
    {
        public string nroCelular { get; set; }
        public string idUsuario { get; set; }
    }
    
    public class CrearUsuarioApp : RequestBase
    {
        public string email { get; set; }
        public string nroCelular { get; set; }
        public string password { get; set; }
    }

    public class ResetPasswordAppRequest : RequestBase
    {
        public string email { get; set; }
    }

    public class ConfirmarCuentaPorEmailRequest : RequestBase
    {
        public string email { get; set; }
        public string codigo { get; set; }
    }

    public class GuardarUsuarioRequest : RequestBase
    {
        public UsuarioDto usuario { get; set; }
    }

    public class UserDeviceRequest : RequestBase
    {
        public bool agregar { get; set; }
        public string idDevice { set; get; }
        public string idUsuario { set; get; }
    }

    public class UsuarioDto
    {
        public string _id { get; set; }
        public string email { get; set; }
        public string nroCelular { get; set; }
        public string password { get; set; }
        public bool activo { get; set; }
        public int idRol { get; set; }
    }
}
