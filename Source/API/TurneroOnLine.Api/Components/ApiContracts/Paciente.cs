﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class GuardarPacienteRequest:RequestBase
    {
        public Paciente paciente { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
    }

    public class GuardarPacienteAppRequest : RequestBase
    {
        public string idPaciente { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string dni { get; set; }
        public DateTime fechaDeNacimiento { get; set; }
        public string idUsuario { get; set; }
        public string idObraSocial { get; set; }
        public string idPlanObraSocial { get; set; }
    }


}
