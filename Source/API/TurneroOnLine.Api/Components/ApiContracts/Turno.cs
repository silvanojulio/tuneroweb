﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class CrearTurnoRequest:RequestBase
    {
        public string idPaciente { get; set; }
        public string idConsultorio { get; set; }
        public string idProfesional { get; set; }
        public int minutos { get; set; }
        public DateTime fechaHoraInicio { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
        public string comentarios { get; set; }
    }

    public class CambiarDeEstadoRequest : RequestBase
    {
        public string idTurno { get; set; }
        public int idEstado { get; set; }
        public string comentarios { get; set; }
    }

    public class ReasignarTurnoRequest : RequestBase
    {
        public string idTurno { get; set; }
        public DateTime fechaHoraInicio { get; set; }
        public string idConsultorio { get; set; }
        public string idProfesional { get; set; }
    }

    public class CancelarTurnoMobile : RequestBase
    {
        public string idTurno { get; set; }
    }

    public class AsignarProfesionalSecundarioRequest : RequestBase
    {
        public string idTurno { get; set; }
        public string idProfesional { get; set; }
    }


    public class ReplicarTurnoEnGoogleCalendarRequest : RequestBase
    {
        public string idTurno { get; set; }
    }

    public class AgregarObservacionesATurnoRequest : RequestBase
    {
        public string idTurno { get; set; }
        public string comentarios { get; set; }
    }

}
