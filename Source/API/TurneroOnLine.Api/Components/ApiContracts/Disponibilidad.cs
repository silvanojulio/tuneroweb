﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TurneroOnLine.Api.Components.ApiContracts
{
    public class GuardarDisponibilidadRequest:RequestBase
    {
        public Disponibilidad disponibilidad { get; set; }
        public List<string> idsProfesionales { get; set; }
        public bool esDisponibilidadRepetitiva { get; set; }

        public HorarioDisponibilidadDto lunes { get; set; }
        public HorarioDisponibilidadDto martes { get; set; }
        public HorarioDisponibilidadDto miercoles { get; set; }
        public HorarioDisponibilidadDto jueves { get; set; }
        public HorarioDisponibilidadDto viernes { get; set; }
        public HorarioDisponibilidadDto sabado { get; set; }
        public HorarioDisponibilidadDto domingo { get; set; }
    }

    public class CancelarDisponibilidadRequest : RequestBase
    {
        public string idDisponibilidad { get; set; }
    }

    public class EditarDisponibilidadRequest : RequestBase
    {
        public string idDisponibilidad { get; set; }
        public DateTime fechaHoraInicio { get; set; }
        public DateTime fechaHoraFin { get; set; }
        public string idConsultorio { get; set; }
        public string idProfesional { get; set; }
        public string comentarios { get; set; }
    }

    public class HorarioDisponibilidadDto
    {
        public int horaDesde { get; set; }
        public int minutosDesde { get; set; }

        public int horaHasta { get; set; }
        public int minutosHasta { get; set; }
    }
}
