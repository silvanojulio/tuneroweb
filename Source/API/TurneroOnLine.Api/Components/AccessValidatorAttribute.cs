﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TuneroOnline.Managers;

namespace TurneroOnLine.Api.Components
{
    public class AccessValidatorAttribute : ActionFilterAttribute
    {
        public const string CURRENT_SESSION = "CURRENT_SESSION";
        public const string CURRENT_TOKEN = "CURRENT_TOKEN";
        const string SESSION_MANAGER = "SESSION_MANAGER";
        const string HEADER_TOKEN = "SECURITYTOKEN";
        private List<RolDeUsuario> _rolesRequeridos;
        ISessionManager _sessionManager;

        public AccessValidatorAttribute(params RolesDeUsuario[] rolesRequeridos)
        {
            if (rolesRequeridos != null)
            {
                _rolesRequeridos = RolDeUsuario.GetRoles(rolesRequeridos.ToList());

                if (!_rolesRequeridos.Any(x => x.Id == RolDeUsuario.Admin.Id))
                    _rolesRequeridos.Add(RolDeUsuario.Admin);
            }
        }
        
        public AccessValidatorAttribute()
        {}

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var token = (string) context.HttpContext.Items[CURRENT_TOKEN];

            if (string.IsNullOrEmpty(token))
            {
                token = context.HttpContext.Request.Headers["CURRENT_TOKEN"];

                if (!string.IsNullOrEmpty(token))
                    token = token.TrimStart('{').TrimEnd('}');
            }

            _sessionManager = (ISessionManager) context.HttpContext.Items[SESSION_MANAGER];

            try
            {
                if (string.IsNullOrEmpty(token)) throw new Exception("Acceso restringido");

                var session = _sessionManager.ValidateToken(token).Result;
                
                if (_rolesRequeridos != null && _rolesRequeridos.Any() && !_rolesRequeridos.Any(x => x.Igual(session.CurrentUser.Rol)))
                {
                    UnauthorizedRequest(context, "El usuario no posee autorización para ejecutar esta acción");
                    return;
                }

                context.HttpContext.Items.Add(CURRENT_SESSION, session);
            }
            catch (Exception ex)
            {
                UnauthorizedRequest(context, ex.Message);
            }           
        }

        private void UnauthorizedRequest(ActionExecutingContext context, string message)
        {
            var result = new ObjectResult(new { error = message })
            {
                StatusCode = (int)System.Net.HttpStatusCode.Unauthorized
            };

            context.Result = result;
        }
    }
}
