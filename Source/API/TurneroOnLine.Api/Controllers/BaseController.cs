﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;
using SJSystems.Core.Security;

namespace TurneroOnLine.Api.Controllers
{
    public class BaseController : Controller
    {
        public const string CURRENT_SESSION = "CURRENT_SESSION";
        protected Session CurrentSession { get { return (Session)this.HttpContext.Items[CURRENT_SESSION]; } }
    }
}
