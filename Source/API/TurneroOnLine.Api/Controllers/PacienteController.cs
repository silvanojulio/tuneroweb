﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class PacienteController : BaseController
    {
        private IUserManager _userManager;
        private IPacientesManager _pacientenManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IObraSocialManager _obraSocialManager;

        public PacienteController(IUserManager userManager, ISessionManager sessionManager, IPacientesManager pacientenManager, IValorSateliteManager valorSatManager,
            IObraSocialManager obraSocialManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _pacientenManager = pacientenManager;
            _valorSatManager = valorSatManager;
            _obraSocialManager = obraSocialManager;
        }
        
        [HttpPost("guardar")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> Guardar([FromBody] GuardarPacienteRequest request)
        {
            try
            {
                var session = await _pacientenManager.GuardarPaciente(request.paciente, request.email);
                
                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("guardarApp")]
        [AccessValidator(RolesDeUsuario.Paciente)]
        public async Task<ObjectResult> GuardarApp([FromBody] GuardarPacienteAppRequest request)
        {
            try
            {
                var paciente = new Paciente
                {
                    Apellido = request.apellido,
                    Nombre = request.nombre,
                    FechaDeNacimiento = request.fechaDeNacimiento,
                    Dni = request.dni,
                    EsPacienteUsuario = request.idUsuario == CurrentSession.CurrentUser._id,
                    IdUsuario = request.idUsuario,
                    _id = request.idPaciente,
                    IdObraSocial = request.idObraSocial,
                    IdPlanObraSocial = request.idPlanObraSocial
                };

                var p = await _pacientenManager.GuardarPaciente(paciente, null);

                return new ObjectResult(p) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("buscar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Buscar(string dni, string nombreCompleto, string nroFicha)
        {
            try
            {
                var pacientes = await _pacientenManager.BuscarPacientes(nombreCompleto, dni, nroFicha, null);

                return new ObjectResult(pacientes) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPorId")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> obtenerPorId(string idPaciente)
        {
            try
            {
                var paciente = await _pacientenManager.ObtenerPorId(idPaciente);

                return new ObjectResult(
                    new {
                            paciente = paciente,
                        }
                    ) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("getDataForCreate")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetDataForCreate()
        {
            try
            {
                var provincias = await _valorSatManager.ObtenerValoresDeTabla(TablasSatelites.Provincias);
                var estados = EstadoPaciente.Estados;
                var obrasSociales = await _obraSocialManager.ObtenerObrasSociales(true);

                return new ObjectResult(new {
                    provincias = provincias,
                    estados = estados,
                    obrasSociales = obrasSociales
                }) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPacienteActual")]
        [AccessValidator(RolesDeUsuario.Paciente)]
        public async Task<ObjectResult> ObtenerPacienteActual()
        {
            try
            {
                var paciente = await _sessionManager.GetCurrentPaciente();

                return new ObjectResult(paciente)
                { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPacientesDeUsuarioActual")]
        [AccessValidator(RolesDeUsuario.Paciente)]
        public async Task<ObjectResult> ObtenerPacientesDeUsuarioActual()
        {
            try
            {
                var pacientes = await _pacientenManager.BuscarPacientes(null, null, null, CurrentSession.CurrentUser._id);

                return new ObjectResult(pacientes)
                { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
    }
}
