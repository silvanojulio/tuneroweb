﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class MobileController : BaseController
    {
        private IUserManager _userManager;
        private IDisponiblidadManager _disponibilidadManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IConsultorioManager _consultorioManager;
        private IProfesionalManager _profesionalManager;
        private IPacientesManager _pacienteManager;

        public MobileController(IUserManager userManager, ISessionManager sessionManager, IDisponiblidadManager disponibilidadManager, 
            IValorSateliteManager valorSatManager, IConsultorioManager consultorioManager, IProfesionalManager profesionalManager,
            IPacientesManager pacienteManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _disponibilidadManager = disponibilidadManager;
            _valorSatManager = valorSatManager;
            _consultorioManager = consultorioManager;
            _profesionalManager = profesionalManager;
            _pacienteManager = pacienteManager;
        }
        
    }
}
