﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController: BaseController
    {
        private IUserManager _userManager;
        private ISessionManager _sessionManager;
        public UserController(IUserManager userManager, ISessionManager sessionManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
        }

        [AccessValidator(RolesDeUsuario.Todos)]
        [HttpGet("getCurrentUser")]
        public ObjectResult GetCurrentUser()
        {
            return new ObjectResult(CurrentSession.CurrentUser) { StatusCode = (int)System.Net.HttpStatusCode.OK };
        }

        [HttpPost("login")]
        public async Task<ObjectResult> Login([FromBody] LoginRequest request)
        {
            try
            {
                var session = await _sessionManager.Login(request.email, request.password);

                session.CurrentUser.Password = null;

                return new ObjectResult(session) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("loginWithGoogle")]
        public async Task<ObjectResult> LoginWithGoogle([FromBody] LoginWithGoogleRequest request)
        {
            try
            {
                var session = _sessionManager.LoginWithGoogle(request.email, request.nombre, request.apellido, request.googleToken, request.imgUrl, request.googleId);
                var r = await session;
                return new ObjectResult(await session) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("createAdmin")]
        public async Task<ObjectResult> CreateAdmin()
        {            
            try
            {
                await _userManager.CreateAdminUser();

                return new ObjectResult("Usuario creado") { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.Forbidden };
            }
        }
        
        [HttpPost("cambiarPassword")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> CambiarPassword([FromBody] CambioDePasswordRequest request)
        {
            try
            {
                var idUsuario = CurrentSession.CurrentUser.Rol.Id == RolDeUsuario.Paciente.Id? 
                                CurrentSession.CurrentUser._id :
                                request.idUsuario;

                await _userManager.CambiarPassword(idUsuario, request.password);
                
                return new ObjectResult(new ResponseBase ()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("asignarNroDeCelular")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> AsignarNroDeCelular([FromBody] AsignarNroDeCelularRequest request)
        {
            try
            {
                var idUsuario = CurrentSession.CurrentUser.Rol.Id == RolDeUsuario.Paciente.Id ?
                                CurrentSession.CurrentUser._id :
                                request.idUsuario;

                await _userManager.AsignarNroDeCelular(idUsuario, request.nroCelular);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
        
        [HttpPost("crearUsuarioApp")]
        public async Task<ObjectResult> CrearUsuarioApp([FromBody] CrearUsuarioApp request)
        {
            try
            {
                await _userManager.CrearUsuarioApp(request.email, request.nroCelular, request.password);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("Device")]
        public async Task<ObjectResult> Device([FromBody] UserDeviceRequest request)
        {
            try
            {
                if(request.agregar)
                    await _userManager.AgregarDeviceId(request.idUsuario, request.idDevice);
                else
                    await _userManager.QuitarDeviceId(request.idUsuario, request.idDevice);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
        
        [HttpPost("resetPassword")]
        public async Task<ObjectResult> ResetPassword([FromBody] ResetPasswordAppRequest request)
        {
            try
            {
                await _userManager.ResetPassword(request.email);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("guardarUsuario")]
        [AccessValidator(RolesDeUsuario.Secretaria)]
        public async Task<ObjectResult> GuardarUsuario([FromBody] GuardarUsuarioRequest request)
        {
            try
            {
                var user = new User
                {
                    Activo = request.usuario.activo,
                    Email = request.usuario.email,
                    Password = request.usuario.password,
                    NroCelular = request.usuario.nroCelular,
                    _id = request.usuario._id,
                    Rol = RolDeUsuario.GetRol((RolesDeUsuario)request.usuario.idRol)
                };

                await _userManager.GuardarUsuario(user);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("buscarUsuarios")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> BuscarUsuarios(string email, int idRol)
        {
            try
            {
                var usuarios = await _userManager.BuscarUsuarios(idRol, email);

                return new ObjectResult(usuarios) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPorId")]
        [AccessValidator(RolesDeUsuario.Secretaria)]
        public async Task<ObjectResult> obtenerPorId(string idUsuario)
        {
            try
            {
                var usuario = await _userManager.ObtenerPorId(idUsuario);

                return new ObjectResult(usuario) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("confirmarCuentaPorEmail")]
        public async Task<ObjectResult> ConfirmarCuentaPorEmail([FromBody] ConfirmarCuentaPorEmailRequest request)
        {
            try
            {
                await _userManager.ActivarCuentaPorMailYCodigo(request.email, request.codigo);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
    }
}
