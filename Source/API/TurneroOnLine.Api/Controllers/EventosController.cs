﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class EventosController : BaseController
    {
        private IUserManager _userManager;
        private ISessionManager _sessionManager;
        private IEventoCalendarioManager _eventosManager;

        public EventosController(IUserManager userManager, ISessionManager sessionManager, IEventoCalendarioManager eventosManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _eventosManager = eventosManager;
        }
        
        [HttpPost("guardar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Guardar([FromBody] GuardarEventoRequest request)
        {
            try
            {
                var evento = new EventoCalendario
                {
                    Color = request.Color,
                    Descripcion = request.Descripcion,
                    fechaHoraFin = request.FechaHoraFin,
                    fechaHoraInicio = request.FechaHoraInicio,
                    Titulo = request.Titulo,
                    _id = request.Id
                };

                await _eventosManager.GuardarEvento(evento);

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("buscar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Buscar(DateTime fechaDesde, DateTime fechaHasta)
        {
            try
            {
                var eventos = await _eventosManager.ObtenerEventosPorFechas(fechaDesde, fechaHasta);

                return new ObjectResult(eventos) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPorId")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ObtenerPorId(string idEvento)
        {
            try
            {
                var evento = await _eventosManager.ObtenerPorId(idEvento);

                return new ObjectResult(evento) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("eliminarEvento")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> EliminarEvento([FromBody] EliminarEventoRequest request)
        {
            try
            {
                await _eventosManager.EliminarEvento(request.IdEvento);

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
    }
}
