﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class DashboardController : BaseController
    {
        private IUserManager _userManager;
        private ISessionManager _sessionManager;
        private IDashboardManager _dashboardManager;

        public DashboardController(IUserManager userManager, ISessionManager sessionManager, IDashboardManager dashboardManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _dashboardManager = dashboardManager;
        }

        [HttpGet("obtenerDashboard")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ObtenerDashboard()
        {
            try
            {
                var dashboard = await _dashboardManager.ObtenerHomeDashboard();

                return new ObjectResult(
                    new
                    {
                        dashboard = dashboard,
                    })
                { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

    }
}
