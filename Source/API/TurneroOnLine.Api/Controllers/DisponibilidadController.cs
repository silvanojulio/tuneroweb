﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class DisponibilidadController : BaseController
    {
        private IUserManager _userManager;
        private IDisponiblidadManager _disponibilidadManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IConsultorioManager _consultorioManager;
        private IProfesionalManager _profesionalManager;

        public DisponibilidadController(IUserManager userManager, ISessionManager sessionManager, IDisponiblidadManager disponibilidadManager, 
            IValorSateliteManager valorSatManager, IConsultorioManager consultorioManager, IProfesionalManager profesionalManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _disponibilidadManager = disponibilidadManager;
            _valorSatManager = valorSatManager;
            _consultorioManager = consultorioManager;
            _profesionalManager = profesionalManager;
        }
        
        [HttpGet("buscar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Buscar(DateTime fechaDesde, DateTime fechaHasta, string idConsultorio, List<string> idsProfesionales, int? idEstado)
        {
            try
            {
                var disponibilidades = await _disponibilidadManager.BuscarDisponibilidades(fechaDesde, fechaHasta, idConsultorio, idsProfesionales, idEstado);

                return new ObjectResult(disponibilidades) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPorId")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ObtenerPorId(string idDisponibilidad)
        {
            try
            {
                var disponibilidad = await _disponibilidadManager.ObtenerDisponibilidadPorId(idDisponibilidad);

                return new ObjectResult(disponibilidad) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }


        [HttpGet("getDataForBuscar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> GetDataForBuscar()
        {
            try
            {
                var estados = EstadoDisponibilidad.Estados;
                var consultorios = await _consultorioManager.ObtenerConsultoriosActivos();
                var profesionales = await _profesionalManager.ObtenerProfesionalesActivos();
                var motivosDeReceso = await _valorSatManager.ObtenerValoresDeTabla(TablasSatelites.MotivosDeReceso);

                return new ObjectResult(new {
                    estados = estados,
                    consultorios = consultorios,
                    profesionales = profesionales,
                    motivosDeReceso = motivosDeReceso,
                }) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
        
        [HttpPost("guardar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Guardar([FromBody] GuardarDisponibilidadRequest request)
        {
            try
            {
                await _disponibilidadManager.GuardarDisponibilidad(request.disponibilidad);

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("crearDisponibilidades")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> CrearDisponibilidades([FromBody] GuardarDisponibilidadRequest request)
        {
            try
            {
                List<DiaHoraMinutoDisponibilidad> dias=null;

                if (request.esDisponibilidadRepetitiva)
                {
                    dias = new List<DiaHoraMinutoDisponibilidad>();

                    if (request.lunes!=null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Lunes,
                            desde = new TimeSpan(request.lunes.horaDesde, request.lunes.minutosDesde, 0),
                            hasta = new TimeSpan(request.lunes.horaHasta, request.lunes.minutosHasta, 0)
                        } );

                    if (request.martes != null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Martes,
                            desde = new TimeSpan(request.martes.horaDesde, request.martes.minutosDesde, 0),
                            hasta = new TimeSpan(request.martes.horaHasta, request.martes.minutosHasta, 0)
                        });

                    if (request.miercoles != null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Miercoles,
                            desde = new TimeSpan(request.miercoles.horaDesde, request.miercoles.minutosDesde, 0),
                            hasta = new TimeSpan(request.miercoles.horaHasta, request.miercoles.minutosHasta, 0)
                        });

                    if (request.jueves != null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Juenes,
                            desde = new TimeSpan(request.jueves.horaDesde, request.jueves.minutosDesde, 0),
                            hasta = new TimeSpan(request.jueves.horaHasta, request.jueves.minutosHasta, 0)
                        });

                    if (request.viernes != null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Viernes,
                            desde = new TimeSpan(request.viernes.horaDesde, request.viernes.minutosDesde, 0),
                            hasta = new TimeSpan(request.viernes.horaHasta, request.viernes.minutosHasta, 0)
                        });

                    if (request.sabado != null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Sabado,
                            desde = new TimeSpan(request.sabado.horaDesde, request.sabado.minutosDesde, 0),
                            hasta = new TimeSpan(request.sabado.horaHasta, request.sabado.minutosHasta, 0)
                        });

                    if (request.domingo != null)
                        dias.Add(new DiaHoraMinutoDisponibilidad
                        {
                            dia = DiaDeLaSemana.Domingo,
                            desde = new TimeSpan(request.domingo.horaDesde, request.domingo.minutosDesde, 0),
                            hasta = new TimeSpan(request.domingo.horaHasta, request.domingo.minutosHasta, 0)
                        });
                    
                }
                
                await _disponibilidadManager.CrearDisponibilidades(request.disponibilidad, request.idsProfesionales, dias);

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("editarDisponibilidad")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> EditarDisponibilidad([FromBody] EditarDisponibilidadRequest request)
        {
            try
            {
                await _disponibilidadManager.EditarDisponibilidad(request.idDisponibilidad, request.idProfesional, request.idConsultorio, 
                    request.fechaHoraInicio, request.fechaHoraFin, request.comentarios );

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("cancelarDisponibilidad")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> CancelarDisponibilidad([FromBody] CancelarDisponibilidadRequest request)
        {
            try
            {
                await _disponibilidadManager.CancelarDisponibilidad(request.idDisponibilidad, "");

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
    }
}
