﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class GeneralController : BaseController
    {
        private IUserManager _userManager;
        private IDisponiblidadManager _disponibilidadManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IConsultorioManager _consultorioManager;
        private IProfesionalManager _profesionalManager;
        private IPacientesManager _pacienteManager;
        
        public GeneralController(IUserManager userManager, ISessionManager sessionManager, IDisponiblidadManager disponibilidadManager, 
            IValorSateliteManager valorSatManager, IConsultorioManager consultorioManager, IProfesionalManager profesionalManager,
            IPacientesManager pacienteManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _disponibilidadManager = disponibilidadManager;
            _valorSatManager = valorSatManager;
            _consultorioManager = consultorioManager;
            _profesionalManager = profesionalManager;
            _pacienteManager = pacienteManager;
        }
        
        [HttpGet("getEstadosDeTurnos")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetEstadosDeTurnos()
        {
            try
            {
                var estados = EstadoDeTurno.Estados;
                var consultorios = await _consultorioManager.ObtenerConsultoriosActivos();

                return new ObjectResult(estados) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("getConsultorios")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetConsultorios()
        {
            try
            {
                var consultorios = await _consultorioManager.ObtenerConsultoriosActivos();

                return new ObjectResult(consultorios) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("getProfesionales")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetProfesionales()
        {
            try
            {
                var profesionales = await _profesionalManager.ObtenerProfesionalesActivos();

                return new ObjectResult(profesionales){ StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("getPacientes")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetPacientes(string textoBuscado)
        {
            try
            {
                var pacientes = await _pacienteManager.BuscarPacientes(textoBuscado);
                return new ObjectResult(pacientes) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("getValoresDeTablaSatelite")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetValoresDeTablaSatelite(List<int> idsTablasSatelites)
        {
            try
            {
                var valores = await _valorSatManager.ObtenerValoresDeTablas(idsTablasSatelites);
                return new ObjectResult(valores) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

    }
}
