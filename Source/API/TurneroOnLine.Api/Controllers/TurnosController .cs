﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class TurnosController : BaseController
    {
        private IUserManager _userManager;
        private IDisponiblidadManager _disponibilidadManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IConsultorioManager _consultorioManager;
        private IProfesionalManager _profesionalManager;
        private ITurnoManager _turnoManager;

        public TurnosController(IUserManager userManager, ISessionManager sessionManager, IDisponiblidadManager disponibilidadManager, 
            IValorSateliteManager valorSatManager, IConsultorioManager consultorioManager, IProfesionalManager profesionalManager,
            ITurnoManager turnoManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _disponibilidadManager = disponibilidadManager;
            _valorSatManager = valorSatManager;
            _consultorioManager = consultorioManager;
            _profesionalManager = profesionalManager;
            _turnoManager = turnoManager;
        }
        
        [HttpGet("getDisponibilidades")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetDisponibilidades([FromQuery] DateTime desde, [FromQuery] DateTime hasta, 
            [FromQuery] List<string> idsConsultorios, [FromQuery] List<string> idsProfesionales)
        {
            try
            {
                desde = desde.Date;
                hasta = hasta.Date.AddDays(1).AddSeconds(-1);

                var disponibilidades = await _disponibilidadManager.ObtenerDisponibilidadesParaTurno(desde, hasta, idsConsultorios, idsProfesionales);

                return new ObjectResult(new { disponibilidades = disponibilidades } ) { StatusCode = (int) System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("crearTurno")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> CrearTurno([FromBody] CrearTurnoRequest request)
        {
            try
            {
                var idTurno = await _turnoManager.CrearTurno(request.idPaciente, request.idConsultorio, request.idProfesional, request.fechaHoraInicio, request.minutos,
                    request.celular, request.email, request.comentarios);

                return new ObjectResult(new { idTurno  = idTurno }) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("buscarTurnos")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> BuscarTurnos(List<string> idsProfesionales, List<string> idsConsultorios, List<int> idsEstados, 
            DateTime desde, DateTime hasta, string idDisponibilidad)
        {
            try
            {
                desde = desde.Date;
                hasta = hasta.Date.AddDays(1).AddSeconds(-1);

                var turnos = await _turnoManager.BuscarTurnos(idsProfesionales, idsConsultorios, idsEstados, desde, hasta, idDisponibilidad);

                return new ObjectResult(new { turnos = turnos }) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("cambiarDeEstado")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> CambiarDeEstado([FromBody] CambiarDeEstadoRequest request)
        {
            try
            {
                await _turnoManager.CambiarDeEstado(request.idTurno, request.idEstado, request.comentarios);

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("cancelarTurnoMobile")]
        [AccessValidator(RolesDeUsuario.Paciente)]
        public async Task<ObjectResult> cancelarTurnoMobile([FromBody] CancelarTurnoMobile request)
        {
            try
            {
                await _turnoManager.CambiarDeEstado(request.idTurno, EstadoDeTurno.CanceladoPorPaciente.Id, null);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("reasignarTurno")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ReasignarTurno([FromBody] ReasignarTurnoRequest request)
        {
            try
            {
                await _turnoManager.ReasignarTurno(request.idTurno, request.fechaHoraInicio, request.idProfesional, request.idConsultorio );

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerProximosTurnosDeUsuario")]
        [AccessValidator(RolesDeUsuario.Paciente)]
        public async Task<ObjectResult> ObtenerProximosTurnosDeUsuario()
        {
            try
            {
                var turnos = await _turnoManager.ObtenerProximosTurnosPorUsuario(CurrentSession.CurrentUser._id);

                return new ObjectResult(turnos) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("testGoogle")]
        public async Task<ObjectResult> TestGoogle()
        {
            try
            {
                await _turnoManager.TestGoogle();

                return new ObjectResult(true) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("asignarProfesionalSecundario")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> AsignarProfesionalSecundario(
                    [FromBody] AsignarProfesionalSecundarioRequest request)
        {
            try
            {
                await _turnoManager.AsignarProfesionalSecundarioATurno(request.idTurno, request.idProfesional);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }


        [HttpPost("replicarTurnoEnGoogleCalendar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ReplicarTurnoEnGoogleCalendar([FromBody] ReplicarTurnoEnGoogleCalendarRequest request)
        {
            try
            {
                _turnoManager.AgregarTurnoAAgendaGoogle(request.idTurno).RunSynchronously();

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("agregarObservacionesATurno")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> AgregarObservacionesATurno([FromBody] AgregarObservacionesATurnoRequest request)
        {
            try
            {
                await _turnoManager.AgregarComentario(request.idTurno, request.comentarios);

                return new ObjectResult(new ResponseBase()) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
    }
}
