﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class NotificacionesController : BaseController
    {
        private INotificacionesMobileManager _notificacionesManager;
        private ISessionManager _sessionManager;
        public NotificacionesController(INotificacionesMobileManager notificacionesManager, ISessionManager sessionManager)
        {
            _notificacionesManager = notificacionesManager;
            _sessionManager = sessionManager;
        }
        
        [HttpGet("obtenerNuevasNotificaciones")]
        public async Task<ObjectResult> ObtenerNuevasNotificaciones([FromQuery] string idUsuario, [FromQuery] DateTime fecha)
        {
            try
            {
                var notificaciones = await _notificacionesManager.ObtenerNotificaciones(idUsuario, fecha);

                return new ObjectResult(new {
                    notificaciones = notificaciones
                }) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerUltimasNotificaciones")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> ObtenerUltimasNotificaciones([FromQuery] string idUsuario)
        {
            try
            {
                var notificaciones = await _notificacionesManager.ObtenerUltimasNotificaciones(idUsuario);

                return new ObjectResult(new
                {
                    notificaciones = notificaciones
                })
                { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

    }
}
