﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;
using System;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class ObrasSocialesController : BaseController
    {
        private IUserManager _userManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IObraSocialManager _obraSocialManager;

        public ObrasSocialesController(IUserManager userManager, ISessionManager sessionManager,  IValorSateliteManager valorSatManager, 
            IObraSocialManager obraSocialManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _valorSatManager = valorSatManager;
            _obraSocialManager = obraSocialManager;
        }
        
        [HttpGet("getObrasSociales")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> GetObrasSociales()
        {
            try
            {
                var obrasSociales = await _obraSocialManager.ObtenerObrasSociales(true);

                return new ObjectResult(obrasSociales) { StatusCode = (int) System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("buscar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Buscar(bool soloActivos)
        {
            try
            {
                var obrasSociales = await _obraSocialManager.ObtenerObrasSociales(soloActivos);

                return new ObjectResult(obrasSociales) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("guardarObraSocial")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> GuardarObraSocial([FromBody] GuardarObraSocialRequest request)
        {
            try
            {
                var obrasSocial = await _obraSocialManager.Guardar(new ObraSocial {
                    Activo = request.Activo,
                    Codigo = request.Codigo,
                    Nombre = request.Nombre,
                    _id = request._id                    
                });

                return new ObjectResult(obrasSocial) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpPost("guardarPlanObraSocial")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> GuardarPlanObraSocial([FromBody] GuardarPlanObraSocialRequest request)
        {
            try
            {
                var obrasSocial = await _obraSocialManager.GuardarPlanDeObraSocial(new PlanDeObraSocial
                {
                    Activo = request.Activo,
                    Codigo = request.Codigo,
                    Nombre = request.Nombre,
                    IdObraSocial = request.IdObraSocial,
                    _id = request._id
                });

                return new ObjectResult(obrasSocial) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPorId")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ObtenerPorId(string idObraSocial)
        {
            try
            {
                var obraSocial = await _obraSocialManager.ObtenerPorId(idObraSocial);

                return new ObjectResult(obraSocial) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPlanesDeObraSocial")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> ObtenerPlanesDeObraSocial(string idObraSocial)
        {
            try
            {
                var planes = await _obraSocialManager.ObtenerPlanesDeObraSocial(idObraSocial);

                return new ObjectResult(planes) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }
    }
}
