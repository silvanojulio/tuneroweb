﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TuneroOnline.Managers;

using TurneroOnLine.Api.Components;
using SJSystems.Core.Security;
using TurneroOnLine.Api.Components.ApiContracts;
using TurneroOnline.Entities;

namespace TurneroOnLine.Api.Controllers
{
    [Route("api/[controller]")]
    public class ProfesionalesController : BaseController
    {
        private IUserManager _userManager;
        private IProfesionalManager _profesinalManager;
        private ISessionManager _sessionManager;
        private IValorSateliteManager _valorSatManager;
        private IObraSocialManager _obraSocialManager;

        public ProfesionalesController(IUserManager userManager, ISessionManager sessionManager, IProfesionalManager profesinalManager, IValorSateliteManager valorSatManager,
            IObraSocialManager obraSocialManager)
        {
            _userManager = userManager;
            _sessionManager = sessionManager;
            _profesinalManager = profesinalManager;
            _valorSatManager = valorSatManager;
            _obraSocialManager = obraSocialManager;
        }

        [HttpPost("guardar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Guardar([FromBody] GuardarProfesionalRequest request)
        {
            try
            {
                var profesional = new Profesional
                {
                    _id = request._id,
                    Nombre = request.Nombre,
                    Apellido = request.Apellido,
                    Activo = request.Activo,
                    DetalleDeProfesional = request.DetalleDeProfesional,
                    EsPrimeraCita = request.EsPrimeraCita,
                    PrefijoProfesional = request.PrefijoProfesional,
                    Color = request.Color,
                    IdsEspecialidades = request.idsEspecialidades
                };

                if (string.IsNullOrEmpty(profesional._id))
                    profesional = await _profesinalManager.CrearProfesional(profesional, request.email, request.nroCelular, request.password);
                else
                    await _profesinalManager.ActualizarProfesional(profesional, request.email, request.nroCelular, request.password);

                return new ObjectResult(profesional) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("buscar")]
        [AccessValidator(RolesDeUsuario.SoloUsuariosPrivados)]
        public async Task<ObjectResult> Buscar(string nombre)
        {
            try
            {
                var profesionales = await _profesinalManager.Buscar(nombre);

                return new ObjectResult(profesionales) { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

        [HttpGet("obtenerPorId")]
        [AccessValidator(RolesDeUsuario.Todos)]
        public async Task<ObjectResult> ObtenerPorId(string idProfesional)
        {
            try
            {
                var profesional = await _profesinalManager.ObtenerPorId(idProfesional);

                if (profesional.Usuario != null)
                    profesional.Usuario.Password = "";

                return new ObjectResult(
                    new
                    {
                        profesional = profesional,
                    }
                    )
                { StatusCode = (int)System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return new ObjectResult(new { error = ex.Message }) { StatusCode = (int)System.Net.HttpStatusCode.ExpectationFailed };
            }
        }

    }
}
