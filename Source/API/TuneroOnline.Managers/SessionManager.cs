﻿using Google.Apis.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface ISessionManager
    {
        Task<Session> ValidateToken(string token);
        Task<Session> Login(string email, string password);
        Task<Session> LoginWithGoogle(string email, string nombre, string apellido, string googleToken, string imgUrl, string googleId);
        User GetCurrentUserSession();
        Task<Paciente> GetCurrentPaciente();
        Task<Profesional> GetCurrentProfesional();
    }

    public class SessionManager : ManagerBase, ISessionManager
    {

        IUserManager _userManager;
        protected ICurrentUser _currentUser;

        Paciente currentPaciente=null;
        Profesional currentProfesional = null;

        public SessionManager(IOptions<TurneroOnlineConfiguration> _settings, IUserManager userManager, ICurrentUser currentUser) : base(_settings)
        {
            _userManager = userManager;
            _currentUser = currentUser;
        }

        public async Task<Session> ValidateToken(string token)
        {
            var session = await Db.Sessiones.GetByIdAsync(token);

            if (session == null || session.Expirada || session.Expirada)
                throw new Exception("No posee sesión activa");

            if (session.UltimaActualizacion.AddMinutes(60) < DateTime.Now)
                throw new Exception("La sesión ha expirado");

            session.UltimaActualizacion = DateTime.Now;

            Db.Sessiones.Update(session);

            return session;
        }

        public async Task<Session> Login(string email, string password)
        {
            var user = _userManager.Login(email, password);

            var session = new Session
            {
                CurrentUser = user,
                Expirada = false,
                FechaInicio = DateTime.UtcNow,
                UltimaActualizacion = DateTime.UtcNow
            };

            await Db.Sessiones.Insert(session);

            return session;
        }

        public async Task<Session> LoginWithGoogle(string email, string nombre, string apellido, string googleToken, string imgUrl, string googleId)
        {
            //await ValidarGoogleUser(email, googleToken);

            var user = Db.Usuarios.GetUserByEmail(email);

            if (user != null)
            {
                user.GoogleProfile = new GoogleProfile
                {
                    Apellido = apellido,
                    Nombre = nombre,
                    GoogleId = googleId,
                    GoogleToken = googleToken,
                    ImgUrl = imgUrl
                };

                await Db.Usuarios.Update(user);
            }
            else
            {
                user = new User
                {
                    Email = email,
                    Activo = true,
                    CodigoValidacionCelular = "0103",
                    CodigoValidacionEmail = "",
                    FechaConfirmacionCelular = DateTime.Now,
                    FechaConfirmacionEmail = DateTime.Now,
                    Password = "nopasswordsetted",
                    Rol = RolDeUsuario.Paciente,
                    FechaAlta = DateTime.Now
                };


                user.GoogleProfile = new GoogleProfile
                {
                    Apellido = apellido,
                    Nombre = nombre,
                    GoogleId = googleId,
                    GoogleToken = googleToken,
                    ImgUrl = imgUrl
                };

                await Db.Usuarios.Insert(user);
            }

            var session = new Session
            {
                CurrentUser = user,
                Expirada = false,
                FechaInicio = DateTime.UtcNow,
                UltimaActualizacion = DateTime.UtcNow
            };

            await Db.Sessiones.Insert(session);

            return session;
        }

        private async Task ValidarGoogleUser(string email, string googleToken)
        {
            

        }

        public User GetCurrentUserSession()
        {
            return _currentUser.GetCurrentUser();
        }

        public async Task<Paciente> GetCurrentPaciente()
        {
            if (currentPaciente == null)
            {
                currentPaciente = await Db.Pacientes.ObtenerPorUserId(GetCurrentUserSession()._id);
            }

            return currentPaciente;
        }

        public async Task<Profesional> GetCurrentProfesional()
        {
            if (CurrentProfesional == null)
            {
                currentProfesional = await Db.Profesionales.ObtenerPorUserId(GetCurrentUserSession()._id);
            }

            return currentProfesional;
        }
    }
}
