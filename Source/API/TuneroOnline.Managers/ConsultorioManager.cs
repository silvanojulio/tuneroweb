﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IConsultorioManager
    {
        Task<List<Consultorio>> ObtenerConsultoriosActivos();
     }

    public class ConsultorioManager : ManagerBase, IConsultorioManager
    {
        public ConsultorioManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager _sessionManager) : base(_settings, _sessionManager)
        {}

        public async Task<List<Consultorio>> ObtenerConsultoriosActivos()
        {
            List<Consultorio> consultorios = await Db.Consultorios.ObtenerConsultoriosActivos();

            return consultorios;
        }
        
    }
}
