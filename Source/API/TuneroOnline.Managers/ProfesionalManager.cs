﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IProfesionalManager
    {
        Task<List<Profesional>> ObtenerProfesionalesActivos();
        Task<Profesional> CrearProfesional(Profesional profesional, string email, string celular, string password);
        Task<Profesional> ActualizarProfesional(Profesional profesional, string email, string celular, string password);
        Task<List<Profesional>> Buscar(string nombre);
        Task<Profesional> ObtenerPorId(string idProfesional);
    }

    public class ProfesionalManager : ManagerBase, IProfesionalManager
    {
        public ProfesionalManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager _sessionManager) : base(_settings, _sessionManager)
        {}
              
        public async Task<List<Profesional>> Buscar(string nombre)
        {
            var items = await Db.Profesionales.Buscar(nombre);

            var especialidades = await Db.ValoresSatelites.ObtenerValoresDeTabla(TablasSatelites.Especialidades);

            items.ForEach(x =>
            {                
                if (x.IdsEspecialidades != null)
                    x.Especialidades = especialidades.Where(y => x.IdsEspecialidades.Contains(y.IdValor)).ToList();
            });

            return items.OrderBy(x=>x.Apellido).ToList();
        }

        public async Task<Profesional> CrearProfesional(Profesional profesional, string email, string celular, string password)
        {
            ValidarProfesional(profesional, email, celular, password, true);

            var usuarioConEmail = Db.Usuarios.GetUserByEmail(email);

            if(usuarioConEmail!=null)
            {
                var profesionalRelacionadoAUsuario = await Db.Profesionales.ObtenerPorUserId(usuarioConEmail._id);

                if (profesionalRelacionadoAUsuario != null)
                    throw new Exception("El email que intenta ingresar pertenece a otro profesional registrado en el sistema");

                usuarioConEmail.Rol = RolDeUsuario.Profesional;
                usuarioConEmail.NroCelular = celular;
                usuarioConEmail.Password = password;
                await Db.Usuarios.Update(usuarioConEmail);
            }
            else
            {
                usuarioConEmail = await CrearUsuarioProfesional(email, celular, password);
                profesional.IdUsuario = usuarioConEmail._id;
            }
            
            await Db.Profesionales.Insert(profesional);

            return profesional;
        }

        public async Task<Profesional> ActualizarProfesional(Profesional profesional, string email, string celular, string password)
        {
            ValidarProfesional(profesional, email, celular, password, !string.IsNullOrEmpty(password));

            var profesionalExistente = await Db.Profesionales.GetByIdAsync(profesional._id);

            profesionalExistente.Nombre = profesional.Nombre;
            profesionalExistente.Apellido = profesional.Apellido;
            profesionalExistente.DetalleDeProfesional = profesional.DetalleDeProfesional;
            profesionalExistente.EsPrimeraCita = profesional.EsPrimeraCita;
            profesionalExistente.PrefijoProfesional = profesional.PrefijoProfesional;
            profesionalExistente.Activo = profesional.Activo;
            profesionalExistente.Color = profesional.Color;
            profesionalExistente.IdsEspecialidades = profesional.IdsEspecialidades;
            
            var usuarioExistente = !string.IsNullOrEmpty(profesionalExistente.IdUsuario) ?
                                       await Db.Usuarios.GetByIdAsync(profesionalExistente.IdUsuario) : null;

            if (usuarioExistente != null)
            {
                var emailAnterior = usuarioExistente.Email;

                usuarioExistente.Email = email;
                usuarioExistente.NroCelular = celular;
                usuarioExistente.Activo = profesional.Activo;

                if (!string.IsNullOrEmpty(password))
                    usuarioExistente.Password = password;

                await Db.Usuarios.Update(usuarioExistente);
            }
            else
            {
                var usuarioConEmail = await CrearUsuarioProfesional(email, celular, password);
                profesionalExistente.IdUsuario = usuarioConEmail._id;
            }
            
            await Db.Profesionales.Update(profesionalExistente);

            return profesionalExistente;
        }
        
        public async Task<Profesional> ObtenerPorId(string idProfesional)
        {
            var item = await Db.Profesionales.GetByIdAsync(idProfesional);

            if (!string.IsNullOrEmpty(item.IdUsuario))
            {
                item.Usuario = await Db.Usuarios.GetByIdAsync(item.IdUsuario);
            }

            var especialidades = await Db.ValoresSatelites.ObtenerValoresDeTabla(TablasSatelites.Especialidades);
            if (item.IdsEspecialidades != null)
            {
                item.Especialidades = especialidades.Where(x => item.IdsEspecialidades.Contains(x.IdValor)).ToList();
            }

            return item;
        }

        public async Task<List<Profesional>> ObtenerProfesionalesActivos()
        {
            List<Profesional> profesionales = await Db.Profesionales.ObtenerProfesionalesActivos();

            return profesionales;
        }

        #region privados

        void ValidarProfesional(Profesional profesional, string email, string celular, string password, bool validarPassword)
        {
            if (string.IsNullOrEmpty(profesional.Nombre) || string.IsNullOrEmpty(profesional.Apellido))
                throw new Exception("Debe completar el nombre y el apellido");

            if ((validarPassword && string.IsNullOrEmpty(password)) || string.IsNullOrEmpty(email))
                throw new Exception("Debe completar el email y la contraseña");

            if (validarPassword && password.Trim().Length < 4)
                throw new Exception("La contraseña debe tener al menos 4 caracteres");
        }

        async Task<User> CrearUsuarioProfesional(String email, string celular, string password)
        {
            var user = new User
            {
                Email = email,
                NroCelular = celular,
                Activo = true,
                FechaAlta = DateTime.Now,
                Password = password,
                Rol = RolDeUsuario.Profesional,
            };

            return await Db.Usuarios.Insert(user);
        }
        #endregion
    }
}
