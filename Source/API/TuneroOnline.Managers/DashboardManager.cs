﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;
using TurneroOnline.Entities.Dtos;

namespace TuneroOnline.Managers
{
    public interface IDashboardManager
    {
        Task<HomeDashboard> ObtenerHomeDashboard();        
    }

    public class DashboardManager : ManagerBase, IDashboardManager
    {
        public DashboardManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager _sessionManager) : base(_settings, _sessionManager)
        {}

        public async Task<HomeDashboard> ObtenerHomeDashboard()
        {
            var d = new HomeDashboard();

            var turnos_t = Db.Turnos.BuscarTurnos(DateTime.Today, DateTime.Today.AddDays(1), null,
                null, null, null);
            
            var cantidadPacientes = Db.Pacientes.ObtenerCantidadDeActivos();
            var cantidadProfesionales = Db.Profesionales.ObtenerCantidadDeActivos();
            var cantidadTurnosPendientes = Db.Turnos.ObtenerCantidadDeTurnosPendientesDeConfirmacion();
            var cantidadTurnosAFuturo = Db.Turnos.ObtenerCantidadDeTurnosConfirmadosAFuturo();
            
            d.CantidadDePacientes = await cantidadPacientes;
            d.CantidadDeProfesionales = await cantidadProfesionales;
            d.CantidadDeTurnosPendientesDeConfirmacion = await cantidadTurnosPendientes;
            d.CantidadDeTurnosPendientesDeAtencion = await cantidadTurnosAFuturo;

            var turnos = await turnos_t;
            var idsProf = turnos.Select(x => x.IdProfesional).Distinct().ToList();
            var profesionales = await Db.Profesionales.GetByIdsAsync(idsProf);
            turnos.ForEach(x => x.Profesional = profesionales.FirstOrDefault(y => y._id == x.IdProfesional));

            d.CantidadPorEstado = new List<DatoParaGrafico<long>>();
            
            d.CantidadPorEstado.Add(new DatoParaGrafico<long>
            {
                Descripcion = EstadoDeTurno.Asistio.Descripcion,
                Valor = turnos.Where(y => y.Estado.Id == EstadoDeTurno.Asistio.Id).Count()
            });

            d.CantidadPorEstado.Add(new DatoParaGrafico<long>
            {
                Descripcion = EstadoDeTurno.Cancelado.Descripcion,
                Valor = turnos.Where(y => y.Estado.Id == EstadoDeTurno.Cancelado.Id).Count()
            });

            d.CantidadPorEstado.Add(new DatoParaGrafico<long>
            {
                Descripcion = EstadoDeTurno.CanceladoPorPaciente.Descripcion,
                Valor = turnos.Where(y => y.Estado.Id == EstadoDeTurno.CanceladoPorPaciente.Id).Count()
            });

            d.CantidadPorEstado.Add(new DatoParaGrafico<long>
            {
                Descripcion = EstadoDeTurno.ConfirmadoPorConsultorio.Descripcion,
                Valor = turnos.Where(y => y.Estado.Id == EstadoDeTurno.ConfirmadoPorConsultorio.Id).Count()
            });

            d.CantidadPorEstado.Add(new DatoParaGrafico<long>
            {
                Descripcion = EstadoDeTurno.ConfirmadoPorPaciente.Descripcion,
                Valor = turnos.Where(y => y.Estado.Id == EstadoDeTurno.ConfirmadoPorPaciente.Id || y.Estado.Id == EstadoDeTurno.Reservado.Id).Count()
            });
            
            d.CantidadPorEstado.Add(new DatoParaGrafico<long>
            {
                Descripcion = EstadoDeTurno.NoAsistio.Descripcion,
                Valor = turnos.Where(y => y.Estado.Id == EstadoDeTurno.NoAsistio.Id).Count()
            });

            var turnosAgrupadosPorProfesional = turnos.GroupBy(x => x.IdProfesional);

            d.CantidadPorProfesionalYEstado = turnosAgrupadosPorProfesional.
                Select(x => new DatoParaGrafico<long>
            {
                Descripcion = x.FirstOrDefault().Profesional.NombreCompleto,
                Valor = x.LongCount(),
                Valores = new List<long>
                {
                    x.Where(y=>y.Estado.Id == EstadoDeTurno.Asistio.Id).Count(),
                    x.Where(y=>y.Estado.Id == EstadoDeTurno.Cancelado.Id).Count(),
                    x.Where(y=>y.Estado.Id == EstadoDeTurno.CanceladoPorPaciente.Id).Count(),
                    x.Where(y=>y.Estado.Id == EstadoDeTurno.ConfirmadoPorConsultorio.Id).Count(),
                    x.Where(y=>y.Estado.Id == EstadoDeTurno.ConfirmadoPorPaciente.Id || y.Estado.Id == EstadoDeTurno.Reservado.Id).Count(),
                    x.Where(y=>y.Estado.Id == EstadoDeTurno.NoAsistio.Id).Count(),
                }
            }).ToList();

            return d;
        }
    }
}
