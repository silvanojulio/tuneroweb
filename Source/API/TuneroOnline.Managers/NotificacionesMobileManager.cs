﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface INotificacionesMobileManager
    {
        Task<List<NotificacionMobile>> ObtenerNotificaciones(string idUsuario, DateTime fecha);
        Task<List<NotificacionMobile>> ObtenerUltimasNotificaciones(string idUsuario);
    }

    public class NotificacionesMobileManager : ManagerBase, INotificacionesMobileManager
    {
        public NotificacionesMobileManager(IOptions<TurneroOnlineConfiguration> _settings) : base(_settings)
        {}

        public async Task<List<NotificacionMobile>> ObtenerNotificaciones(string idUsuario, DateTime fecha)
        {
            return await Db.NotificacionesMobile.ObtenerNotificaciones(idUsuario, fecha);
        }

        public async Task<List<NotificacionMobile>> ObtenerUltimasNotificaciones(string idUsuario)
        {
            var notificaciones = await Db.NotificacionesMobile.ObtenerNotificaciones(idUsuario, DateTime.Now.AddDays(-5));

            return notificaciones.OrderByDescending(x => x.FechaCreada).ToList();
        }
    }
}
