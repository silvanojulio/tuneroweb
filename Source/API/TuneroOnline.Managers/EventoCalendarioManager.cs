﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IEventoCalendarioManager
    {
        Task GuardarEvento(EventoCalendario evento);
        Task<List<EventoCalendario>> ObtenerEventosPorFechas(DateTime desde, DateTime hasta);
        Task<EventoCalendario> ObtenerPorId(string idEvento);
        Task EliminarEvento(string idEvento);
    }

    public class EventoCalendarioManager : ManagerBase, IEventoCalendarioManager
    {
        public EventoCalendarioManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager sessionManager) : 
            base(_settings, sessionManager)
        {
        }

        public async Task GuardarEvento(EventoCalendario evento)
        {
            if(evento.IsNew)
                await Db.EventosCalendario.Insert(evento);
            else
                await Db.EventosCalendario.Update(evento);
        }

        public async Task EliminarEvento(string idEvento)
        {
            await Db.EventosCalendario.DeleteAsync(idEvento);
        }

        public async Task<EventoCalendario> ObtenerPorId(string idEvento)
        {
            return await Db.EventosCalendario.GetByIdAsync(idEvento);
        }
        
        public async Task<List<EventoCalendario>> ObtenerEventosPorFechas(DateTime desde, DateTime hasta)
        {
            return await Db.EventosCalendario.ObtenerPorFechas(desde.Date, hasta.Date.AddDays(1).AddSeconds(-1));
        }
    }
}
