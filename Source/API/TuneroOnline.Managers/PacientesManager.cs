﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IPacientesManager
    {
        Task<IEnumerable<Paciente>> BuscarPacientes(string nombreApellido, string dni, string nroFicha, string idUsuario);
        Task<List<Paciente>> BuscarPacientes(string textoBuscado);
        Task<Paciente> GuardarPaciente(Paciente paciente, string email);
        Task<Paciente> ObtenerPorId(string id);        
    }

    public class PacientesManager : ManagerBase, IPacientesManager
    {
        public PacientesManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager _sessionManager) : base(_settings, _sessionManager)
        {

        }

        public async Task<Paciente> GuardarPaciente(Paciente paciente, string email)
        {
            await ValidarPaciente(paciente);

            if (CurrentUser.Rol.Igual(RolDeUsuario.Paciente))
                paciente.IdUsuario = CurrentUser._id;

            if (paciente.IsNew) await CrearPaciente(paciente, email);
            else await ActualizarPaciente(paciente, email);

            return paciente;
        }

        public async Task<IEnumerable<Paciente>> BuscarPacientes(string nombreApellido, string dni, string nroFicha, string idUsuario)
        {      
            if (!string.IsNullOrEmpty(nombreApellido) && nombreApellido.Length < 3) throw new Exception("Debe ingresar al menos 3 caracteres para el nombre del cliente en el filtro");
            
            var pacientes = await Db.Pacientes.BuscarPacientes(nombreApellido, dni, nroFicha, idUsuario);

            var usuarios = await Db.Usuarios.GetByIdsAsync(pacientes.Select(x => x.IdUsuario).Where(x => !string.IsNullOrEmpty(x)).ToList());

            var profesionales = await Db.Profesionales.GetByIdsAsync(pacientes.Select(x => x.IdProfesionalDefault).Where(x => !string.IsNullOrEmpty(x)).ToList());

            foreach (var paciente in pacientes)
            {
                paciente.Usuario = usuarios.FirstOrDefault(x => x._id == paciente.IdUsuario);

                if(!string.IsNullOrEmpty(paciente.IdProfesionalDefault))
                    paciente.Profesional = profesionales.FirstOrDefault(x => x._id == paciente.IdProfesionalDefault);
            }

            return pacientes;
        }

        public async Task<Paciente> ObtenerPorId(string id)
        {
            var paciente = await Db.Pacientes.GetByIdAsync(id);

            if (paciente == null) throw new Exception("No existe paciente con el id solicitado");

            return paciente;
        }

        private async Task CrearPaciente(Paciente paciente, string email)
        {
            paciente.Estado = EstadoPaciente.Activo;

            if (!string.IsNullOrEmpty(paciente.IdUsuario))
            {
                var pacientesDelUsuario = await Db.Pacientes.BuscarPacientes(null, null, null, paciente.IdUsuario);
                paciente.EsPacienteUsuario = !pacientesDelUsuario.Any();
            }
            else if(!string.IsNullOrEmpty(email))
            {
                var usuario = Db.Usuarios.GetUserByEmail(email);

                if (usuario != null)
                    paciente.IdUsuario = usuario._id;
            }

            await Db.Pacientes.Insert(paciente);
        }

        private async Task ActualizarPaciente(Paciente paciente, string email)
        {
            var pacienteActual = await Db.Pacientes.GetByIdAsync(paciente._id);

            paciente.EsPacienteUsuario = pacienteActual.EsPacienteUsuario;

            if (string.IsNullOrEmpty(paciente.Domicilio))
                paciente.Domicilio = pacienteActual.Domicilio;
            
            if (paciente.Estado == null)
                paciente.Estado = pacienteActual.Estado;

            if (string.IsNullOrEmpty(paciente.NroFicha))
                paciente.NroFicha = pacienteActual.NroFicha;

            if (string.IsNullOrEmpty(paciente.Ciudad))
                paciente.Ciudad = pacienteActual.Ciudad;

            if (paciente.Provincia == null)
                paciente.Provincia = pacienteActual.Provincia;

            if (!string.IsNullOrEmpty(email))
            {
                var usuario = Db.Usuarios.GetUserByEmail(email);

                paciente.IdUsuario = usuario != null ? usuario._id : pacienteActual.IdUsuario;
            }
            else
                paciente.IdUsuario = pacienteActual.IdUsuario;

            await Db.Pacientes.Update(paciente);
        }

        private async Task ValidarPaciente(Paciente paciente)
        {
            string mensaje = string.Empty;

            //Verificar que todos los datos estén completos.
            if (string.IsNullOrEmpty(paciente.Dni))
                mensaje += "Debe completar el DNI. ";

            if (string.IsNullOrEmpty(paciente.Apellido))
                mensaje += "Debe completar el Apellido. ";

            if (string.IsNullOrEmpty(paciente.Nombre))
                mensaje += "Debe completar el Nombre. ";
            
            //Verificar si ya existe otro paciente con el mismo dni.
            var pacienteConDni = await Db.Pacientes.ObtenerPacientePorDni(paciente.Dni);
            if (pacienteConDni != null && !paciente.Equals(paciente))
                mensaje += "Ya existe un paciente con el DNI ingresado. Corija el DNI o intente recuperando la cuenta. ";

            if (!string.IsNullOrEmpty(mensaje))
                throw new Exception(mensaje);
        }

        public async Task<List<Paciente>> BuscarPacientes(string textoBuscado)
        {
            string idUsuarioActual = _sessionManager.GetCurrentUserSession().Rol.Igual(RolDeUsuario.Paciente) ?
                                     _sessionManager.GetCurrentUserSession()._id : null;

            var pacientes = (await Db.Pacientes.BusquedaRapida(textoBuscado, idUsuarioActual)).
                            Where(x=>x.Estado.Id == EstadoPaciente.Activo.Id).ToList();

            var usuarios = await Db.Usuarios.GetByIdsAsync(pacientes.Select(x => x.IdUsuario).
                        Where(x => !string.IsNullOrEmpty(x)).ToList());

            foreach (var paciente in pacientes)
                paciente.Usuario = usuarios.FirstOrDefault(x => x._id == paciente.IdUsuario);

            return pacientes.OrderBy(x=>x.Apellido).Take(50).ToList();
        }
    }
}
