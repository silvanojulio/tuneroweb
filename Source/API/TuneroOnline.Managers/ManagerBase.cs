﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Text;
using TurneroOnline.Entities;
using TurneroOnline.Repositories;

namespace TuneroOnline.Managers
{
    public abstract class ManagerBase
    {
        protected IOptions<TurneroOnlineConfiguration> Settings { get; set; }
        protected ISessionManager _sessionManager { get; set; }
        protected DataBaseContext Db { get; set; }

        public ManagerBase(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager sessionManager)
        {
            Settings = _settings;
            _sessionManager = sessionManager;
            Db = new DataBaseContext(_settings);
        }

        public ManagerBase(IOptions<TurneroOnlineConfiguration> _settings)
        {
            Settings = _settings;
            Db = new DataBaseContext(_settings);
        }

        public User CurrentUser
        {
            get
            {
                if (_sessionManager == null) throw new Exception("ISessionManager null");
                return _sessionManager.GetCurrentUserSession();
            }
        }

        public Profesional CurrentProfesional
        {
            get
            {
                if (_sessionManager == null) throw new Exception("ISessionManager null");
                return _sessionManager.GetCurrentProfesional().Result;
            }
        }

        public Paciente CurrentPaciente
        {
            get
            {
                if (_sessionManager == null) throw new Exception("ISessionManager null");
                return _sessionManager.GetCurrentPaciente().Result;
            }
        }
    }
}
