﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IGoogleFirebaseManager
    {
        void SendPushNotification(String deviceId, String title, String message);
        Task SendPushNotificationToUser(String idUsuario, String title, String message);
    }

    public class GoogleFirebaseManager : ManagerBase, IGoogleFirebaseManager
    {
        public GoogleFirebaseManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager _sessionManager) : base(_settings, _sessionManager)
        {}

        static String serverKey = "AAAA1jUF1B4:APA91bGDzO-T-0FBkQG0LZvMGDKDDt-4lkVJIzGb-ot11s0mjoxlydhzk7pE6WxnRUDYJnDBnrlcsUia8xgny2d6xUXrxapSCd7jtfrBvEwSySvxptUvGTxkYj3HggbRwWf6Q0yN_sMw";
        static String senderId = "920012575774";
        static String projectId = "turnero-d490b";

        public async Task SendPushNotificationToUser(String idUsuario, String title, String message)
        {
            var usuario = await Db.Usuarios.GetByIdAsync(idUsuario);

            if (usuario.IdsDevices == null || !usuario.IdsDevices.Any()) return;

            foreach (var idDevice in usuario.IdsDevices)
            {
                try
                {
                    SendPushNotification(idDevice, title, message);
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void SendPushNotification(String deviceId, String title, String message)
        {
            //Create the web request with fire base API  
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            //Sender Id - From firebase project setting  
            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = deviceId,
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = message,
                    title = title.Replace(":", "")
                }
            };
            var serializer = new Newtonsoft.Json.JsonSerializer();

            System.IO.TextWriter writer = new StringWriter();
            serializer.Serialize(writer, payload);
            Byte[] byteArray = Encoding.UTF8.GetBytes(writer.ToString());
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();                                
                            }
                    }
                }
            }
        }

    }
}
