﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IUserManager
    {
        User Login(string email, string password);
        Task CreateAdminUser();
        Task CambiarPassword(string idUsuario, string password);
        Task ResetPassword(string email);
        Task AsignarNroDeCelular(string idUsuario, string nroCelular);
        Task CrearUsuarioApp(string email, string nroCelular, string password);
        Task GuardarUsuario(User user);
        Task<List<User>> BuscarUsuarios(int idRol, string email);
        Task<User> ObtenerPorId(string idUsuario);
        Task AgregarDeviceId(string idUsuario, string idDevice);
        Task QuitarDeviceId(string idUsuario, string idDevice);
        Task ActivarCuentaPorMailYCodigo(string email, string codigoActivacion);
    }

    public class UserManager : ManagerBase, IUserManager
    {
        public UserManager(IOptions<TurneroOnlineConfiguration> _settings) : base(_settings)
        {
            
        }

        public User Login(string email, string password)
        {
            var usuario = Db.Usuarios.GetUserByEmail(email.ToLower());

            if (usuario == null || usuario.Password != password)
                throw new Exception("Credenciales no válidas");

            if (!usuario.Activo) throw new Exception("El usuario no está activo");

            return usuario;
        }

        public async Task CreateAdminUser()
        {
            await Db.Usuarios.CrearUsuarioAdmin();
        }

        public async Task CambiarPassword(string idUsuario, string password)
        {
            var usuario = await Db.Usuarios.GetByIdAsync(idUsuario);

            if (string.IsNullOrEmpty(password) || password.Trim().Length==0)
                throw new Exception("La contraseña no puede ser espacios o vacía");

            if (password.Trim().Length < 4)
                throw new Exception("La contraseña debe contener al menos 4 caracteres");

            usuario.Password = password;
            await Db.Usuarios.Update(usuario);

        }

        public async Task AsignarNroDeCelular(string idUsuario, string nroCelular)
        {
            var usuario = await Db.Usuarios.GetByIdAsync(idUsuario);

            if (string.IsNullOrEmpty(nroCelular) || nroCelular.Trim().Length == 0)
                throw new Exception("La contraseña no puede ser espacios o vacía");

            if (nroCelular.Trim().Length > 10 || nroCelular.Trim().Length < 7)
                throw new Exception("El nro ingresado no es correcto. Ingrese el código de área sin el 15.");

            usuario.NroCelular = nroCelular.Trim();
            await Db.Usuarios.Update(usuario);
        }
        
        public async Task ActivarCuentaPorMailYCodigo(string email, string codigoActivacion)
        {
            var usuarioExistente = Db.Usuarios.GetUserByEmail(email, false);

            if (usuarioExistente == null) throw new Exception("Cuenta inexistente");
            if (usuarioExistente.Activo) throw new Exception("La cuenta ya habia sido activada");
            if (usuarioExistente.CodigoValidacionEmail != codigoActivacion) throw new Exception("El código de activación no es correcto");

            usuarioExistente.Activo = true;
            usuarioExistente.FechaConfirmacionEmail = DateTime.Now;
            await Db.Usuarios.Update(usuarioExistente);
        }


        public async Task CrearUsuarioApp(string email, string nroCelular, string password)
        {
            var usuarioExistente = Db.Usuarios.GetUserByEmail(email, false);

            if (usuarioExistente != null)
            {
                if(usuarioExistente.Activo) throw new Exception("El email que desea registrar ya existe como usuario");
                else
                {
                    await EnviarEmailPorNuevoUsuario(email, usuarioExistente.CodigoValidacionEmail);
                    throw new Exception("Verifique en su bandeja de entrada, hemos reenviado el mail para validar esta cuenta");
                }
            }
                

            if (email == null || !email.Contains("@") || email.Length < 4)
                throw new Exception("Ingrese un email válido");

            if (password == null  || password.Contains(" ") || password.Trim().Length < 4)
                throw new Exception("Ingrese una contraseña válida que tenga más de 4 caracteres sin espacios");

            var random = new Random();

            var usuario = new User
            {
                Activo = false,
                CodigoValidacionCelular = random.Next(1000, 9999).ToString(),
                CodigoValidacionEmail = Guid.NewGuid().ToString(),
                Email = email.ToLower().Trim(),
                NroCelular = nroCelular,
                Password = password,
                Rol = RolDeUsuario.Paciente,
                FechaAlta = DateTime.Now
            };

            await Db.Usuarios.Insert(usuario);

            await EnviarEmailPorNuevoUsuario(email, usuario.CodigoValidacionEmail);
        }

        public async Task ResetPassword(string email)
        {
            var usuario = Db.Usuarios.GetUserByEmail(email);

            if (usuario == null) throw new Exception("No existe ningún usuario registrado con el email ingresado.");

            usuario.Password = new Random(99).Next(100000, 999999).ToString();
            await Db.Usuarios.Update(usuario);

            EnviarEmailPorResetDePassword(usuario.Password, usuario.Email).Start();
        }

        public async Task EnviarEmailPorResetDePassword(string password, string email)
        {
            var pathTemplate = Path.Combine(Settings.Value.PathTemplates, "Emails", "resetPassword.html");

            var variables = new Dictionary<string, string>();
            variables.Add("nombreEmpresa", Settings.Value.NombreEmpresa);
            variables.Add("password", password);

            var mgun = new MailGunSender(Settings.Value.MailGunPublicKey, Settings.Value.MailGunSecretKey, Settings.Value.MailGunDomain);
            await mgun.SendEmail(new List<string> { email }, null, Settings.Value.EmailFrom, Settings.Value.NombreEmpresa + ": Resteo de contraseña", pathTemplate, variables,
                null, null, null, null);
        }


        //{ { urlSistema} }
        //{ { tituloEmail} }
        //{ { nombreEmpresa} }
        //{ { linkParaConfirmar} }
        public async Task EnviarEmailPorNuevoUsuario(string email, string codigo)
        {
            var pathTemplate = Path.Combine(Settings.Value.PathTemplates, "Emails", "confirmacionEmailPaciente.html");
            var pathTemplateBase = Path.Combine(Settings.Value.PathTemplates, "Emails", "templateBase.html");
            var pathLogo = Path.Combine(Settings.Value.PathTemplates, "Emails", "logo-sistema.png");
            var parLogo = new List<Tuple<string, string>> { new Tuple<string, string>("logo-sistema.png", pathLogo) };

            var asunto = Settings.Value.NombreEmpresa + ": Confirmación de correo electrónico";
            var urlSistema = Settings.Value.UrlSistema;
            var tituloEmail = "Confirmación de correo electrónico";
            var nombreEmpresa = Settings.Value.NombreEmpresa;
            var linkParaConfirmar = Settings.Value.UrlSistema + "/login.html#/user/login?activarcuenta=true&codigo=" + codigo + "&email=" + email;

            var variables = new Dictionary<string, string>();
            variables.Add("nombreEmpresa", nombreEmpresa);
            variables.Add("urlSistema", urlSistema);
            variables.Add("tituloEmail", tituloEmail);
            variables.Add("linkParaConfirmar", linkParaConfirmar);

            var mgun = new MailGunSender(Settings.Value.MailGunPublicKey, Settings.Value.MailGunSecretKey, Settings.Value.MailGunDomain);
            await mgun.SendEmail(
                new List<string> { email }, 
                null, 
                Settings.Value.EmailFrom, 
                asunto, pathTemplate, variables,
                null,
                parLogo,  //Inline images
                null, null, pathTemplateBase);
        }

        public async Task GuardarUsuario(User user)
        {
            var usuarioExistente = Db.Usuarios.GetUserByEmail(user.Email);

            if (usuarioExistente != null && usuarioExistente._id!= user._id)
                throw new Exception("El email que desea registrar ya existe como usuario");

            if (user.Email == null || !user.Email.Contains("@") || user.Email.Length < 4)
                throw new Exception("Ingrese un email válido");

            if (string.IsNullOrEmpty(user.Password) && !user.IsNew || (user.Password.Contains(" ") || user.Password.Trim().Length < 4))
                throw new Exception("Ingrese una contraseña válida que tenga más de 4 caracteres sin espacios");

            if (user.IsNew)
            {
                var random = new Random();
                user.CodigoValidacionCelular = random.Next(1000, 9999).ToString();
                user.CodigoValidacionEmail = random.Next(1000, 9999).ToString();
                user.FechaAlta = DateTime.Now;
                user.Activo = true;
                user.Email = user.Email.ToLower().Trim();

                await Db.Usuarios.Insert(user);
            }
            else
            {
                var usuarioEdicion = await Db.Usuarios.GetByIdAsync(user._id);
                usuarioEdicion.Email = user.Email.ToLower().Trim();

                if(!string.IsNullOrEmpty(user.Password))
                    usuarioEdicion.Password = user.Password;

                usuarioEdicion.Rol = user.Rol;
                usuarioEdicion.NroCelular = user.NroCelular;
                usuarioEdicion.Activo = user.Activo;

                await Db.Usuarios.Update(usuarioEdicion);
            }
        }

        public async Task<List<User>> BuscarUsuarios(int idRol, string email)
        {
            var items = await Db.Usuarios.Buscar(email, idRol);

            items.ForEach(x => {
                x.Password = null;
            });

            return items.OrderBy(x=>x.Email).ToList();
        }

        public async Task<User> ObtenerPorId(string idUsuario)
        {
            var item = await Db.Usuarios.GetByIdAsync(idUsuario);

            return item;
        }

        public async Task AgregarDeviceId(string idUsuario, string idDevice)
        {
            var usuario = await Db.Usuarios.GetByIdAsync(idUsuario);

            if (usuario == null)
                throw new Exception("Usuario no encontrado");

            if (usuario.IdsDevices == null) usuario.IdsDevices = new List<string>();
            if (usuario.IdsDevices.Any(x => x == idDevice)) return;

            usuario.IdsDevices.Add(idDevice);
            await Db.Usuarios.Update(usuario);
        }

        public async Task QuitarDeviceId(string idUsuario, string idDevice)
        {
            var usuario = await Db.Usuarios.GetByIdAsync(idUsuario);

            if (usuario == null)
                throw new Exception("Usuario no encontrado");

            if (usuario.IdsDevices == null || !usuario.IdsDevices.Any(x=>x == idDevice))
                throw new Exception("El idDevice no existe en el usuario");

            usuario.IdsDevices.Remove(idDevice);

            await Db.Usuarios.Update(usuario);
        }

    }
}
