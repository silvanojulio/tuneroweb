﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface ITurnoManager
    {
        Task<string> CrearTurno(string idPaciente, string idConsultorio, string idProfesional, DateTime fechaHoraInicio, int minutos, 
            string celular, string email, string comentarios);
        Task<List<Turno>> BuscarTurnos(List<string> idsProfesionales, List<string> idsConsultorios, List<int> idsEstados, DateTime desde, DateTime hasta, string idDisponibilidad);
        Task AsignarProfesionalSecundarioATurno(string idTurno, string idProfesional);
        Task CambiarDeEstado(string idTurno, int idEstado, string comentarios);
        Task ReasignarTurno(string idTurno, DateTime fechaHora, string idProfesional, string idConsultorio);
        Task<List<Turno>> ObtenerProximosTurnosPorUsuario(string idUsuario);
        Task<Turno> ObtenerPorId(string idTurno);
        Task TestGoogle();
        Task AgregarTurnoAAgendaGoogle(string idTurno, string comentarios=null);
        Task AgregarComentario(string idTurno, string comentarios);
    }

    public class TurnoManager : ManagerBase, ITurnoManager
    {
        private IDisponiblidadManager _disponibilidadManager;
        private IGoogleFirebaseManager _googleFirebaseManager;

        public TurnoManager(IOptions<TurneroOnlineConfiguration> settings, ISessionManager _sessionManager,
            IDisponiblidadManager disponibilidadManager, IGoogleFirebaseManager googleFirebaseManager) : base(settings, _sessionManager)
        {
            _disponibilidadManager = disponibilidadManager;
            _googleFirebaseManager = googleFirebaseManager;
        }

        public async Task<List<Turno>> BuscarTurnos(List<string> idsProfesionales, List<string> idsConsultorios, List<int> idsEstados, 
            DateTime desde, DateTime hasta, string idDisponibilidad)
        {
            if (!string.IsNullOrEmpty(idDisponibilidad)) {
                var disponibilidad = await Db.Disponibilidades.GetByIdAsync(idDisponibilidad);

                idsProfesionales = new List<string> { disponibilidad.IdProfesional };
                idsConsultorios = new List<string> { disponibilidad.IdConsultorio };
                idsEstados = EstadoDeTurno.EstadosValidosParaPaciente.Select(x=>x.Id).ToList();
                desde = disponibilidad.fechaHoraInicio.AddSeconds(-1);
                hasta = disponibilidad.fechaHoraFin.AddSeconds(1);
            }

            var turnos = await Db.Turnos.BuscarTurnos(desde, hasta, idsConsultorios, idsProfesionales, idsEstados, null);

            var idsProf = turnos.Select(x => x.IdProfesional).ToList();
            idsProf.AddRange(turnos.Select(x => x.IdProfesionalSecundario).ToList());
            idsProf = idsProf.Distinct().ToList();

            var idsPac = turnos.Select(x => x.IdPaciente).Distinct().ToList();
            var idsCons = turnos.Select(x => x.IdConsultorio).Distinct().ToList();

            var pacientes = Db.Pacientes.GetByIdsAsync(idsPac);
            var consultorios = Db.Consultorios.GetByIdsAsync(idsCons);
            var profesionales = Db.Profesionales.GetByIdsAsync(idsProf);

            foreach (var turno in turnos)
            {
                turno.Profesional = (await profesionales).FirstOrDefault(x => x._id == turno.IdProfesional);
                turno.Consultorio = (await consultorios).FirstOrDefault(x => x._id == turno.IdConsultorio);
                turno.Paciente = (await pacientes).FirstOrDefault(x => x._id == turno.IdPaciente);

                if (!string.IsNullOrEmpty(turno.IdProfesionalSecundario))
                    turno.ProfesionalSecundario = (await profesionales).FirstOrDefault(x => x._id == turno.IdProfesionalSecundario);
            }

            return turnos.OrderBy(x=>x.fechaHoraInicio).ToList();
        }

        public async Task CambiarDeEstado(string idTurno, int idEstado, string comentarios)
        {
            var turno = await Db.Turnos.GetByIdAsync(idTurno);
            turno.Estado = EstadoDeTurno.ObtenerEstado(idEstado);
            turno.AgregarHistorial("Cambio de estado: " + turno.Estado.Descripcion, _sessionManager.GetCurrentUserSession().Email);

            if(!string.IsNullOrEmpty(comentarios))
                turno.AgregarHistorial("Comentario: " + comentarios, 
                    _sessionManager.GetCurrentUserSession().Email);

            await Db.Turnos.Update(turno);

            if (turno.Estado.Igual(EstadoDeTurno.ConfirmadoPorConsultorio) || 
                turno.Estado.Igual(EstadoDeTurno.Cancelado) || 
                turno.Estado.Igual(EstadoDeTurno.CanceladoPorPaciente))
            {
                EnviarEmailPorCambioDeEstado(idTurno, false).Start();

                if (!string.IsNullOrEmpty(turno.IdGoogleCalendarEvent) && 
                    (turno.Estado.Igual(EstadoDeTurno.Cancelado) || turno.Estado.Igual(EstadoDeTurno.CanceladoPorPaciente)))
                {
                    QuitarTurnoDeAgendaGoogle(idTurno).Start();
                }
                else if (string.IsNullOrEmpty(turno.IdGoogleCalendarEvent) && turno.Estado.Igual(EstadoDeTurno.ConfirmadoPorConsultorio))
                {
                    AgregarTurnoAAgendaGoogle(idTurno).Start();
                }
            }
        }

        public async Task<string> CrearTurno(string idPaciente, string idConsultorio, string idProfesional, 
            DateTime fechaHoraInicio, int minutos, string celular, string email, string comentarios)
        {
            var fechaHoraFin = fechaHoraInicio.AddMinutes(minutos);

            var disponibilidades = await _disponibilidadManager.
                    ObtenerDisponibilidadesParaTurno(fechaHoraInicio.Date, fechaHoraFin.Date.AddDays(1).AddSeconds(1),
                new List<string> { idConsultorio }, null);

            if (!disponibilidades.Any(x => x.FechaDesde <= fechaHoraInicio && x.FechaHasta >= fechaHoraFin 
                    && x.IdProfesional == idProfesional))
                throw new Exception("El profesional no tiene la disponibilidad para el turno que desea asignar");

            var disponibilidadSecundaria = minutos != 15 ? null :
                disponibilidades.FirstOrDefault(x => x.FechaDesde <= fechaHoraInicio && x.FechaHasta >= fechaHoraFin
                    && x.IdProfesional != idProfesional);

            var turno = new Turno
            {
                IdConsultorio = idConsultorio,
                IdPaciente = idPaciente,
                IdProfesional = idProfesional,
                IdProfesionalSecundario = disponibilidadSecundaria != null? 
                                            disponibilidadSecundaria.IdProfesional : null,
                NroCelular = celular,
                Email = email,
                fechaHoraInicio = fechaHoraInicio,
                fechaHoraFin = fechaHoraFin,
                FechaConfirmado = DateTime.Now,
                Observaciones = "Turno asignado por sistema web",
            };
            
            bool enviarEmailDeConfirmacion = false;

            if (_sessionManager.GetCurrentUserSession().Rol.Igual(RolDeUsuario.Paciente))
            {
                turno.Estado = EstadoDeTurno.ConfirmadoPorPaciente;
            }
            else
            {
                turno.Estado = EstadoDeTurno.ConfirmadoPorConsultorio;
                enviarEmailDeConfirmacion = true;
            }

            turno.AgregarHistorial("Creación de turno: " + turno.Estado.Descripcion, _sessionManager.GetCurrentUserSession().Email);

            if(!string.IsNullOrEmpty(comentarios))
                turno.AgregarHistorial("Observaciones: " + comentarios, _sessionManager.GetCurrentUserSession().Email);

            await Db.Turnos.Insert(turno);

            var paciente = await Db.Pacientes.GetByIdAsync(idPaciente);

            paciente.IdProfesionalDefault = idProfesional;
            await Db.Pacientes.Update(paciente);

            if(!string.IsNullOrEmpty(email) && email.Contains("@"))
            {
                var usuarioDePaciente = Db.Usuarios.GetUserByEmail(email);

                if (usuarioDePaciente == null)
                {
                    CrearUsuarioDeTurno(email, celular, paciente);
                }
            }

            if(enviarEmailDeConfirmacion)
            {
                EnviarEmailPorCambioDeEstado(turno._id, false).Start();
                AgregarTurnoAAgendaGoogle(turno._id, comentarios).Start();
            }

            return turno._id;
        }

        public async Task<List<Turno>> ObtenerProximosTurnosPorUsuario(string idUsuario)
        {
            if (string.IsNullOrEmpty(idUsuario))
                throw new Exception("El idUsuario no puede ser nulo");

            var idsEstados = EstadoDeTurno.EstadosValidosParaPaciente.Select(x => x.Id).ToList();
            var pacientesDeUsuario = Db.Pacientes.BuscarPacientes(null, null, null, idUsuario);
            var idsPacientes = (await pacientesDeUsuario).Select(x => x._id).ToList();

            if (!idsPacientes.Any()) return new List<Turno>();

            var turnos = await Db.Turnos.BuscarTurnos(DateTime.Today, DateTime.Now.AddMonths(6), null, null, null, idsPacientes);

            var idsProf = turnos.Select(x => x.IdProfesional).Distinct().ToList();
            var idsPac = turnos.Select(x => x.IdPaciente).Distinct().ToList();
            var idsCons = turnos.Select(x => x.IdConsultorio).Distinct().ToList();

            var pacientes = Db.Pacientes.GetByIdsAsync(idsPac);
            var consultorios = Db.Consultorios.GetByIdsAsync(idsCons);
            var profesionales = Db.Profesionales.GetByIdsAsync(idsProf);

            foreach (var turno in turnos)
            {
                turno.Profesional = (await profesionales).FirstOrDefault(x => x._id == turno.IdProfesional);
                turno.Consultorio = (await consultorios).FirstOrDefault(x => x._id == turno.IdConsultorio);
                turno.Paciente = (await pacientes).FirstOrDefault(x => x._id == turno.IdPaciente);
            }

            return turnos.OrderBy(x => x.fechaHoraInicio).ToList();
        }

        public async Task ReasignarTurno(string idTurno, DateTime fechaHoraInicio, string idProfesional, string idConsultorio)
        {
            if (fechaHoraInicio < DateTime.Now)
                throw new Exception("El inicio del turno ya ha pasado. Debe seleccionar fecha y hora válida.");

            var turno = await Db.Turnos.GetByIdAsync(idTurno);
            var fechaHoraFin = fechaHoraInicio.AddMinutes(turno.Minutos);

            var disponibilidades = await _disponibilidadManager.ObtenerDisponibilidadesParaTurno(fechaHoraInicio.Date, fechaHoraFin.Date.AddDays(1).AddSeconds(1),
                new List<string> { idConsultorio }, new List<string> { idProfesional });

            if (!disponibilidades.Any(x => x.FechaDesde <= fechaHoraInicio && x.FechaHasta >= fechaHoraFin))
                throw new Exception("EL profesional no tiene la disponibilidad para el turno que desea asignar");

            turno.fechaHoraInicio = fechaHoraInicio;
            turno.fechaHoraFin = fechaHoraFin;
            turno.IdConsultorio = idConsultorio;
            turno.IdProfesional = idProfesional;

            turno.Estado = EstadoDeTurno.ConfirmadoPorConsultorio;

            turno.AgregarHistorial("Reasignación de turno: " + turno.Estado.Descripcion, _sessionManager.GetCurrentUserSession().Email);

            await Db.Turnos.Update(turno);

            EnviarEmailPorCambioDeEstado(turno._id, true).Start();
            QuitarTurnoDeAgendaGoogle(turno._id).Start();
            AgregarTurnoAAgendaGoogle(turno._id).Start();
        }

        public async Task<Turno> ObtenerPorId(string idTurno)
        {
            var turno = await Db.Turnos.GetByIdAsync(idTurno);

            var profesional = Db.Profesionales.GetByIdAsync(turno.IdProfesional);
            var consultorio = Db.Consultorios.GetByIdAsync(turno.IdConsultorio);
            var paciente = Db.Pacientes.GetByIdAsync(turno.IdPaciente);

            turno.Profesional = await profesional;
            turno.Consultorio = await consultorio;
            turno.Paciente = await paciente;

            if (!string.IsNullOrEmpty(turno.IdProfesionalSecundario))
                turno.ProfesionalSecundario = await Db.Profesionales.GetByIdAsync(turno.IdProfesionalSecundario);
            
            return turno;
        }

        public async Task AgregarComentario(string idTurno, string comentarios)
        {
            var turno = await Db.Turnos.GetByIdAsync(idTurno);

            turno.AgregarHistorial("Observaciones: " + comentarios, _sessionManager.GetCurrentUserSession().Email);

            await Db.Turnos.Update(turno);
        }
        
        public async Task AsignarProfesionalSecundarioATurno(string idTurno, string idProfesional)
        {
            var turno = await Db.Turnos.GetByIdAsync(idTurno);

            if (string.IsNullOrEmpty(idProfesional))
            {
                turno.IdProfesionalSecundario = null;
                await Db.Turnos.Update(turno);
                return;
            }

            var disponibilidad = (await _disponibilidadManager.
                   ObtenerDisponibilidadesParaTurno(turno.fechaHoraInicio, turno.fechaHoraFin,
               new List<string> { turno.IdConsultorio }, new List<string> { idProfesional }))
               .Where(x => x.FechaDesde <= turno.fechaHoraInicio && x.FechaHasta >= turno.fechaHoraFin)
               .ToList();

            if (disponibilidad == null || !disponibilidad.Any())
                throw new Exception("El profesional seleccionado no tiene disponibilidad para el horario del turno");

            if (turno.IdProfesional == idProfesional)
                throw new Exception("El profesional secundario debe ser diferente que el principal");

            turno.IdProfesionalSecundario = idProfesional;

            if(!string.IsNullOrEmpty(idProfesional))
            {
                var profesionalAgregado = await Db.Profesionales.GetByIdAsync(idProfesional);
                turno.AgregarHistorial("Segundo profesional: " + profesionalAgregado.NombreCompleto,
                        _sessionManager.GetCurrentUserSession().Email);
            }
            else
            {
                turno.AgregarHistorial("Segundo profesional quitado.", 
                    _sessionManager.GetCurrentUserSession().Email);
            }

            await Db.Turnos.Update(turno);

            if (!string.IsNullOrEmpty(turno.IdGoogleCalendarEvent))
            {
                try
                {
                    var profesionalSecundario = await Db.Profesionales.GetByIdAsync(idProfesional);
                    if (string.IsNullOrEmpty(profesionalSecundario.IdUsuario)) return;
                    profesionalSecundario.Usuario = await Db.Usuarios.GetByIdAsync(profesionalSecundario.IdUsuario);

                    var service = GetCalendarService();
                    var evento = service.Events.Get("primary", turno.IdGoogleCalendarEvent).Execute();
                    evento.Attendees.Add(new EventAttendee() { Email = profesionalSecundario.Usuario.Email });
                    service.Events.Update(evento, "primary", turno.IdGoogleCalendarEvent).Execute();
                }
                catch (Exception ex)
                {
                    turno.AgregarHistorial("No se ha podido agregar el segundo profesional al calendario de Google.",
                                            _sessionManager.GetCurrentUserSession().Email);
                    await Db.Turnos.Update(turno);
                }
            }
        }

        #region envios de email

        private Task EnviarEmailPorCambioDeEstado(string idTurno, bool esReprogramacion)
        {
            var t = new Task(async () =>
            {
                var turno = await ObtenerPorId(idTurno);
                
                if (string.IsNullOrEmpty(turno.Paciente.IdUsuario)) return;

                var template = "";
                var asunto = "";
                var mensajeMobile = "";

                if (turno.Estado.Igual(EstadoDeTurno.ConfirmadoPorConsultorio))
                {
                    if (esReprogramacion)
                    {
                        template = "reprogramacionDeTurno.html";
                        asunto = "Turno reprogramado";
                        mensajeMobile = "Su turno ha sido reprogramado para el {0} a las {1} hs, con {2}, en {3}";
                    }
                    else
                    {
                        template = "confirmacionTurno.html";
                        asunto = "Turno confirmado";
                        mensajeMobile = "Su turno ha sido confirmado desde el consultorio para el {0} a las {1} hs, con {2}, en {3}";
                    }
                }
                else if (turno.Estado.Igual(EstadoDeTurno.Cancelado) || turno.Estado.Igual(EstadoDeTurno.CanceladoPorPaciente))
                {
                    template = "cancelacionDeTurno.html";
                    asunto = "Turno cancelado";
                    mensajeMobile = "Se ha cancelado el próximo turno. {0} a las {1} hs, con {2}, en {3}";
                }

                mensajeMobile = string.Format(mensajeMobile, turno.fechaHoraInicio.ToString("dd/MM/yyyy"), turno.fechaHoraInicio.ToString("HH:mm"),
                    turno.Profesional.NombreCompleto, turno.Consultorio.Direccion);

                var usuario = await Db.Usuarios.GetByIdAsync(turno.Paciente.IdUsuario);

                var pathTemplate = Path.Combine(Settings.Value.PathTemplates, "Emails", template);

                var variables = new Dictionary<string, string>();
                variables.Add("nombreEmpresa", Settings.Value.NombreEmpresa);
                variables.Add("fechaTurno", turno.fechaHoraInicio.ToString("dd/MM/yyyy HH:mm"));
                variables.Add("consultorio", turno.Consultorio.Nombre);
                variables.Add("profesional", turno.Profesional.NombreCompleto);
                variables.Add("paciente", turno.Paciente.NombreCompleto);

                try
                {
                    if (!string.IsNullOrEmpty(turno.Paciente.IdUsuario))
                    {
                        await Db.NotificacionesMobile.CrearNotificacion(asunto, mensajeMobile, turno.Paciente.IdUsuario);
                        await _googleFirebaseManager.SendPushNotificationToUser(turno.Paciente.IdUsuario, asunto, mensajeMobile);
                    }
                }
                catch (Exception ex)
                {}

                var mgun = new MailGunSender(Settings.Value.MailGunPublicKey, 
                                             Settings.Value.MailGunSecretKey, 
                                             Settings.Value.MailGunDomain);
                await mgun.SendEmail(new List<string> { usuario.Email }, null, Settings.Value.EmailFrom, 
                                                                    Settings.Value.NombreEmpresa + ": "+ asunto, pathTemplate, variables, 
                    null, null, null, null);
            });

            return t;
        }

        #endregion

        #region Usuario en turno

        async void CrearUsuarioDeTurno(string email, string nroCelular, Paciente paciente)
        {
            var random = new Random();

            var usuario = new User
            {
                Activo = true,
                CodigoValidacionCelular = random.Next(1000, 9999).ToString(),
                CodigoValidacionEmail = random.Next(1000, 9999).ToString(),
                Email = email.ToLower().Trim(),
                NroCelular = nroCelular,
                Password = "NoPassword",
                Rol = RolDeUsuario.Paciente,
                FechaAlta = DateTime.Now
            };

            await Db.Usuarios.Insert(usuario);

            paciente.IdUsuario = usuario._id;

            await Db.Pacientes.Update(paciente);
        }

        #endregion

        static string ApplicationName = "TurneroOnline";

        private CalendarService GetCalendarService()
        {
            try
            {
                var certificate = new X509Certificate2(@"google-key.p12", "notasecret", X509KeyStorageFlags.Exportable);

                ServiceAccountCredential credential = new ServiceAccountCredential(
                  new ServiceAccountCredential.Initializer(Settings.Value.AccountServiceEmail)
                  {
                      Scopes = new[] { CalendarService.Scope.Calendar }
                  }.FromCertificate(certificate));

                /// Create the service.
                var service = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = Settings.Value.ApplicationName,
                });

                return service;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Task AgregarTurnoAAgendaGoogle(string idTurno, string comentarios=null)
        {
            var t = new Task(async ()=> {

                var turno = await ObtenerPorId(idTurno);

                if (string.IsNullOrEmpty(turno.Profesional.IdUsuario))
                {
                    turno.AgregarHistorial("Google Calendar: El profesional principal del turno no es usuario en el sistema", _sessionManager.GetCurrentUserSession().Email);
                    await Db.Turnos.Update(turno);
                    return;
                }

                turno.Profesional.Usuario = await Db.Usuarios.GetByIdAsync(turno.Profesional.IdUsuario);

                List<string> emails = new List<string> { turno.Profesional.Usuario.Email };

                if (turno.ProfesionalSecundario != null && !string.IsNullOrEmpty(turno.ProfesionalSecundario.IdUsuario))
                {
                    turno.ProfesionalSecundario.Usuario = await Db.Usuarios.GetByIdAsync(turno.ProfesionalSecundario.IdUsuario);
                    emails.Add(turno.ProfesionalSecundario.Usuario.Email);
                } 

                if (!string.IsNullOrEmpty(turno.Paciente.IdUsuario))
                {
                    var usuarioPaciente = await Db.Usuarios.GetByIdAsync(turno.Paciente.IdUsuario);

                    if (usuarioPaciente != null)
                    {
                        emails.Add(usuarioPaciente.Email);
                    }
                }

                int difHoraria = string.IsNullOrEmpty(Settings.Value.DiferenciaHorariaCalendar)? 0:
                                    Convert.ToInt32(Settings.Value.DiferenciaHorariaCalendar);

                string titulo = string.Format("Turno: Paciente {0}. {1}. ", turno.Paciente.NombreCompleto, Settings.Value.NombreEmpresa);
                string descripcion = string.Format("{0}. En {1}.{2}", 
                    turno.Profesional.NombreCompleto, 
                    turno.Consultorio.Nombre, 
                    string.IsNullOrEmpty(comentarios)?"": "\n Observaciones: " + comentarios);

                string lugar = turno.Consultorio.Direccion;

                try
                {
                    Event myEvent = new Event
                    {
                        Summary = titulo,
                        Location = lugar,
                        Description = descripcion,
                        Start = new EventDateTime()
                        {
                            DateTime = turno.fechaHoraInicio.AddMinutes(difHoraria),
                            TimeZone = "America/Argentina/Buenos_Aires"
                        },
                        End = new EventDateTime()
                        {
                            DateTime = turno.fechaHoraFin.AddMinutes(difHoraria),
                            TimeZone = "America/Argentina/Buenos_Aires"
                        },
                        Attendees = emails.Select(x => new EventAttendee() { Email = x }).ToList()
                    };

                    var service = GetCalendarService();

                    Event r = service.Events.Insert(myEvent, "primary").Execute();

                    if (string.IsNullOrEmpty(r.Id)) throw new Exception("No exitoso");

                    turno.IdGoogleCalendarEvent = r.Id;

                    await Db.Turnos.Update(turno);

                }catch(Exception ex)
                {
                    turno.AgregarHistorial("Google Calendar: Se ha producido un error al intentar replicarlo. Error: "+ex.Message, _sessionManager.GetCurrentUserSession().Email);
                    await Db.Turnos.Update(turno);
                }

            });

            return t;
        }

        private Task QuitarTurnoDeAgendaGoogle(string idTurno)
        {
            var t = new Task(async () => {

                var turno = await ObtenerPorId(idTurno);
                if (string.IsNullOrEmpty(turno.IdGoogleCalendarEvent))
                    return;

                var service = GetCalendarService();

                var r = service.Events.Delete(Settings.Value.AccountServiceEmail, turno.IdGoogleCalendarEvent).Execute();
                
            });

            return t;
        }

        public async Task TestGoogle()
        {
            String serviceAccountEmail = "calendar-admin@turneroonline.iam.gserviceaccount.com";
            var certificate = new X509Certificate2(@"google-key.p12", "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
              new ServiceAccountCredential.Initializer(serviceAccountEmail)
              {
                  Scopes = new[] { CalendarService.Scope.Calendar }
              }.FromCertificate(certificate));

            /// Create the service.
            var service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            Event myEvent = new Event
            {
                Summary = "Ejemplo api google, mi cumple",
                Location = "Somewhere",
                Start = new EventDateTime()
                {
                    DateTime = new DateTime(2017, 11, 3, 12, 00, 00) ,
                    TimeZone = "America/Argentina/Buenos_Aires"
                },
                End = new EventDateTime()
                {
                    DateTime = new DateTime(2017, 11, 3, 15, 00, 00),
                    TimeZone = "America/Argentina/Buenos_Aires"
                },
                Attendees = new List<EventAttendee>()
                    {
                    new EventAttendee() { Email = "silvanojulio@gmail.com" },
                    new EventAttendee() { Email = "liliana93@gmail.com" }
                    }
            };
            
            Event r = service.Events.Insert(myEvent, "primary").Execute();
            
        }
    }
    
}
