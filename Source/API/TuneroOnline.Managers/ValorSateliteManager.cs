﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IValorSateliteManager
    {
        Task<List<ValorSatelite>> ObtenerValoresDeTabla(TablasSatelites tablaSatelite);
        Task<List<ValorSatelite>> ObtenerValoresDeTablas(List<TablasSatelites> tablasSatelites);
        Task<List<ValorSatelite>> ObtenerValoresDeTablas(List<int> idsTablasSatelites);
    }

    public class ValorSateliteManager : ManagerBase, IValorSateliteManager
    {
        public ValorSateliteManager(IOptions<TurneroOnlineConfiguration> _settings) : base(_settings)
        {}

        public async Task<List<ValorSatelite>> ObtenerValoresDeTabla(TablasSatelites tablaSatelite)
        {
            return await Db.ValoresSatelites.ObtenerValoresDeTabla(tablaSatelite);
        }

        public async Task<List<ValorSatelite>> ObtenerValoresDeTablas(List<TablasSatelites> tablasSatelites)
        {
            return await Db.ValoresSatelites.ObtenerValoresDeTabla(tablasSatelites);
        }

        public async Task<List<ValorSatelite>> ObtenerValoresDeTablas(List<int> idsTablasSatelites)
        {
            return (await Db.ValoresSatelites.ObtenerValoresDeTabla(idsTablasSatelites)).
                OrderBy(x=>x.TextoValor).ToList();
        }
    }
}
