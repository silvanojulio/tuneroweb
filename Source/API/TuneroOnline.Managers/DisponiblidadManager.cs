﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;

namespace TuneroOnline.Managers
{
    public interface IDisponiblidadManager
    {
        Task<Disponibilidad> GuardarDisponibilidad(Disponibilidad disponiblidad);
        Task<List<Disponibilidad>> BuscarDisponibilidades(DateTime fechaDesde, DateTime fechaHasta, string idConsultorio, List<string> idsProfesionales, int? idEstado);
        Task CrearDisponibilidades(Disponibilidad disponibilidad, List<string> idsProfesionales, 
            List<DiaHoraMinutoDisponibilidad> dias);
        Task EliminarDisponibilidad(string idDisponiblidad);
        Task CancelarDisponibilidad(string idDisponiblidad, string comentarios);
        Task<List<DisponiblidadParaTurno>> ObtenerDisponibilidadesParaTurno(DateTime desde, DateTime hasta, List<string> idsConsultorios, List<string> idsProfesionales);
        Task<Disponibilidad> ObtenerDisponibilidadPorId(string idDisponibilidad);
        Task EditarDisponibilidad(string idDisponibilidad, string idProfesional, string idConsultorio, DateTime fechaHoraInicio, DateTime fechaHoraFin, string comentarios);
    }

    public class DisponiblidadManager : ManagerBase, IDisponiblidadManager
    {
        public DisponiblidadManager(IOptions<TurneroOnlineConfiguration> _settings, ISessionManager _sessionManager) : base(_settings, _sessionManager)
        {}

        public async Task<List<Disponibilidad>> BuscarDisponibilidades(DateTime fechaDesde, DateTime fechaHasta, string idConsultorio, List<string> idsProfesionales, int? idEstado)
        {
            if(CurrentUser.Rol.Igual(RolDeUsuario.Profesional)) idsProfesionales = new List<string> { CurrentProfesional._id };

            var disponibilidades = await Db.Disponibilidades.BuscarDisponibilidades(fechaDesde, fechaHasta, idConsultorio, idsProfesionales, idEstado);

            var idsProfs = disponibilidades.Select(x => x.IdProfesional).Distinct().ToList();
            var idsCons = disponibilidades.Select(x => x.IdConsultorio).Distinct().ToList();

            var profesionales = await Db.Profesionales.GetByIdsAsync(idsProfs);
            var consultorios = await Db.Consultorios.GetByIdsAsync(idsCons);

            foreach (var d in disponibilidades)
            {
                d.Profesional = profesionales.FirstOrDefault(x => x._id == d.IdProfesional);
                d.Consultorio = consultorios.FirstOrDefault(x => x._id == d.IdConsultorio);
            }

            return disponibilidades;
        }

        public async Task CancelarDisponibilidad(string idDisponiblidad, string comentarios)
        {
            var disponibilidad = await Db.Disponibilidades.GetByIdAsync(idDisponiblidad);

            if(!disponibilidad.EsReceso)
                await ValidarCancelarDisponibilidad(disponibilidad);

            disponibilidad.Comentarios = comentarios;
            disponibilidad.Estado = EstadoDisponibilidad.Cancelado;
            await Db.Disponibilidades.Update(disponibilidad);
            
        }

        public async Task EliminarDisponibilidad(string idDisponiblidad)
        {
            var disponibilidad = await Db.Disponibilidades.GetByIdAsync(idDisponiblidad);
            disponibilidad.Estado = EstadoDisponibilidad.Cancelado;
            await Db.Disponibilidades.Update(disponibilidad);
        }

        public async Task<Disponibilidad> GuardarDisponibilidad(Disponibilidad disponiblidad)
        {
            await ValidarDisponibilidad(disponiblidad);

            if (disponiblidad.IsNew) return await CrearDisponiblidad(disponiblidad);
            else return await EditarDisponiblidad(disponiblidad);
        }

        public async Task CrearDisponibilidades(Disponibilidad disponibilidad, List<string> idsProfesionales, 
            List<DiaHoraMinutoDisponibilidad> dias)
        {
            var disponibilidades = ObtenerSerieDeDisponibilidades(disponibilidad, idsProfesionales, dias);

            await ValidarDisponiblidades(disponibilidades);

            foreach (var d in disponibilidades)
            {
                await Db.Disponibilidades.Insert(d);
            }
        }
        
        public async Task<List<DisponiblidadParaTurno>> ObtenerDisponibilidadesParaTurno(DateTime desde, DateTime hasta, List<string> idsConsultorios, List<string> idsProfesionales)
        {
            var items = new List<DisponiblidadParaTurno>();
            
            var disponibilidades = await Db.Disponibilidades.BuscarDisponibilidades(desde, hasta, idsConsultorios, idsProfesionales, EstadoDisponibilidad.Confirmado.Id);
            var turnos = await Db.Turnos.BuscarTurnos(desde, hasta, idsConsultorios, idsProfesionales, EstadoDeTurno.EstadosValidosParaDisponibilidad.Select(x=>x.Id).ToList(), null);

            var idsProfs = disponibilidades.Select(x => x.IdProfesional).Distinct().ToList();
            var idsCons = disponibilidades.Select(x => x.IdConsultorio).Distinct().ToList();

            var profesionales = (await Db.Profesionales.GetByIdsAsync(idsProfs)).OrderBy(x=>x.Apellido).ToList();
            var consultorios = await Db.Consultorios.GetByIdsAsync(idsCons);

            foreach (var profesional in profesionales)
            {
                var disponibilidadesDelProfesional = disponibilidades.Where(x => x.IdProfesional == profesional._id && !x.EsReceso).OrderBy(x=>x.fechaHoraInicio).ToList();
                var recesosDelProfesional = disponibilidades.Where(x => x.IdProfesional == profesional._id && x.EsReceso).ToList();
                var turnosDeProfesional = turnos.Where(x => x.IdProfesional == profesional._id || x.IdProfesionalSecundario == profesional._id).ToList();

                var periodosOcupados = recesosDelProfesional.Select(x => (Periodo)x).ToList();
                periodosOcupados.AddRange(turnosDeProfesional.Select(x => (Periodo)x).ToList());
                periodosOcupados = periodosOcupados.OrderBy(x => x.fechaHoraInicio).ToList();

                foreach (var d in disponibilidadesDelProfesional)
                {
                    var inicio = d.fechaHoraInicio;
                    var fin = d.fechaHoraFin;

                    //Recesos o turnos ocupados
                    var ocupaciones = periodosOcupados.Where(x => x.fechaHoraInicio >= inicio && x.fechaHoraInicio <= fin).ToList();

                    foreach (var ocupacion in ocupaciones)
                    {
                        if(inicio <= ocupacion.fechaHoraInicio)
                        {
                            items.Add(new DisponiblidadParaTurno
                            {
                                FechaDesde = inicio,
                                FechaHasta = ocupacion.fechaHoraInicio,
                                IdConsultorio = d.IdConsultorio,
                                IdProfesional = d.IdProfesional,
                            });
                        }

                        inicio = ocupacion.fechaHoraFin;
                    }

                    if(inicio <= fin)
                    {
                        items.Add(new DisponiblidadParaTurno
                        {
                            FechaDesde = inicio,
                            FechaHasta = fin,
                            IdConsultorio = d.IdConsultorio,
                            IdProfesional = d.IdProfesional,
                        });
                    }
                }
            }

            foreach (var d in items)
            {
                d.NombreProfesional = profesionales.FirstOrDefault(x => x._id == d.IdProfesional).NombreCompleto;
                d.NombreConsultorio = consultorios.FirstOrDefault(x => x._id == d.IdConsultorio).Nombre;
            }

            items = items.Where(x => x.FechaDesde != x.FechaHasta).ToList();

            return items;
        }

        public async Task<Disponibilidad> ObtenerDisponibilidadPorId(string idDisponibilidad)
        {
            var disponibilidad = await Db.Disponibilidades.GetByIdAsync(idDisponibilidad);

            if (disponibilidad != null)
            {
                disponibilidad.Profesional = await Db.Profesionales.GetByIdAsync(disponibilidad.IdProfesional);
                disponibilidad.Consultorio = await Db.Consultorios.GetByIdAsync(disponibilidad.IdConsultorio);
            }

            return disponibilidad;
        }

        public async Task EditarDisponibilidad(string idDisponibilidad, string idProfesional, string idConsultorio, DateTime fechaHoraInicio, DateTime fechaHoraFin, string comentarios)
        {
            var disponibilidad = await Db.Disponibilidades.GetByIdAsync(idDisponibilidad);

            var turnosValidos = await Db.Turnos.BuscarTurnos(disponibilidad.fechaHoraInicio, disponibilidad.fechaHoraFin,
                               new List<string> { disponibilidad.IdConsultorio }, new List<string> { disponibilidad.IdProfesional },
                               new List<int> { EstadoDeTurno.ConfirmadoPorConsultorio.Id, EstadoDeTurno.ConfirmadoPorPaciente.Id, EstadoDeTurno.Reservado.Id }, null);

            var turnosIncluidos = turnosValidos.Where(x => x.fechaHoraInicio >= fechaHoraInicio && x.fechaHoraFin <= fechaHoraFin && x.IdConsultorio == idConsultorio).ToList();

            var turnosExcluidos = turnosValidos.Where(x => !turnosIncluidos.Contains(x)).ToList();

            if (turnosExcluidos.Any()) throw new Exception("Hay turnos asignados a pacientes que están relacionados con la disponibilidad editada. Debe reasignarlos antes de editar la disponibilidad.");
            
            if(disponibilidad.IdProfesional != idProfesional)
            {
                foreach (var turno in turnosValidos)
                {
                    turno.IdProfesional = idProfesional;

                    await Db.Turnos.Update(turno);
                }
            }

            disponibilidad.IdProfesional = idProfesional;
            disponibilidad.IdConsultorio = idConsultorio;
            disponibilidad.fechaHoraInicio = fechaHoraInicio;
            disponibilidad.fechaHoraFin = fechaHoraFin;
            disponibilidad.Comentarios = comentarios;

            await Db.Disponibilidades.Update(disponibilidad);
        }

        #region metodos privados

        public async Task ValidarCancelarDisponibilidad(Disponibilidad disponibilidad)
        {
            var turnosValidos = await Db.Turnos.BuscarTurnos(disponibilidad.fechaHoraInicio, disponibilidad.fechaHoraFin,
                                new List<string> { disponibilidad.IdConsultorio }, new List<string> { disponibilidad.IdProfesional },
                                new List<int> { EstadoDeTurno.ConfirmadoPorConsultorio.Id, EstadoDeTurno.ConfirmadoPorPaciente.Id, EstadoDeTurno.Reservado.Id }, null);

            if (turnosValidos.Any()) throw new Exception("No es posible cancelar esta disponibilidad ya que posee turnos asociados");
        }

        public List<Disponibilidad> ObtenerSerieDeDisponibilidades(Disponibilidad disponibilidad, 
            List<string> idsProfesionales, List<DiaHoraMinutoDisponibilidad> dias)
        {
            var disponibilidades = new List<Disponibilidad>();
           
            if (disponibilidad.EsTodoElDia)
            {
                disponibilidad.fechaHoraInicio = disponibilidad.fechaHoraInicio.Date;
                disponibilidad.fechaHoraFin = disponibilidad.fechaHoraFin.Date.AddDays(1).AddSeconds(-1);
            }

            List<Tuple<DateTime, DateTime>> fechas = dias == null || !dias.Any() ? 
                null : ObtenerFechasEntreDias(disponibilidad.fechaHoraInicio, disponibilidad.fechaHoraFin, dias);

            foreach (var idProfesional in idsProfesionales)
            {
                if (fechas == null)
                {
                    //Lógica de día sin repetición
                    var d = new Disponibilidad
                    {
                        IdProfesional = idProfesional,
                        fechaHoraInicio = disponibilidad.fechaHoraInicio,
                        fechaHoraFin = disponibilidad.fechaHoraFin,
                        IdConsultorio = disponibilidad.IdConsultorio,
                        EsReceso = disponibilidad.EsReceso,
                        EsTodoElDia = disponibilidad.EsTodoElDia,
                        Comentarios = disponibilidad.Comentarios,
                        Estado = EstadoDisponibilidad.Confirmado,
                        MotivoReceso = disponibilidad.MotivoReceso,
                    };

                    disponibilidades.Add(d);
                }
                else
                {
                    //Repeticiones
                    foreach (var fecha in fechas)
                    {
                        var d = new Disponibilidad
                        {
                            IdProfesional = idProfesional,
                            fechaHoraInicio = fecha.Item1,
                            fechaHoraFin = fecha.Item2,
                            IdConsultorio = disponibilidad.IdConsultorio,
                            EsReceso = disponibilidad.EsReceso,
                            EsTodoElDia = disponibilidad.EsTodoElDia,
                            Comentarios = disponibilidad.Comentarios,
                            Estado = EstadoDisponibilidad.Confirmado,
                            MotivoReceso = disponibilidad.MotivoReceso,
                        };

                        disponibilidades.Add(d);
                    }
                }

            }

            return disponibilidades;
            
        }

        /// <summary>
        /// Genera un listado de fechas inicio - fin solo de los días de la semana indicados usando la hora de inicio y fin para sus respectivos valores por día 
        /// </summary>
        /// <param name="fechaHoraInicio"></param>
        /// <param name="fechaHoraFin"></param>
        /// <param name="dias"></param>
        /// <returns></returns>
        private List<Tuple<DateTime, DateTime>> ObtenerFechasEntreDias(DateTime fechaInicio, DateTime fechaFin, 
            List<DiaHoraMinutoDisponibilidad> dias)
        {
            if (fechaInicio > fechaFin)
                throw new Exception("La fecha de inicio no puede ser superior a la fecha de fin");

            var fechaActual = fechaInicio.Date;
            fechaFin = fechaFin.Date;

            var fechas = new List<Tuple<DateTime, DateTime>>();
            var idsDiasDeSemana = dias.Select(x => x.dia.Id).ToList();

            while (fechaActual <= fechaFin)
            {
                var idDiaDeSemana = (int) fechaActual.DayOfWeek;

                if (idsDiasDeSemana.Contains(idDiaDeSemana))
                {
                    var dia = dias.FirstOrDefault(x => x.dia.Id == idDiaDeSemana);

                    var fechaHoraDesde = fechaActual + dia.desde;
                    var fechaHoraHasta = fechaActual + dia.hasta;

                    var fecha = new Tuple<DateTime, DateTime>(fechaHoraDesde, fechaHoraHasta);
                    fechas.Add(fecha);                   
                }

                fechaActual = fechaActual.AddDays(1);
            }

            return fechas;
        }

        private async Task<Disponibilidad> EditarDisponiblidad(Disponibilidad disponiblidad)
        {
            if (CurrentUser.Rol.Igual(RolDeUsuario.Profesional))
            {
                disponiblidad.Profesional = CurrentProfesional;
            }

            await Db.Disponibilidades.Update(disponiblidad);

            return disponiblidad;
        }

        private async Task<Disponibilidad> CrearDisponiblidad(Disponibilidad disponiblidad)
        {
            disponiblidad.Estado = EstadoDisponibilidad.Confirmado;

            if(CurrentUser.Rol.Igual(RolDeUsuario.Profesional))
            {
                disponiblidad.Profesional = CurrentProfesional;
            }

            await Db.Disponibilidades.Insert(disponiblidad);

            return disponiblidad;
        }

        private async Task ValidarDisponiblidades(Disponibilidad disponiblidad, List<string> idsProfesionales, List<DiaDeLaSemana> dias)
        {
            if (disponiblidad.fechaHoraFin < disponiblidad.fechaHoraInicio) throw new Exception("El inicio no puede ser un momento posterior al final");

            if (!disponiblidad.EsReceso)
            {
                if (idsProfesionales != null)
                {
                    if (!idsProfesionales.Any()) throw new Exception("Debe seleccionar al menos un profesional");

                    var mensaje = new StringBuilder();

                    foreach (var idProfesional in idsProfesionales)
                    {
                        List<Disponibilidad> disponiblidadesDeProf = await Db.Disponibilidades.
                            ObtenerDisponiblidadesDentroRango(disponiblidad.fechaHoraInicio, disponiblidad.fechaHoraFin, idProfesional, null, false);

                        if (disponiblidadesDeProf.Any())
                        {
                            var profesional = await Db.Profesionales.GetByIdAsync(idProfesional);
                            mensaje.Append(string.Format("El profesional {0} ya tiene una disponibilidad activa dentro o en parte del mismo periodo indicado.", 
                                                         profesional.NombreCompleto));
                        };
                    }

                    if (!string.IsNullOrEmpty(mensaje.ToString())) throw new Exception("Hay profesionales con conflicto de disponibilidades anteriores");
                }
                else
                {
                    //Buscar disponiblidad del mismo profesional dentro del mismo horario en cualquier consultorio
                    List<Disponibilidad> disponiblidades = await Db.Disponibilidades.
                        ObtenerDisponiblidadesDentroRango(disponiblidad.fechaHoraInicio, disponiblidad.fechaHoraFin, disponiblidad.Profesional._id, null, false);

                    if (disponiblidades.Any(x => x._id != disponiblidad._id))
                        throw new Exception("Ya existe una disponiblidad para el mismo profesional que cohincide con la franja horaria indicada");
                }
                
            }
            else
            {
                if (disponiblidad.MotivoReceso == null) throw new Exception("Debe seleccionar el motivo del receso");
            }

            return;
        }

        private async Task ValidarDisponiblidades(List<Disponibilidad> disponibilidades)
        {
            var mensaje = new StringBuilder();

            foreach (var disponibilidad in disponibilidades)
            {
                try
                {
                    await ValidarDisponibilidad(disponibilidad);
                }
                catch (Exception ex)
                {
                    mensaje.AppendLine(ex.Message);
                }
            }

            if (!string.IsNullOrEmpty(mensaje.ToString()))
                throw new Exception(mensaje.ToString());

        }

        private async Task ValidarDisponibilidad(Disponibilidad disponibilidad)
        {
            if (disponibilidad.EsReceso)
            {
                await ValidarReceso(disponibilidad);
                return;
            }

            List<Disponibilidad> disponiblidadesDeProf = await Db.Disponibilidades.
                           ObtenerDisponiblidadesDentroRango(disponibilidad.fechaHoraInicio, 
                           disponibilidad.fechaHoraFin, disponibilidad.IdProfesional, null, false);

            if (disponiblidadesDeProf.Any())
            {
                var profesional = await Db.Profesionales.GetByIdAsync(disponibilidad.IdProfesional);

                throw new Exception(string.Format("El profesional {0} ya tiene una disponibilidad activa dentro o en parte del mismo periodo indicado (del {1} al {2}).",
                                             profesional.NombreCompleto, 
                                             disponibilidad.fechaHoraInicio.ToString("dd/MM/yyyy HH:mm"), 
                                             disponibilidad.fechaHoraFin.ToString("dd/MM/yyyy HH:mm")));
            }
        }

        private async Task ValidarReceso(Disponibilidad disponibilidad)
        {
            if (disponibilidad.MotivoReceso == null)
                throw new Exception("Debe seleccionar el motivo del receso");
            
            List<Disponibilidad> recesoDeProf = await Db.Disponibilidades.
                           ObtenerDisponiblidadesDentroRango(disponibilidad.fechaHoraInicio,
                           disponibilidad.fechaHoraFin, disponibilidad.IdProfesional, null, true);

            if (recesoDeProf.Any())
            {
                var profesional = await Db.Profesionales.GetByIdAsync(disponibilidad.IdProfesional);

                throw new Exception(string.Format("El profesional {0} ya tiene un receso activo dentro o en parte del mismo periodo indicado (del {1} al {2}).",
                                             profesional.NombreCompleto,
                                             disponibilidad.fechaHoraInicio.ToString("dd/MM/yyyy HH:mm"),
                                             disponibilidad.fechaHoraFin.ToString("dd/MM/yyyy HH:mm")));
            }

            var turnosValidos = await Db.Turnos.BuscarTurnos(disponibilidad.fechaHoraInicio, disponibilidad.fechaHoraFin,
                                new List<string> { disponibilidad.IdConsultorio }, 
                                new List<string> { disponibilidad.IdProfesional },
                                new List<int> {
                                    EstadoDeTurno.ConfirmadoPorConsultorio.Id,
                                    EstadoDeTurno.ConfirmadoPorPaciente.Id,
                                    EstadoDeTurno.Reservado.Id }, 
                                null);

            if (turnosValidos.Any()) throw new Exception("No es posible agregar este receso ya que existen turnos dentro" +
                " del horario ingresado. Debe reasignar o cancelar los turnos para luego registrar el receso.");

        }

        #endregion
    }
}
