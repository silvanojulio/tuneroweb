﻿using Microsoft.Extensions.Options;
using SJSystems.Core.Security;
using SJSystems.Core.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TurneroOnline.Entities;
using System.Linq;
using MongoDB.Driver;

namespace TuneroOnline.Managers
{
    public interface IObraSocialManager
    {
        Task<List<ObraSocial>> ObtenerObrasSociales(bool soloActivas);
        Task<ObraSocial> Guardar(ObraSocial obraSocial);
        Task<PlanDeObraSocial> GuardarPlanDeObraSocial(PlanDeObraSocial plan);
        Task<List<PlanDeObraSocial>> ObtenerPlanesDeObraSocial(string idObraSocial);
        Task<ObraSocial> ObtenerPorId(string idObraSocial);

    }

    public class ObraSocialManager : ManagerBase, IObraSocialManager
    {
        public ObraSocialManager(IOptions<TurneroOnlineConfiguration> _settings) : base(_settings)
        {
            
        }
        
        public async Task<ObraSocial> Guardar(ObraSocial obraSocial)
        {
            if (obraSocial.IsNew)
                await Db.ObrasSociales.Insert(obraSocial);
            else
                await Db.ObrasSociales.Update(obraSocial);

            return obraSocial;
        }

        public async Task<PlanDeObraSocial> GuardarPlanDeObraSocial(PlanDeObraSocial plan)
        {
            if (plan.IsNew)
                await Db.PlanesDeObraSocial.Insert(plan);
            else
                await Db.PlanesDeObraSocial.Update(plan);

            return plan;
        }

        public async Task<List<PlanDeObraSocial>> ObtenerPlanesDeObraSocial(string idObraSocial)
        {
            var items = await Db.PlanesDeObraSocial.ObtenerPlanesDeObraSocial(idObraSocial);

            return items.OrderBy(x=>x.Nombre).ToList();
        }

        public async Task<ObraSocial> ObtenerPorId(string idObraSocial)
        {
            var item = await Db.ObrasSociales.GetByIdAsync(idObraSocial);
            item.Planes = await Db.PlanesDeObraSocial.ObtenerPlanesDeObraSocial(idObraSocial);

            return item;
        }

        public async Task<List<ObraSocial>> ObtenerObrasSociales(bool soloActivas)
        {
            var obrasSociales =  (await Db.ObrasSociales.GetAllASync()).ToList().Where(x=> x.Activo || !soloActivas).ToList();

            var idsObrasSociales = obrasSociales.Select(x => x._id).ToList();

            var planes = (await Db.PlanesDeObraSocial.ObtenerPlanesDeObraSocial(idsObrasSociales)).Where(x => x.Activo || !soloActivas).ToList();

            obrasSociales.ForEach(x => {
                x.Planes = planes.Where(y => y.IdObraSocial == x._id).OrderBy(p => p.Nombre).ToList();
            });

            return obrasSociales.OrderBy(x => x.Nombre).ToList();
        }

        #region privados
        
        #endregion
    }
}
